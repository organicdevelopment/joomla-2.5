(function($){
	
	window.addEvent('domready', function(){
		var clients = $$('.app-clients .items .client')
		clients.each(function(el){
			el.addEvent('click', function(e){
				if(!el.hasClass('active')){
					new Event(e).stop()
					clients.removeClass('active')
					el.addClass('active')
				}
			})
			
			var links = el.getElement('.pos-links')
			if(links){
				new Element('a', {'class': 'close', 'text' : 'Close Window'}).addEvent('click', function(e){
					new Event(e).stop()
					el.removeClass('active')
				}).inject(links)
			}
		})
	})
	
})(document.id)