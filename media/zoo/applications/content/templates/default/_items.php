<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
				
// init vars
$i       = 0;
$columns = array();
$column  = 0;
$row     = -1;
$rows	 = array();
$row_count = ceil(count($this->items) / $this->params->get('template.items_cols'));



// create columns
foreach($this->items as $item) {

	if ($this->params->get('template.items_order')) {
		// order down
		if ($row >= $rows) {
			$column++;
			$row  = 0;
			$rows = ceil((count($this->items) - $i) / ($this->params->get('template.items_cols') - $column));
		}
		$row++;
				
		if (!isset($columns[$column])) {
			$columns[$column] = '';
		}
		
		$columns[$column] .= $this->partial('item', compact('item'));
	} else {
		//Incrememnt col counter
		if($row == 0){
			$columns = $i;
		}
		
		//New row
		if(!($i % $this->params->get('template.items_cols'))){
			$row++;
			$rows[$row] = array();
		}
		
		$rows[$row][] = $this->partial('item', compact('item'));
		
	}
	$i++;
}


if ($this->params->get('template.items_order')) {
	// render columns
	$count = count($columns);
	if ($count) {
		echo '<div class="items items-col-'.$count.' grid-block">';
		for ($j = 0; $j < $count; $j++) {
			$first = ($j == 0) ? ' first' : null;
			$last  = ($j == $count - 1) ? ' last' : null;
			echo '<div class="grid-box col width'.intval(100 / $count).$first.$last.'">'.$columns[$j].'</div>';
		}
		echo '</div>';
	}
}else{
	
	$count = count($rows);
	if($rows){
		echo '<div class="items items-col-'.$columns.'">';
		foreach($rows AS $i => $row){
			
			$first = $i == 0 ? ' first' : null;
			$last = $i == $count-1 ? ' last' : null;
			
			echo '<div class="row row_'.($i+1).' '.$first.$last.' grid-block">';
			
			$row_count = count($row);
			foreach($row AS $j => $item){
				$first = $j == 0 ? ' first' : null;
				$last = $j == $columns-1 ? ' last' : null;
				
				echo '<div class="grid-box col col_'.($j+1).' width'.intval(100 / $columns).$first.$last.'">'.$item.'</div>';
			}
			
			echo '</div>';
		}
		echo '</div>';
	}	
}


// render pagination
echo $this->partial('pagination');