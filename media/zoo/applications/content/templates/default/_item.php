<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<?php if ($item) : ?>

	<?php if ($this->renderer->pathExists('item/'.$item->type)) : ?>
	<article class="item <?php echo $item->type;?>">
		<?php echo $this->renderer->render('item.'.$item->type.'.teaser', array('view' => $this, 'item' => $item)); ?>
	</article>
	<?php else : ?>
	<article class="item">
		<?php echo $this->renderer->render('item.teaser', array('view' => $this, 'item' => $item)); ?>
	</article>
	<?php endif; ?>

<?php endif; ?>

