<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

//Reset stylesheet
if($this->params->get('template.reset')) $this->app->document->addStylesheet('assets:css/reset.css');

// include assets css/js
$this->app->document->addStylesheet($this->template->resource.'assets/css/style.css');

//Include application stylesheets/js
if($this->app->path->path($this->template->resource.'assets/css/'.$this->application->alias.'.css')) $this->app->document->addStylesheet($this->template->resource.'assets/css/'.$this->application->alias.'.css');
if($this->app->path->path($this->template->resource.'assets/js/'.$this->application->alias.'.js')) $this->app->document->addStylesheet($this->template->resource.'assets/js/'.$this->application->alias.'.js');

//Include submission stylesheets/js
if($this->app->path->path($this->template->resource.'assets/css/'.$this->submission->alias.'.css')) $this->app->document->addStylesheet($this->template->resource.'assets/css/'.$this->submission->alias.'.css');
if($this->app->path->path($this->template->resource.'assets/js/'.$this->submission->alias.'.js')) $this->app->document->addStylesheet($this->template->resource.'assets/js/'.$this->submission->alias.'.js');


//Set classes
$classes = array('yoo-zoo','page-mysubmission');
$classes[] = 'group-'.$this->application->getGroup();
$classes[] = 'tmpl-'.$this->template->name;
$classes[] = 'app-'.$this->application->alias;
$classes[] = 'sub-'.$this->submission->alias;

//Get the layouts
$layouts = $this->renderer->getLayouts('mysubmission');
?>

<div id="yoo-zoo" class="<?php echo implode(' ', $classes) ?>">
	
	<?php if (in_array($this->application->alias.'.'.$this->submission->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('mysubmission.'.$this->application->alias.'.'.$this->submission->alias, array('view' => $this, 'submission' => $this->submission, 'template' => $this->template)); ?>
	<?php elseif (in_array($this->application->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('mysubmission.'.$this->application->alias, array('view' => $this, 'submission' => $this->submission, 'template' => $this->template)); ?>
	<?php else: ?>
		<?php echo $this->renderer->render('mysubmission.default', array('view' => $this, 'submission' => $this->submission, 'template' => $this->template)); ?>
	<?php endif ?>
</div>