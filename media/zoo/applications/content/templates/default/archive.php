<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

//Reset stylesheet
if($this->params->get('template.reset')) $this->app->document->addStylesheet('assets:css/reset.css');

// include assets css/js
$this->app->document->addStylesheet($this->template->resource.'assets/css/style.css');

//Include application stylesheets/js
if($this->app->path->path($this->template->resource.'assets/css/'.$this->application->alias.'.css')) $this->app->document->addStylesheet($this->template->resource.'assets/css/'.$this->application->alias.'.css');
if($this->app->path->path($this->template->resource.'assets/js/'.$this->application->alias.'.js')) $this->app->document->addStylesheet($this->template->resource.'assets/js/'.$this->application->alias.'.js');

// show description only if it has content
if (!$this->category->description) {
	$this->params->set('template.show_description', 0);
}

// show image only if an image is selected
if (!($image = $this->category->getImage('content.image'))) {
	$this->params->set('template.show_image', 0);
}

//Set classes
$classes = array('yoo-zoo','page-archive');
$classes[] = 'group-'.$this->application->getGroup();
$classes[] = 'tmpl-'.$this->template->name;
$classes[] = 'app-'.$this->application->alias;
$classes[] = 'archive-'.$this->category->alias;

//Get the layouts
$layouts = $this->renderer->getLayouts('archive');
?>

<div id="yoo-zoo" class="<?php echo implode(' ', $classes) ?>">
	
	<?php if (in_array($this->application->alias.'.'.$this->category->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('archive.'.$this->application->alias.'.'.$this->category->alias, array('view' => $this, 'items' => $this->items, 'category' => $this->category, 'template' => $this->template)); ?>
	<?php if (in_array($this->application->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('archive.'.$this->application->alias, array('view' => $this, 'items' => $this->items, 'category' => $this->category, 'template' => $this->template)); ?>
	<?php else: ?>
		<?php echo $this->renderer->render('archive.default', array('view' => $this, 'items' => $this->items, 'category' => $this->category, 'template' => $this->template)); ?>
	<?php endif ?>
</div>