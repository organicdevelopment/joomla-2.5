<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<?php if ($view->params->get('template.show_title')) : ?>
<h1 class="title"><?php echo $view->application->getParams()->get('content.title') ?></h1>
<?php endif; ?>

<?php if ($view->application->getParams()->get('content.subtitle')) : ?>
<h2 class="title"><?php echo $view->application->getParams()->get('content.subtitle') ?></h2>
<?php endif; ?>

<?php if ($view->params->get('template.show_description') || $view->params->get('template.show_image')) : ?>
<div class="description">
	<?php if ($view->params->get('template.show_image')) : ?>
		<img class="size-auto align-<?php echo $view->params->get('template.alignment'); ?>" src="<?php echo $image['src']; ?>" <?php echo $image['width_height']; ?> title="<?php echo $view->application->getParams()->get('content.title'); ?>" alt="<?php echo $view->application->getParams()->get('content.title'); ?>" />
	<?php endif; ?>
	<?php if ($view->params->get('template.show_description')) echo $view->application->getText($view->application->description); ?>
</div>
<?php endif; ?>

<?php

	// render items
	if (count($view->items)) {
		echo $view->partial('items');
	}
	
?>