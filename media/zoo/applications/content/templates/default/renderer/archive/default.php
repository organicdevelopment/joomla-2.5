<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<?php if ($view->params->get('template.show_title')) : ?>
<h1 class="page-title"><?php echo JText::sprintf('Archive from %s', strftime('%B %Y',strtotime($view->date))) ?></h1>
<?php endif; ?>

<?php if ($category->id) : ?>
<h2 class="title"><?php echo $category->getParams()->get('content.title', $category->name) ?></h2>
<?php endif; ?>

<?php if ($view->params->get('template.show_description') || $view->params->get('template.show_image')) : ?>
<div class="description">
	<?php if ($view->params->get('template.show_image')) : ?>
		<img class="size-auto align-<?php echo $view->params->get('template.alignment'); ?>" src="<?php echo $image['src']; ?>" <?php echo $image['width_height']; ?> title="<?php echo $category->name; ?>" alt="<?php echo $category->name; ?>" />
	<?php endif; ?>
	<?php if ($view->params->get('template.show_description')) echo $category->getText($category->description); ?>
</div>
<?php endif; ?>

<?php

	// render items
	if (count($view->items)) {
		echo $view->partial('items');
	}
	
?>