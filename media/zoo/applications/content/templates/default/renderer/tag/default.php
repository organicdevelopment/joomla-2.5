<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<?php if ($view->params->get('template.show_title')) : ?>
<h1 class="tag-title"><?php echo JText::_('Articles tagged with').': '.$view->tag; ?></h1>
<?php endif; ?>

<?php
// render items
if (count($view->items)) {
	echo $view->partial('items');
}
?>