<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

//Reset stylesheet
if($this->params->get('template.reset')) $this->app->document->addStylesheet('assets:css/reset.css');

// include assets css/js
$this->app->document->addStylesheet($this->template->resource.'assets/css/style.css');

//Include application stylesheets/js
if($this->app->path->path($this->template->resource.'assets/css/'.$this->application->alias.'.css')) $this->app->document->addStylesheet($this->template->resource.'assets/css/'.$this->application->alias.'.css');
if($this->app->path->path($this->template->resource.'assets/js/'.$this->application->alias.'.js')) $this->app->document->addStylesheet($this->template->resource.'assets/js/'.$this->application->alias.'.js');

//Include tag stylesheets/js
if($this->app->path->path($this->template->resource.'assets/css/'.$this->tag->alias.'.css')) $this->app->document->addStylesheet($this->template->resource.'assets/css/'.$this->tag->alias.'.css');
if($this->app->path->path($this->template->resource.'assets/js/'.$this->tag->alias.'.js')) $this->app->document->addStylesheet($this->template->resource.'assets/js/'.$this->tag->alias.'.js');


//Set classes
$classes = array('yoo-zoo','page-tag');
$classes[] = 'group-'.$this->application->getGroup();
$classes[] = 'tmpl-'.$this->template->name;
$classes[] = 'app-'.$this->application->alias;
$classes[] = 'tag-'.$this->tag->alias;

//Get the layouts
$layouts = $this->renderer->getLayouts('tag');
?>

<div id="yoo-zoo" class="<?php echo implode(' ', $classes) ?>">
	
	<?php if (in_array($this->application->alias.'.'.$this->tag->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('tag.'.$this->application->alias.'.'.$this->tag->alias, array('view' => $this, 'items' => $this->items, 'tag' => $this->tag, 'template' => $this->template)); ?>
	<?php if (in_array($this->application->alias, $layouts)) : ?>
		<?php echo $this->renderer->render('tag.'.$this->application->alias, array('view' => $this, 'items' => $this->items, 'tag' => $this->tag, 'template' => $this->template)); ?>
	<?php else: ?>
		<?php echo $this->renderer->render('tag.default', array('view' => $this, 'items' => $this->items, 'tag' => $this->tag, 'template' => $this->template)); ?>
	<?php endif ?>
</div>