<?php

/**
* @version      $Id: zoo.php 115 2011-06-08 09:40:23Z progmist $
* @package      JCE Advlink
* @copyright    Copyright (C) 2010 Progmist. All rights reserved.
* @license      GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @author       progmist
*/

// no direct access
defined('_WF_EXT') or die('ERROR_403');

class ProgmistlinksZoo extends JObject
{
    var $_option = 'com_zoo';
    
    function __construct($options = array())
    {
        
    }
    
    function & getInstance()
    {
        static $instance;
        
        if (!is_object($instance)) {
            $instance = new ProgmistlinksZoo();
        }
        return $instance;
    }
    
    public function getOption()
    {
        return $this->_option;
    }
    
    function getList()
    {
        $wf = WFEditorPlugin::getInstance();
        
        $zooVer = ProgmistlinksZoo::getZooVersion();
        if ($wf->checkAccess('progmistlinks.zoo', 1) && $zooVer) {
            return '<li id="index.php?option=com_zoo"><div class="tree-row"><div class="tree-image"></div><span class="folder zoo nolink"><a href="javascript:;">' . JText::_('ZOO') . '</a></span></div></li>';
        }
    }
    
    function getZooVersion() {
        
        return 2; 
         
    }
    
    function getLinks($args)
    {       
        $zooVer = ProgmistlinksZoo::getZooVersion();
        if (!$zooVer) return false; // zoo not installed
                
        $methodName = 'getItems'.$zooVer;
        
        return ProgmistlinksZoo::$methodName($args);
    }
    
    
    function getTags($app_id) {
        
        $db = JFactory::getDBO();
        
        
        $res = array();
        
        $db->setQuery("select id from #__zoo_item where application_id=$app_id");
        
        $item_ids = $db->loadResultArray();
   
    
        if (count($item_ids)) {
            
            $item_ids = implode(',',$item_ids);
            
            $db->setQuery("select distinct(name) from #__zoo_tag where item_id in ($item_ids)");
            
            $res = $db->loadObjectList();
   
            $res = is_array($res) ? $res : array();
        
        }
        
        return $res;
        
    }
    
    function getItems2 ($args) {
        
        global $mainframe;  
        
        $apps   = ProgmistlinksZoo::_catalog();
        
        $items      = array();
        $task       = isset($args->task) ? $args->task : '';
        
        switch ($task) {
        default:
        
            if (count($apps)) {
                
                foreach ($apps as $app) {
                    $items[] = array(
                        'id'        => 'index.php?option=com_zoo&task=frontpage&app_id='.$app->id,
                        'url'       => ProgmistlinksZoo::_route('index.php?option=com_zoo&task=frontpage&app_id='.$app->id),
                        'name'      =>  $app->name,
                        'class'     =>  'folder zoo'
                    );
                }
                
            }
            
            break;
        case 'tags': {
         
            $tags = ProgmistlinksZoo::getTags($args->app_id);

            foreach ($tags as $tag) {
                $items[] = array(
                    'id'        =>  'index.php?option=com_zoo&task=tag&tag='.$tag->name.'&app_id='.$args->app_id,
                    'url'       =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=tag&tag='.$tag->name.'&app_id='.$args->app_id),
                    'name'      =>  $tag->name,
                    'class'     =>  'file zoo'
                );
            }
                       
            break;
        }
        case 'frontpage':           
            $categories = ProgmistlinksZoo::_category($args->app_id);
            
            foreach ($categories as $category) {
                $items[] = array(
                    'id'        =>  'index.php?option=com_zoo&task=category&app_id='.$args->app_id.'&category_id='.$category->id,
                    'url'       =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=category&category_id='.$category->id),
                    'name'      =>  $category->name . ' / ' . $category->alias,
                    'class'     =>  'folder zoo'
                );
            }
            
            $_items = ProgmistlinksZoo::_items($args->app_id, 0);
            
            if (count($_items)) {
                
                foreach ($_items as $item) {
                    $items[] = array(
                        'id'    => 'index.php?option=com_zoo&task=item&item_id='.$item->id,
                        'url'       =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=item&item_id='.$item->id),
                        'name'  => $item->name . ' / ' . $item->alias,
                        'class' => 'file'
                    );
                }
                
            }
            
            $items[] = array(
                        'id'        => 'index.php?option=com_zoo&task=tags&app_id='.$args->app_id,
                        'url'       =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=tags&app_id='.$args->app_id),
                        'name'      =>  'Tags',
                        'class'     =>  'folder zoo nolink'
                    );
            
            break;
        case 'category':
            $categories = ProgmistlinksZoo::_category($args->app_id, $args->category_id);
            
            if (count($categories)) {
                
                foreach ($categories as $category) {
                    $items[] = array(
                        'id'        =>  'index.php?option=com_zoo&task=category&app_id='.$args->app_id.'&category_id='.$category->id,
                        'url'       =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=category&category_id='.$category->id),
                        'name'      =>  $category->name . ' / ' . $category->alias,
                        'class'     =>  'folder zoo'
                    );
                }
            
            }
            
            $_items = ProgmistlinksZoo::_items($args->app_id, $args->category_id);
            
            if (count($_items)) {
                
                foreach ($_items as $item) {
                    $items[] = array(
                        'id'    => 'index.php?option=com_zoo&task=item&item_id='.$item->id,
                        'url'   =>  ProgmistlinksZoo::_route('index.php?option=com_zoo&task=item&item_id='.$item->id),
                        'name'  => $item->name . ' / ' . $item->alias,
                        'class' => 'file'
                    );
                }
                
            }
            break;

        }
        
        return $items;        
            
    }    
    
    function getAppIdByItem_id($item_id) {
        
        $db = JFactory::getDBO();
        
        $db->setQuery("select application_id from #__zoo_item where id=$item_id");
        
        $app_id = $db->loadResult();      
        
        return isset($app_id) ? $app_id : 0;  
             
    }
    
    function getAppIdByCategory_id($category_id) {
        
        $db = JFactory::getDBO();
        
        $db->setQuery("select application_id from #__zoo_category where id=$category_id");
        
        $app_id = $db->loadResult();      
        
        return isset($app_id) ? $app_id : 0; 
                
    }
    
    function _route2($link) {
        
        $uri = new JURI($link);
                
        $Itemid = '';
        
        $db = JFactory::getDBO();
        
        $task = $uri->getVar('task');
        $item_id = $uri->getVar('item_id');
        $category_id = $uri->getVar('ategory_id');
        $app_id = $uri->getVar('ategory_id');
        
        switch ($task) {
            
            case 'item': {
            
                $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=item' and params like '%item_id=$item_id%'");
                
                $Itemid = $db->loadResult();
                
                if ( !isset($Itemid) ) {
                    
                    $app_id = ProgmistlinksZoo::getAppIdByItem_id($item_id);
                    
                    if ( isset($app_id) ) {
                        
                        $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=frontpage' and params like '%application=$app_id%'");
                        
                        $Itemid = $db->loadResult();
                        
                    }    
                    
                } else {
                    
                    $uri->setVar('view', 'item');
                    
                    $uri->delVar('task');
                    
                    $uri->delVar('item_id');
                    
                }
                
                
                  
            } break;
            
            case 'category': {
                
                $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=category' and params like '%category=$category_id%'");
                
                $Itemid = $db->loadResult();
                
                if ( !isset($Itemid) ) {
                    
                    $app_id = ProgmistlinksZoo::getAppIdByCategory_id($category_id);
                    
                    if (isset($app_id)) {
                        
                        $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=frontpage' and params like '%application=$app_id%'");
                        
                        $Itemid = $db->loadResult();
                        
                    }    
                    
                } else {
                    
                    $uri->setVar('view', 'category');
                    
                    $uri->delVar('task');
                    
                    $uri->delVar('category_id');
                    
                }
                  
                
            }  break;
            
            case 'frontpage': {
                    
                $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=frontpage' and params like '%application=$app_id%'");
                
                $Itemid = $db->loadResult();
                        
                if (isset($Itemid)) {
                    
                    $uri->setVar('view', 'frontpage');
                    
                    $uri->delVar('task');
                    
                    $uri->delVar('app_id');
                    
                }
                
            }   break;   
            
            case 'tag': {
            
                $db->setQuery("select id from #__menu where link='index.php?option=com_zoo&view=frontpage' and params like '%application=$app_id%'");
                
                $Itemid = $db->loadResult();    
                
            } break;       
             
        }  
        
        $link = $Itemid ? $uri->toString().'&Itemid='.$Itemid : $link;
        
        return $link;   
            
    }

    function _route($link)
    {
        
        $zooVer = ProgmistlinksZoo::getZooVersion();
        
        $methodName = '_route'.$zooVer;
        
        return ProgmistlinksZoo::$methodName($link);
        
        
    }

    
    function _catalog2() {
        
        $db     =& JFactory::getDBO();
        
        $query = 'SELECT id, name'
        . ' FROM #__zoo_application'
        . ' ORDER BY name '
        ;

        $db->setQuery($query);
        
        $res = $db->loadObjectList();
               
        $res = $res ? $res : array();
        
        return $res;    
            
    }
    
    function _catalog()
    {
        
        $zooVer = ProgmistlinksZoo::getZooVersion();
        
        $methodName = '_catalog'.$zooVer;
        
        return ProgmistlinksZoo::$methodName();
        
        
    }
        
    function _category2($app_id, $category_id = 0) {
        
        $db = &JFactory::getDBO();
        
        $db->setQuery('select * from #__zoo_category where application_id='.(int)$app_id.' and parent='.(int)$category_id);

        $categories = $db->loadObjectList();
     
        return $categories ? $categories : array();      ;            
           
    }
    
    function _category($catalog_id, $category_id = 0){
        
        $zooVer = ProgmistlinksZoo::getZooVersion();
        
        $methodName = '_category'.$zooVer;
        
        return ProgmistlinksZoo::$methodName($catalog_id, $category_id);
        
    }
    
    
    function _items2($app_id, $category_id) {
        
        $db = &JFactory::getDBO();
        
        if (!$category_id) {
            
           $db->setQuery("select * from #__zoo_item as i, #__zoo_category_item as c"
                        ." where i.application_id=".(int)$app_id
                        ." and c.item_id=i.id"
           );
            
           $items = $db->loadObjectList('id');              
            
           $item_ids = array_keys($items);
           
           $item_ids = implode(',', $item_ids);
           
           $db->setQuery("select * from #__zoo_item as i"
                    ." where i.application_id=".(int)$app_id
                    ." and i.id not in (".$item_ids.")"
           );
           
           $items = $db->loadObjectList(); 
           
        } else {
            $q = "select * from #__zoo_item as i, #__zoo_category_item as c"
                        ." where i.application_id=".(int)$app_id
                        ." and c.item_id=i.id"
                        ." and c.category_id=".(int)$category_id;
    
            $db->setQuery($q);
            
            $items = $db->loadObjectList('id');         
            
        }

        return $items ? $items : array();

    }
    
    
    function _items($catalog_id, $category_id)
    {
        $zooVer = ProgmistlinksZoo::getZooVersion();
        $methodName = '_items'.$zooVer;
        
        return ProgmistlinksZoo::$methodName($catalog_id, $category_id);
    }


}

?>