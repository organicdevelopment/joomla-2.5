<?php
/**
 * Zoo SEF extension for Joomla!
 *
 * @author      $Author: Oli Griffiths @ Organic Development $
 * @copyright   Organic Development Ltd
 * @package     JoomSEF
 * @license     GNU/GPLv3 http://www.artio.net/license/gnu-general-public-license
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access.');

require_once dirname(__FILE__).'/sef.ext.extended.php';
require_once(JPATH_ADMINISTRATOR . '/components/com_zoo/config.php');

class SefExt_com_zoo extends SefExtExtended
{	
	public static $cats = array();

	public function beforeCreate(&$uri){
		parent::beforeCreate($uri);
		
		//Set the app
		$this->app = App::getInstance('zoo');
		$this->app_id 		= (int) $uri->getVar('app_id');
		$this->item_id 		= (int) $uri->getVar('item_id');
		$this->submission_id= (int) $uri->getVar('submission_id');
		$this->cat_id	 	= (int) $uri->getVar('category_id');		
				
		//Reset all ids to ensure they're integers
		if($this->item_id) $uri->setVar('item_id', $this->item_id);
		if($this->submission_id) $uri->setVar('submission_id', $this->submission_id);
		if($this->cat_id) $uri->setVar('category_id', $this->cat_id);
		if($this->app_id) $uri->setVar('app_id', $this->app_id);
		
		//Set view if not set, remap task to view
		if($this->task && !$this->view || $this->task == $this->view){
			$this->view = $this->task;
			$this->task = null;
			$this->uri->setVar('view', $this->view);
			$this->uri->delVar('task');
		}
				
		//unset task if view is set
		if($this->view){
			$this->uri->delVar('task');
			$this->uri->setVar('view', $this->view);
		}
				
		//If we have an itemid, attempt to get the item,cat and app id from url
		if($this->itemid){
			$menu =& JSite::getMenu();
			$menuparams = $menu->getParams($this->itemid);
			$this->view = $this->view ? $this->view : $menuparams->get('view');
			$this->layout = $this->layout ? $this->layout : $menuparams->get('layout');
					
			switch($this->view){
				
				case 'item':
					if(!$this->item_id && $id = $menuparams->get('item_id')){
						$this->item_id = (int) $id;
						$this->view = 'item';
						$uri->setVar('view', $this->view);
						$uri->setVar('item_id', $this->item_id);
					}
					break;
				
				case 'feed':
				case 'category':
					if(!$this->cat_id && $id = $menuparams->get('category')){
						$this->cat_id = (int) $id;
						$this->view = 'category';
						$uri->setVar('view', $this->view);
						$uri->setVar('category_id', $this->cat_id);
					}
					
					break;
					
				case 'submission':
					if(!$this->submission_id && $id = $menuparams->get('submission')){
						$this->submission_id = (int) $id;
						$this->view = 'submission';
						$uri->setVar('view', $this->view);
						$uri->setVar('submission_id', $this->submission_id);
					}
					break;
					
				case '':
				case 'frontpage':
					if(!$this->app_id && $id = $menuparams->get('application')){
						$this->app_id = (int) $id;
						$this->view = 'frontpage';
						$uri->setVar('view', $this->view);
						$uri->setVar('app_id', $this->app_id);
					}
					break;	
			}			
		}

		//Set the app id for specific views
		switch($this->view){
			case 'item':
					$this->item = $this->app->table->item->get($this->item_id);
					if($this->item) $this->app_id = (int) $this->item->application_id;
				break;
				
			case 'submission':
					$this->submission = $this->app->table->submission->get($this->submission_id);
					if($this->submission) $this->app_id = (int) $this->submission->application_id;
				break;
				
			case 'feed':
			case 'category':
					$this->category = $this->app->table->category->get($this->cat_id);
					if($this->category) $this->app_id = (int) $this->category->application_id;
				break;
		}
		
		//Set the application
		if($this->app_id){
			$this->application = $this->app->table->application->get($this->app_id);
		}
			
		
		//Check if we're forcing the itemid for this application
		$app_itemids = explode("\n",$this->params->get('app_item_ids'));
		foreach($app_itemids AS $ids){
			$id = explode(':', $ids);
			$app_id = trim($id[0]);
			$item_id = isset($id[1]) ? trim($id[1]) : null;
			if($app_id == $this->app_id && $item_id){
				$this->itemid = $item_id;
				$this->uri->setVar('Itemid', $this->itemid);
			}
		}	
		
		//hide the layout from the url
		$this->layout = '';
		$this->uri->delVar('layout');

	}
	
	/**
	 * Some pre processing for the create function
	 */
	public function createStart(){

		$this->addApplication();
		
		$this->includeVar('app_id');
		$this->includeVar('view');
	}
	
	/**
	 * Add the title to the path
	 */
	protected function addTitle($object, $type){		
		if(!$object) return;
				
		$this->addPart($type.'_id', $this->getTitleOrAlias($object, $type));		
	}
	
	
	/**
	 * Gets the title or the alias tag from the object
	 */
	protected function getTitleOrAlias($object, $type){
		
		$use = $this->params->get('title_'.$type.'_alias', -1);
		$use = $use == -1 ? $this->config->useAlias : $use;
		
		switch($use){
			default:
			case 0:
				return $object->name;
			break;
			
			case 1:
				return $this->app->alias->$type->translateIDToAlias((int)$object->id);		
			break;
		}
	}
		
	
	/**
	 * Add the application to the url
	 */
	protected function addApplication($paramname = '', $default = false){
		if(!isset($this->app_added)) $this->app_added = false;
		
		if($this->app_added) return;
		
		//Show application name?
		$hide_application = explode(',', trim($this->params->get('hide_application')));
		if(isset($this->application) && ($this->params->get('show_application', 1) || $this->params->get($paramname, $default)) && (count($hide_application) > 0 && !in_array($this->app_id, $hide_application))){
			$this->app_added = true;			
			$this->addTitle($this->application,'application');
		}
	}
	
	/**
	 * Build the category pathway
	 */
	protected function buildCategoryPathway($category){
		
		if(!$category) return;
		
		//Get the app categories
		if(!isset(self::$cats[$category->application_id])){
			self::$cats[$category->application_id] = $this->app->table->category->getAll($category->application_id);
			$this->app->tree->build(self::$cats[$category->application_id], 'Category');
		}
		
		//Get the new category
		$category =  self::$cats[$category->application_id][$category->id];
	
		//Add full pathway
		if($this->params->get('show_category_pathway', 1) && $category){					
			$pathway = $category->getPathway();
			foreach($pathway AS $path){
				if($path->id != $category->id) $this->addTitle($path, 'category');
			}
		}
		
		$this->addTitle($category, 'category');
	}

	
	/**
	 * Create view for frontpage
	 */
	public function createViewFrontpage(){		
		if($this->params->get('show_application', 1) || $this->params->get('show_application_frontpage', 1)) $this->addApplication('show_application_frontpage', 1);
		if($this->params->get('show_view_frontpage', 0)) $this->addPart('view', $this->params->get('text_'.$this->view, $this->view ));
	}
	
	
	/**
	 * Alpha index view
	 */
	public function createViewAlphaindex(){
		$this->addApplication('show_application_alphaindex', 1);
		
		$text = $this->params->get('text_view_alphaindex');
		if($this->params->get('show_view_alphaindex', 0)) $this->addPart('view',$text ? $text : 'alphaindex');
		$this->addPart('alpha_char',$this->uri->getVar('alpha_char'));		
	}
	
	/**
	 * Submission view
	 */
	public function createLayoutSubmissionSubmission(){		
		
		$this->addApplication('show_application_submission', 1);		
		
		$text = $this->params->get('text_submission');
		if($this->params->get('show_view_submission', 0)) $this->addPart('view',$text ? $text : 'submission');
		$this->addPart('submission_id', $this->submission->name );
		$this->includeVar('layout');
	}
	

	/**
	 * My Submissions view
	 */
	public function createLayoutSubmissionMysubmissions(){
		$this->addApplication('show_application_submission', 1);	

		$view_text = $this->params->get('text_submission');
		$layout_text = $this->params->get('text_mysubmission');
		if($this->params->get('show_view_mysubmission', 0)) $this->addPart('view',$view_text ? $view_text : 'submission');
		if(isset($this->submission)) $this->addPart('submission_id', $this->submission->name );
		$this->addPart('layout',$layout_text ? $layout_text : 'my-submission');
	}
	
	/**
	 * createView the tag url
	 * @return 
	 */
	public function createViewTag(){		
		$this->addApplication('show_application_tag', 1);
		
		$this->addPart( 'view', $this->params->get('text_'.$this->view, $this->view ));
		$this->addPart('tag', $this->uri->getVar('tag'));
	}

	/**
	 * createView the feed url
	 * @return 
	 */
	public function createViewFeed(){

		if($this->cat_id){
			$this->category = $this->app->table->category->get((int)$this->cat_id);			
			$this->buildCategoryPathway($this->category);
		
		}else if(!$this->params->get('show_application', 1)){
			$this->addPart('app_id', $this->addApplication('', true));
		}
		
		$this->includeVar('format');
		$this->addPart('type', $this->uri->getVar('type'));
	}
	
	
	
	/**
	 * createView the category url
	 * @return 
	 */
	public function createViewCategory(){
		$this->addApplication('show_application_category', 0);		
		
		$this->removePart('view');
		
		//Are we showing the primary category
		$hide_app_category = explode(',',$this->params->get('hide_app_category'));
		$hide_category = explode(',',$this->params->get('hide_category'));
		if(!in_array($this->app_id, $hide_app_category) && !in_array($this->cat_id, $hide_category)){						
			$this->buildCategoryPathway($this->category);
		}	
		
		$this->includeVar('cat_id');
	}
	
	/**
	 * createView the itrm url
	 * @return 
	 */
	public function createViewItem(){
		$this->addApplication('show_application_item', 0);
		
		
		//Are we showing the item type
		if ($this->params->get('show_item_type')) {
			$this->addPart($this->item->type);
		}
		
		//Are we showing the primary category
		$hide_app_category = explode(',',$this->params->get('hide_app_category'));
		$hide_category = explode(',',$this->params->get('hide_category'));
		$primary_cat = 0;
		
		//Are we showing the category
		if(($show_cat = $this->params->get('show_item_category', 1)) && !in_array($this->app_id, $hide_app_category)){

			//Get the primary category
			$primary_cat = $this->item->getPrimaryCategoryId();
			$this->cat_id = $this->cat_id ? $this->cat_id : $primary_cat;
			
			//If we're only showing the primary cat and cat id is not the same as the one supplied, add it do nonsef vars
			if($show_cat == 1){								
				if($this->cat_id && $this->cat_id != $primary_cat) $this->nonSefVar($this->cat_id);
			}
			
			//If the category id is the same as the primary, remove it
			if($this->cat_id && $this->cat_id == $primary_cat) $this->uri->delVar('category_id');
					
			//Check if category is hidden, if not add pathway
			if(!in_array($this->cat_id, $hide_category) && $this->cat_id){
				$this->category = $this->item->app->table->category->get($this->cat_id);
				$this->buildCategoryPathway($this->category);
			}
		}
		
		//Add the item title
		$this->addTitle($this->item, 'item');				
	}
}


