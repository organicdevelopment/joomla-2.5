<?php
/**
 * SEF Extended extension for Joomla!
 *
 * @author      $Author: Organic Development
 * @copyright   Organic Development Ltd.
 * @package     JoomSEF
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access.');

require_once dirname(__FILE__).'/../sef.ext.php';
abstract class SefExtExtended extends SefExt
{
	public $parts = array();
	public $includeVars= array('task');
	public $ignoreVars= array('option','Itemid');
	public $config;
	public $params;
	public $option;
	public $controller;
	public $view;	
	public $layout;
	public $task;
	public $id;
	public $limit;
	public $limitstart;
	public $lang;
	public $lookup = true;
	public static $lookups = array();
	
	protected $view_default = '';
	protected $layout_default = 'default';
	
	/**
	 * Clean ids like id:xxx
	 */
	protected function cleanId($id){
		$id = is_array($id) ? $id : explode(':', $id);
		return (int) $id[0];
	}
	
	
	/**
	 * Adds a variable to the include array
	 * @param string $var
	 */
	public function includeVar($var = null){
		if($var) $this->includeVars[$var] = $var;
	}
	
	/**
	 * Adds a variable to the ignore array
	 * @param string $var
	 */
	public function ignoreVar($var = null){
		if($var) $this->ignoreVars[$var] = $var;
	}
	
	/**
	 * Adds a variable to the nonsef vars array
	 * @param string $var
	 */
	public function nonSefVar($var, $value = ''){
		$this->nonSefVars[$var] = $value;
	}
	
	
	/**
	 * Adds a variable to the title array and include array
	 * Also looks up in the plugin params for an alternative string
	 * @param string $var
	 */
	public function addPart( $var, $part = '', $lang_lookup = 1){
		static $lang;
		
		//Include the variable
		$this->includeVar($var);
		
		//Lookup param string for title
		if($lang_lookup && $part){
			//Load language object			
			if(!$lang){
				$lang =& JFactory::getLanguage();
				$lang->load($this->option);
			}
			$option = strtoupper(preg_replace('/com_/','',$this->option));
			
			//Check if there is a language translation for this view
			$key = $option.'_SEF_'.strtoupper($var).'_'.strtoupper($part);
			$sef_title = $lang->hasKey($key) ? $lang->_($key) : '';
			
			//If there is, use that, if not see if there is a param translation,if not use the view var
			$part = $sef_title ? $sef_title : $this->params->get($var.'_'.$part, $part);					
		}
		
		if($part) $this->parts[] = $part;
	}
	
	/**
	 * Remove a part from the array
	 */
	public function removePart($var){
		$value = $this->uri->getVar($var);
		$key = array_search($value, $this->parts);
		if($key !== false){
			unset($this->parts[$key]);
			$this->parts = array_merge(array(), $this->parts);
		} 		
	}
	
	
	
	/**
	 * Pre processing to extract item,cat,app id form url
	 * @param object $uri
	 * @return 
	 */
	public function beforeCreate(&$uri){
		
    	//Get the config
    	$this->config =& SEFConfig::getConfig();

        // Get key uri parts.	
		$this->uri			=& $uri;	
		$this->option 		= $uri->getVar('option');
		$this->controller 	= $uri->getVar('controller');		
		$this->layout 		= $uri->getVar('layout');
		$this->task 		= $uri->getVar('task');		
		$this->view 		= $uri->getVar('view', $this->task);
		$this->id 			= $this->cleanId($uri->getVar('id'));
		if($this->id)		$uri->setVar('id', $this->id);
		$cid = (array) $uri->getVar('cid');
		$this->uri 			=& $uri;
		$this->limit	 	= $uri->getVar('list');
		$this->limitstart	= $uri->getVar('limitstart');
		$this->lang			= $uri->getVar('lang');
		$this->itemid		= $uri->getVar('Itemid');
		
		/**
		 * Controller & View Handling
		 **/
		
		//If no controller and view, default controller is homepage
		if(!$this->controller && !$this->view){
			$this->view = $this->controller = $this->params->get('view_default', $this->view_default);
			if($this->view) $uri->setVar('view',$this->view);
			
		//If controller is set and view isnt, set the view to the controller
		} else if($this->controller && !$this->view){
			$this->view = $this->controller;	
			$uri->setVar('view', $this->view);			
			$uri->delVar('controller');
		
		//If controller and view are the same, unset controller
		}else if($this->controller == $this->view){
			$uri->delVar('controller');	
		}
		
		/**
		 * Task & layout handling
		 **/
		//If layout is form and no task is set, set the task according to $this->id
		if($this->layout == 'form' && !$this->task){
			//Set the task
			if($this->id) $this->task = 'edit';
			else $this->task = 'add';
			//Remove layout 
			$this->layout = null;
			$uri->delVar('layout');
			$uri->setVar('task', $this->task);
		}
		
		//Get the params
		$this->params = SEFTools::getExtParams($this->option);		
		
		//Parse the lookups
		$this->parseLookups();
		
		//Add title element
		$this->addPart('jmenu', JoomSEF::_getMenuTitle($this->option, $this->task, $this->itemid));
	}
	
	
	/**
	 * Parse the lookup info from the XML file
	 */
	public function parseLookups(){
		static $lookups_parsed;
		
		if($lookups_parsed) return;
		
		$xml = & SEFTools::getExtXML($this->option);

		//Check for a lookups node
		if(isset($xml->document->lookups[0])){

			//Loop through lookups
			foreach($xml->document->lookups[0]->children() AS $lookup){
				
				//Get the attributes
				$attribs = $lookup->attributes();

				//Assign required items
				$required = array('table','key','value');				
				$tmp = new stdClass;
				foreach($required AS $k){
					$tmp->$k = JArrayHelper::getValue($attribs, $k);
					unset($attribs[$k]);
				}

				//Check required items exist
				if(!$tmp->table || !$tmp->key || !$tmp->value) continue;
				
				//Grab the ID variable
				$tmp->id = JArrayHelper::getValue($attribs, 'id', $tmp->key);				
				unset($attribs['id']);
				
				//If there are no more vars, theres nothing to match
				if(empty($attribs)) continue;
				
				//Parse vars
				$tmp->vars = $attribs;				
				
				//Add to lookups array
				if(isset($tmp->vars['view'])){
					if(!isset(self::$lookups['views'][$tmp->vars['view']])) self::$lookups[$tmp->vars['view']] = array();
					
					if(isset($tmp->vars['layout'])){
						self::$lookups[$tmp->vars['view']][$tmp->vars['layout']] = $tmp;
					}else{
						self::$lookups[$tmp->vars['view']][] = $tmp;
					}
				}
				else self::$lookups[] = $tmp;				
			}
		}
		$lookups_parsed = true;
	}
	
	
	/**
	 * Lookup attempts to match the request arguments with
	 * lookup parameters parsed from earlier
	 * If a match if found, the value is looked up from the DB
	 */
	public function lookup(){
		static $lookups = array(), $looked_up = array();
		
		$vars = $this->uri->getQuery(true);
		
		//Check we have lookups
		if(!count(self::$lookups)) return;		
		unset($vars['option']);
		
		$var_hash = md5(serialize($vars));
		
		//Have we looked this url up before?
		if(!isset($lookups[$var_hash])){
		
			$lookup = null;
			
			//Check if we have a view specific lookup
			if($this->view && isset(self::$lookups[$this->view])){				

				//Check if we have a view and layout specific lookup
				if($this->layout && isset(self::$lookups[$this->view][$this->layout])){
					$lookup = self::$lookups[$this->view][$this->layout];
				}else{
					//If no layout is supplied, loop through lookups and attempt to match
					foreach(self::$lookups[$this->view] AS $k => $v){
						if(is_int($k)){
							$matches = array_diff_assoc($v->vars, $vars);
							if(count($matches) == 0){
								$lookup = $v;
								break;
							}
						}
					}
				}								
				
			}else{
				//if no view found loop through lookups and attempt to match
				foreach(self::$lookups AS $k => $v){
					if(is_int($k)){
						$matches = array_diff_assoc($v->vars, $vars);
						if(count($matches) == 0){
							$lookup = $v;
							break;
						}
					}
				}
			}
			
			//Add lookup
			$lookups[$var_hash] = $lookup;
		}else{
			$lookup = $looked_up[$var_hash];
		}
		
		
		//If we found a lookup record, look it up in the DB
		if($lookup && isset($lookup->value) && isset($lookup->table) && isset($lookup->key)){
			$lookup_hash = md5(serialize($lookup));
			
			//Have we already done this lookup
			if(!isset($looked_up[$lookup_hash])){
				$id = $this->uri->getVar($lookup->id);
				$db =& JFactory::getDBO();			
				$db->setQuery(sprintf("SELECT `%s` FROM `%s` WHERE `%s` = '%s' LIMIT 1", $lookup->value, $lookup->table, $lookup->key, $id));
				$result = $db->loadResult();
				if($error = $db->getErrorMsg()){
					JError::raiseWarning(500, JText::sprintf('SEF Lookup Error:: %s::%s()', __CLASS__, __FUNCTION__));
					JError::raiseWarning(500, JText::sprintf('SEF Lookup Error::%s', $error));
				}
			}else{
				$result = $looked_up[$lookup_hash];
			}
			//Add result if one retrieved
			if($result){
				$this->addPart($lookup->id, $result);
			}
		}
	}
	
	protected function createStart(){}
	protected function createEnd(){}	
	
	
	/**
	 * Main sef create function. This builds the url according to the rules below
	 * @param object $uri
	 * @return object $uri
	 */
    public function create(&$uri)
    {
    	//Before Processling
    	$this->createStart();
    	
		//If controller is not equal to view then add controller to url
		if($this->controller != $this->view){
			$this->addPart( 'controller', $this->controller );
		}

		/**
		 * Dynamic method call to a method called createLayout$this->view$this->layout() eg createLayoutContactsAll()
		 * This allows for view specific functions to add to the url
		 **/
	
		if($this->view || $this->layout){
			//Construct the method for the layout
			$method = $this->layout && $this->params->get('show_layout', 1) && $this->view ? 'createLayout'.ucfirst($this->view).ucfirst($this->layout) : null;
			if($method && method_exists($this, $method)) $this->$method();
			else{
				
				/**
				 * Dynamic method call to a method called createView$this->view() eg createViewContacts()
				 * This allows for view specific functions to add to the url
				 **/ 		
				if($this->view && $this->params->get('show_view', 1)){	
					$method = 'createView'.ucfirst($this->view);
					if(method_exists($this, $method)) $this->$method();
					else if($this->params->get('show_view_'.$this->view, 1)) $this->addPart( 'view', $this->params->get('text_'.$this->view, $this->view ));
				}	
			 		
				//Remove the layout if it matches the default
				if($this->layout == $this->layout_default){
					$this->uri->delVar('layout');
				
				//Else Add the layout part to the url
				}else if($this->params->get('show_layout_'.$this->view.'_'.$this->layout, 1)){
					$this->addPart( 'layout', $this->layout );
				}
			}
		}		
		

		/**
		 * Loop through vars and exclude the nonsef vars
		 */		
		foreach($this->uri->getQuery(true) AS $k => $v){			
			if(!in_array($k, $this->includeVars) && !in_array($k, $this->ignoreVars) && $k != 'lang'){
				$this->nonSefVars[$k] = $v;
			}			
		}			
		
		//Attempt to lookup
		if($this->lookup) $this->lookup();
		
		//After processing
		$this->createEnd();

		//Create new uri object
        $newUri = $this->uri;
        if (count($this->parts) > 0) {
            //Get the title
            $newUri = JoomSEF::_sefGetLocation($this->uri, $this->parts, $this->task, $this->limit, $this->limitstart, $this->lang, $this->nonSefVars, $this->ignoreVars);
        }    
        
        return $newUri;
    }
}

