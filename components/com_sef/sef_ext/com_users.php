<?php
/**
 * SEF component for Joomla!
 * 
 * @package   JoomSEF
 * @version   4.2.8
 * @author    ARTIO s.r.o., http://www.artio.net
 * @copyright Copyright (C) 2012 ARTIO s.r.o. 
 * @license   GNU/GPLv3 http://www.artio.net/license/gnu-general-public-license
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access.');

class SefExt_com_users extends SefExt
{
    public function getNonSefVars(&$uri)
    {
        $this->_createNonSefVars($uri);

        return array($this->nonSefVars, $this->ignoreVars);
    }

    protected function _createNonSefVars(&$uri)
    {
        if (!isset($this->nonSefVars) && !isset($this->ignoreVars)) {
            $this->nonSefVars = array();
        	$this->ignoreVars = array();
        }

        if (!is_null($uri->getVar('token'))) {
            $this->nonSefVars['token'] = $uri->getVar('token');
        }
        if(!is_null($uri->getVar('return'))) {
        	$this->nonSefVars['return']=$uri->getVar('return');
        }
    }

    function GetUserName($id)
    {
        $user = JUser::getInstance($id);
        
        return $user->username;
    }
    
    function create(&$uri)
    {
        $vars = $uri->getQuery(true);
        extract($vars);
        $this->_createNonSefVars($uri);
        
        $title = array();
        $title[] = JoomSEF::_getMenuTitleLang(@$option, $lang, @$Itemid);

        if (!empty($view)) {
            $title[] = JText::_('COM_SEF_USERS_'.$view);
        }
        
        if (!empty($layout)) {
            $title[] = JText::_('COM_SEF_USERS_'.$layout);
        }
        
        if (!empty($task)) {
            $tasks = explode('.', $task);
            
            if ($tasks[0] == 'profile') {
                if (isset($user_id)) {
                    $title[] = $this->GetUserName($user_id);
                }
            }
            else {
                $title[] = JText::_('COM_SEF_USERS_'.$tasks[0]);
            }
            
            if (isset($tasks[1])) {
                if ($tasks[1] == 'remind') {
                    $tasks[1] = 'submit';
                }
                
                $title[] = JText::_('COM_SEF_USERS_'.$tasks[1]);
                
                if (in_array($tasks[1], array('confirm', 'complete'))) {
                    $title[] = JText::_('COM_SEF_USERS_SUBMIT');
                }
            }
        }
        
        $newUri = $uri;
        if (count($title) > 0) {
            $newUri = JoomSEF::_sefGetLocation($uri, $title, null, null, null, @$lang, $this->nonSefVars);
        }
        
        return $newUri;
    }

}
?>
