<?php
/**
* @package   ZOO Category
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

/*
	Class: ArchiveModuleHelper
		The archive module helper class
*/
class ArchiveModuleHelper extends AppHelper 
{
	public function render($months, $params, $attribs)
	{
		$result = array("<ul $attribs>");
		foreach($months as $count => $date)
		{
			
			//Construct date
			$date = explode('-', $date);
			
			$month = $date[0];
			$year = $date[1];
			
			$date = mktime(0, 0, 0, $month, 1, $year);
			
			//Format dates
			$output_date = date($params->get('date_format'), $date); //Formatted for list output (HTML)
			$link_date = date('Y-m', $date); 	 //Formatted for the component link URL
			$current_date = date('Y-m', time()); //Current date for comparison
			
			//Construct class
			$current = $current_date == $link_date;
			$class = 'class="level1'.($current ? ' current' : '').'"';
			
			//Build link URL
			$url = JRoute::_('index.php?option=com_zoo&controller=archive&app_id='.$params->get('app_id').'&date='.$link_date);
			
			//Construct ouput HTML
			$result[] = "<li $class>";
			$result[] = "<a href=\"$url\" $class><span>$output_date</span></a>";
			$result[] = '</li>';
			
			//Leave foreach if limit is reached
			if($count == $params->get('limit')) { break; }
		}
		$result[] = '</ul>';
		
		return implode("\n", $result);
		
	}
}