<?php
/**
* @package   ZOO Category
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// load config
require_once(JPATH_ADMINISTRATOR.'/components/com_zoo/config.php');

// get app
$zoo = App::getInstance('zoo');

// load zoo frontend language file
$zoo->system->language->load('com_zoo');

// init vars
$path = dirname(__FILE__);

//register base path
$zoo->path->register($path, 'mod_zooarchive');

// register helpers
$zoo->path->register($path, 'helpers');
$zoo->loader->register('ArchiveModuleHelper', 'helpers:helper.php');

//Query the ZOO items table to retrieve months which have articles published in them
$db =& JFactory::getDBO();
$query = "SELECT DISTINCT DATE_FORMAT(A.publish_up, '%m-%Y') FROM ".$db->nameQuote('#__zoo_item'). " AS A".
		" WHERE A.state  = 1 AND" .
		" A.application_id = ". $params->get('app_id').
		" ORDER BY A.publish_up DESC" ;

$db->setQuery($query);

$months = $db->loadResultArray();

if ($months) {
	include(JModuleHelper::getLayoutPath('mod_zooarchive', $params->get('theme', 'list')));
}