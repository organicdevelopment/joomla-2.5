<?php
/**
 * Organic Development Footer Module
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.html.html');
?>
<div style='float: right; font-size: 0.8em; padding: 5px; clear: right' class="credit">
<?php
echo JHTML::link(
		$params->get('link', 'http://www.organic-development.com'),
		$params->get('text', 'Developed by Organic Development'), 
		array('title' => $params->get('link_title', 'Web Development, Search Engine Optimisation, SEO, Social Media by Organic Development - Devon. Organic-Development.com'))
	);
?>
</div>