<?php
/**
* @package   yoo_master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// init vars
$id				= $module->id;
$position		= $module->position;
$title			= JText::_($module->title);
$showtitle		= $module->showtitle;
$content		= $module->content;
$color_char		= '::';
$subtitle_char	= '||';
$title_template	= '<h3 class="module-title">%s</h3>';
$template 		= 'default-1';
$suffix			= '';
$style			= '';
$badge			= '';
$icon			= '';
$dropdownwidth	= '';

// extract params
//extract($params);

//Template is a reserved word so we use layout instead
if(isset($layout)) $template = $layout;

//Append suffix
$style .= ' '.$suffix;

//Set menu template
if ($module->position == 'menu'){
	$template = $module->menu ? 'raw' : 'dropdown';
	$showtitle = 0;
}

//Force hide title for raw templates
if($template == 'raw') $showtitle = 0;

// set badge if exists
if ($badge) {
	$badge = '<div class="badge badge-'.$badge.'"></div>';
}

// split title in two colors
$split_color = mb_strpos($title, $color_char);
if ($split_color !== false) {
	$title = '<span class="color">'.mb_substr($title, 0, $split_color).'</span>'.mb_substr($title, $split_color + strlen($color_char));
}

// create subtitle
$subtitle = mb_strpos($title, $subtitle_char);
if ($subtitle !== false) {
	$title = '<span class="title">'.mb_substr($title, 0, $subtitle).'</span><span class="subtitle">'.mb_substr($title, $subtitle + strlen($subtitle_char)).'</span>';
}


// create title icon if exists
if ($icon) {
	$title = '<span class="icon icon-'.$icon.'"></span>'.$title.'';
}

// create title template
if ($title_template) {
	$title = sprintf($title_template, $title);
}

// set dropdownwidth if exists
if ($dropdownwidth) {
	$dropdownwidth = 'style="width: '.$dropdownwidth.'px;"';
}

//Using advanced module manager, set module title to a link
if(isset($module->adv_params) && isset($adv_params->adv_params->extra1) && $adv_params->adv_params->extra1){
	$title = JHTML::link(JRoute::_($adv_params->adv_params->extra), $title);
}

// render menu
if ($module->menu) {

	// set menu renderer
	if (isset($params['menu'])) {
		$renderer = $params['menu'];
	} else if (in_array($module->position, array('menu'))) {
		$renderer = 'dropdown';
	} else if (in_array($module->position, array('toolbar-l', 'toolbar-r', 'footer'))) {
		$renderer = 'default';
	} else {
		$renderer = 'accordion';
	}

	// set menu style
	if ($renderer == 'dropdown') {
		$module->menu_style = 'menu-dropdown';
	} else if ($renderer == 'accordion') {
		$module->menu_style = 'menu-sidebar';
	} else if ($renderer == 'default') {
		$module->menu_style = 'menu-line';
	} else {
		$module->menu_style = null;
	}

	$content = $this['menu']->process($module, array_unique(array('pre', 'default', $renderer, 'post')));
}

// render module
echo $this->render("modules/templates/{$template}", compact('style', 'badge', 'showtitle', 'title', 'content', 'dropdownwidth', 'params'));

