<?php
/**
 * @version		$Id: default.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	mod_articles_categories
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<div id="supportframe">
<form action="<?php echo JRoute::_('index.php'); ?>?option=com_module&id=102" method="post" id="form-support">
	<fieldset class="supportform">
		<label id="mod-ticket-subject-lbl" for="mod-ticket-subject"><?php echo JText::_('Ticket Subject'); ?></label><br>
		<input name="ticket-subject" id="mod-ticket-subject" type="text" class="inputbox" size="15" />
		<br /><br />
		<label id="mod-ticket-body-lbl" for="mod-ticket-body"><?php echo JText::_('Please Describe The Problem'); ?></label><br />
		<textarea name="ticket-body" id="mod-ticket-body" class="textarea" ></textarea>
		<div class="clr"></div><br />
		<input type="submit" class="hidebtn right" value="<?php echo JText::_('Submit Ticket' ); ?>" />
		<input type="hidden" name="task" value="support" />
		<?php echo JHtml::_('form.token'); ?>
	</fieldset>
</form>
</div>
<script type="text/javascript">
window.addEvent('domready', function(){
	var submitSupportForm = function(e){
		e.stop();
		var request = new Form.Request('form-support', 'supportframe', {
			resetForm: false,
			onSuccess: function(){
			var link = $('supportframe').getElement('a');
			 	
			if(link){
				link.addEvent('click', function(e){   
						e.stop();
			 			$('supportframe').empty();
						form.clone(true,true).inject('supportframe');
			 			$('form-support').addEvent('submit', submitSupportForm);	 		
			 		})
			 	}
			 } 
		});
		request.send();
	}
	$('form-support').addEvent('submit', submitSupportForm);
	var form = $('form-support').clone(true,true);
});
</script>