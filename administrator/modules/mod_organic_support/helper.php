<?php
function sendSuportTicket()
{
	$values = JRequest::get('post');
	$mailer = getMailer();
	$mailer->setSender(getTicketSender());
	$mailer->addRecipient(getTcketRecipient());
	$mailer->setSubject($values['ticket-subject']);
	$mailer->setBody(getBody($values['ticket-body']));
	$send =& $mailer->Send();
	
	if ($send !== true){
	
		// Log mailer send error
		logError($send->message);
		
		// Display failed message to user
		return '<p>Sorry there was an error submitting your support ticket. 
				Please email your support request directly to '. JHtml::link('mailto:'.getTcketRecipient(), getTcketRecipient()). '</p>';

	}else{
		// Display success message to user
		return '<p>Thanks you for your support request <br>
				<br>A member of the our team will be<br>
				in touch as soon as possible.<br /><br />
				
				<a href="#">Click here to reset form</a>
				</p>';
	}
}

function getMailer()
{
	$mailer =& JFactory::getMailer();
	return $mailer;
}

function getTcketRecipient()
{
	$config =& JFactory::getConfig();
	return $config->getValue('config.support_mail');
	// return 'kyle@organic-development.com';
}

function getTicketSender()
{
	$user =& JFactory::getUser();
	return array($user->email, $user->name);
}

function getTicketSenderId()
{
	$user =& JFactory::getUser();
	return $user->id;
}

function getBody($body)
{
	$sender = getTicketSender();
	$sender_id = getTicketSenderId();
	$body = 	
"
Ticket Sender & Email
---------------------
($sender_id) {$sender[1]} [{$sender[0]}]


Ticket Message
--------------
$body
	
	
	
Debug Info	
----------
User-agent: {$_SERVER['HTTP_USER_AGENT']}
Remote Address: {$_SERVER['REMOTE_ADDR']}
Script Filename: {$_SERVER['SCRIPT_FILENAME']}
Request Method: {$_SERVER['REQUEST_METHOD']}
Request URI: {$_SERVER['REQUEST_URI']}


Post Dump: " .print_r($_POST, true). 
"

Get Dump: " . print_r($_GET, true);
				
				
	return $body;
}

function logError($error)
{
	// Get instance of error log.
	jimport('joomla.error.log');
	$log = &JLog::getInstance('mod_organic_support.log.php');

	// Add a log entry
	$log->addEntry(array('comment' => 'Organic support module mailer error occurred: '.$error));
}