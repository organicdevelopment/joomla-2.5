<?php
/**
 * @version		$Id: mod_property_search.php 20806 2011-02-21 19:44:59Z dextercowley $
 * @package		Organic.Property
 * @subpackage	mod_property_search
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the helper functions only once
require_once dirname(__FILE__).'/helper.php';

// Process the send request if form has been posted, else display the ticket form.
if(JRequest::getVar('task') == 'support'){
	echo sendSuportTicket();
}else{
	require JModuleHelper::getLayoutPath('mod_organic_support', $params->get('layout', 'default'));
}
