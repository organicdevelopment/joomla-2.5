<?php
/**
 * @package SmartIcons Module for Joomla! 1.6
 * @version $Id: default.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

// No direct access.
defined('_JEXEC') or die;

$buttons = SmartIconsHelper::getButtons();
$tab = null;
$tabStarted = false;
$lastButton = end($buttons);
$tabIndex = 0;
?>
<div id="cpanel"><?php
foreach ($buttons as $button){
	if(JFactory::getUser()->authorise('core.view', 'com_smarticons.category.'.$button->TabId)) {
		if($button->Tab != $tab) {
			if (!is_null($tab)) {
				$html = ob_get_clean();
				if(!$tabStarted) {
					echo JHtml::_('tabs.start', 'Tabs');
					echo JHtml::_('tabs.panel', $tab, $tabIndex++);
					echo $html;
					$html = '';
					$tabStarted = true;
				}
				echo $html;
				echo JHtml::_('tabs.panel', $button->Tab, $tabIndex++);
					
			}
			$tab = $button->Tab;
			ob_start();
		}
		echo SmartIconsHelper::button($button);
	}
	if ($button == $lastButton) {
		$html = ob_get_clean();
		echo $html;
	}

}
if ($tabStarted) {
	echo JHtml::_('tabs.end');;
}

?></div>
