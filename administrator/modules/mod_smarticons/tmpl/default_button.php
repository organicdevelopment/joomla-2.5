<?php
/**
 * @package SmartIcons Module for Joomla! 1.6
 * @version $Id: default_button.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// No direct access.
defined('_JEXEC') or die;
$style = "";
if (isset($button->params->bold) && $button->params->bold==1) {
	$style.= "font-weight:bold; ";
}
if (isset($button->params->italic) && $button->params->italic==1) {
	$style.= "font-style:italic; ";
}
if (isset($button->params->underline) && $button->params->underline==1) {
	$style.= "text-decoration:underline;";
}
if (isset($button->params->NewWindow) && $button->params->NewWindow==1) {
	$target = ' target="_blank"';
}

?>
<div class="icon-wrapper">
	<div class="icon">
		<a href="<?php echo $button->Target; ?>"<?php if(isset($target)) echo $target;?>>
		<?php if ($button->Display == 1 || $button->Display == 2) :?>
			<?php echo JHTML::_('image', $button->Icon, $button->Text, NULL); ?>
		<?php endif;?>
		<?php if ($button->Display == 1 || $button->Display == 3) :?>
			<span style="<?php echo $style;?>"><?php echo $button->Title; ?></span>
		<?php endif;?>
		</a>
	</div>
</div>