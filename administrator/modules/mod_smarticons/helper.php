<?php
/**
 * @package SmartIcons Module for Joomla! 1.6
 * @version $Id: helper.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

// No direct access.
defined('_JEXEC') or die;

abstract class SmartIconsHelper
{
	/**
	 * Stack to hold default buttons
	 *
	 * @since	1.6
	 */
	protected static $buttons = array();

	/**
	 * Helper method to generate a button in administrator panel
	 *
	 * @param	array	A named array with keys link, image, text, access and imagePath
	 *
	 * @return	string	HTML for button
	 * @since	1.6
	 */
	public static function button($button) {
		/*
		 * Verificari de acces pentru buton
		 */;
		$params = new JRegistry;
		$params->loadJSON($button->params);
		$button->params = $params->toObject();
		$task = array();
		$component = array();
		$access = array();
		$access[] = array('core.view', 'com_smarticons.icon.' . $button->idIcon);
		if (preg_match('/option=(\b[a-zA-Z0-9_]*)/', $button->Target, $component)) {
			$access[] = array('core.manage', $component[1]);
		}
		if (preg_match('/task=(\b[a-zA-Z0-9\.]*\b)/', $button->Target, $task)) {
			$task = explode('.',$task[1]);
			switch ($task[1]) {
				case 'add':
					$access[] = array('core.create', $component[1]);
					break;
				case 'edit':
					$access[] = array('core.edit', $component[1]);
					break;
				default:
					break;
			}
		}
		foreach ($access as $permision) {
			if(!JFactory::getUser()->authorise($permision[0], $permision[1])) {
				return '';
			}
		};
		ob_start();
		require JModuleHelper::getLayoutPath('mod_smarticons', 'default_button');
		$html = ob_get_clean();
		return $html;
	}

	/**
	 * Helper method to return button list.
	 *
	 * This method returns the array by reference so it can be
	 * used to add custom buttons or remove default ones.
	 *
	 * @return	array	An array of buttons
	 * @since	1.6
	 */
	public static function &getButtons() {
		if (empty(self::$buttons)) {
			$db =&JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->select('Tab.title AS Tab, Tab.id as TabId, Icon.idIcon, Icon.Title, Icon.Text, Icon.Target, Icon.Icon, Icon.Display, Icon.params');
			$query->from('#__com_smarticons AS Icon');
			$query->innerjoin('#__categories AS Tab ON Icon.catid = Tab.id');
			$query->where('Icon.published = 1');
			$query->where('Tab.published = 1');
			$query->order('Tab.lft, Icon.ordering');

			$db->setQuery($query);

			if($icons = $db->loadObjectList()) {
				self::$buttons = $icons;
			};
		}
		return self::$buttons;
	}
}
