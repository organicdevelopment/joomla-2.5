<?php
/**
 * SEF component for Joomla!
 * 
 * @package   JoomSEF
 * @version   4.2.8
 * @author    ARTIO s.r.o., http://www.artio.net
 * @copyright Copyright (C) 2012 ARTIO s.r.o. 
 * @license   GNU/GPLv3 http://www.artio.net/license/gnu-general-public-license
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport('joomla.application.component.model');

class SEFModel extends JModel
{
    protected function booleanRadio($name, $attrs, $selected)
    {
        $html  = '<fieldset class="radio">';
        $html .= JHTML::_('select.booleanlist', $name, $attrs, $selected);
        $html .= '</fieldset>';
        
        return $html;
    }
}
?>