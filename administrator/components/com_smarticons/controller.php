<?php
/**
 * @package SmartIcons Component for Joomla! 1.6
 * @version $Id: controller.php 4 2011-03-05 00:34:25Z Bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

class SmartIconsController extends JController {
	
	function display($cachable = false)
	{
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'Icons'));

		// call parent behavior
		parent::display($cachable);
		
		//Add sub-menu
		SmartIconsHelper::addSubmenu('icons');
	}
}