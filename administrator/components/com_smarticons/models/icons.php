<?php
/**
 * @package SmartIcons Component for Joomla! 1.6
 * @version $Id: icons.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.modellist' );

class SmartIconsModelIcons extends JModelList {
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'Icon.idIcon', 'Icon.ordering',
				'Icon.Title', 'CategoryTitle',
				'Icon.Display', 'Icon.published'
			);
		}

		parent::__construct($config);
	}
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select icon fields
		$query->select('Icon.idIcon, Icon.asset_id, Icon.catid, Icon.Title, Icon.Target, Icon.Display');
		$query->select('Icon.published, Icon.ordering, Icon.checked_out, Icon.checked_out_time');
		// From the icon table
		$query->from('#__com_smarticons AS Icon');
		
		//Select category fields
		$query->select('Category.title AS CategoryTitle');
		//Join with categories
		$query->join('LEFT OUTER', '#__categories AS Category ON Icon.catid = Category.id');

		//Select the checked out user
		$query->select('User.name AS editor');
		//Join with the users table
		$query->join('LEFT OUTER', '#__users AS User ON Icon.checked_out = User.id');
		
		// Add the list ordering clause.
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		if ($orderCol == 'Icon.ordering' || $orderCol == 'Category.title') {
			$orderCol = 'Category.title '.$orderDirn.', Icon.ordering';
		}
		$query->order($db->getEscaped($orderCol.' '.$orderDirn));
		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		// List state information.
		parent::populateState('Icon.ordering', 'ASC');
	}
}
