<?php
/**
 * @package SmartIcons Component for Joomla! 1.6
 * @version $Id: view.html.php 4 2011-03-05 00:34:25Z Bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class SmartIconsViewIcons extends JView {
	function display($tpl = null) {
		$this->icons = $this->get('Items');
		$this->pagination = $this->get('pagination');
		$this->state = $this->get('State');

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}
	protected function addToolBar()
	{
		$canDo = SmartIconsHelper::getActions();
		JToolBarHelper::title(JText::_('COM_SMARTICONS_MANAGER_ICONS'), 'smarticons');
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('icon.add', 'JTOOLBAR_NEW');
		}
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('icon.edit', 'JTOOLBAR_EDIT');
		}
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::custom('icons.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
			JToolBarHelper::custom('icons.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::custom('icons.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			JToolBarHelper::divider();
		}

		if ($canDo->get('core.delete')) {
			JToolBarHelper::deleteList('', 'icons.delete', 'JTOOLBAR_DELETE');
			JToolBarHelper::divider();
		}
		if ($canDo->get('core.admin')) {
				
			JToolBarHelper::preferences('com_smarticons');
		}
	}
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_SMARTICONS_ADMINISTRATION'));
	}
}