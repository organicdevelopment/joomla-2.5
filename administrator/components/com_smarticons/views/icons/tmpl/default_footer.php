<?php
/**
 * @package SmartIcons Component for Joomla! 1.6
 * @version $Id: default_footer.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die('Restricted access');
?>
<tr>
	<td colspan="8"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>
