<?php
/**
 * @package SmartIcons Component for Joomla! 1.6
 * @version $Id: default_button.php 8 2011-08-28 15:07:19Z bobo $
 * @author SUTA Bogdan-Ioan
 * @copyright (C) 2011 SUTA Bogdan-Ioan
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// No direct access
defined('_JEXEC') or die('Restricted access');

$style = "";
if (isset($button->params->bold) && $button->params->bold==1) {
	$style.= "font-weight:bold; ";
}
if (isset($button->params->italic) && $button->params->italic==1) {
	$style.= "font-style:italic; ";
}
if (isset($button->params->underline) && $button->params->underline==1) {
	$style.= "text-decoration:underline;";
}

?>
<div class="icon-wrapper">
	<div class="icon">
		<a id="icon_url" target="_blank" href="<?php echo $this->item->Target; ?>">
			<?php echo JHtml::_('image',$this->item->Icon, $this->item->Text, array('id' => "icon_image")); ?>
			<span id="icon_text" style="<?php echo $style;?>"><?php echo $this->item->Title; ?></span></a>
	</div>
</div>
