<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

/**
 * The cache helper class.
 *
 * @package Component.Helpers
 * @since 2.0
 */
class CacheHelper extends AppHelper {

	/**
	 * Creates an AppCache instance
	 *
	 * @param string $file Path to cache file
	 * @param boolean $hash Wether the key should be hashed
	 * @param int $lifetime The values lifetime
	 *
	 * @return AppCache
	 *
	 * @since 2.0
	 */
	public function create($file, $hash = true, $lifetime = null) {
		$cache = $this->app->object->create('AppCache', array($file, $hash, $lifetime));
		$this->app->zoo->putIndexFile(dirname($file));
		return $cache;
	}

}

/**
 * The cache class.
 *
 * @package Component.Helpers
 * @since 2.0
 */
class AppCache {

	/**
	 * Path to cache file
	 *
	 * @var string
	 * @since 2.0
	 */
	protected $_file = 'config.txt';

	/**
	 * Path to cache file
	 *
	 * @var array
	 * @since 2.0
	 */
	protected $_items = array();

	/**
	 * marks cache dirty
	 *
	 * @var boolean
	 * @since 2.0
	 */
	protected $_dirty = false;

	/**
	 * The cached items
	 *
	 * @var boolean
	 * @since 2.0
	 */
	protected $_hash = true;

	/**
	 * Class constructor
	 *
	 * @param string $file Path to cache file
	 * @param boolean $hash Wether the key should be hashed
	 * @param int $lifetime The values lifetime
	 * @since 2.0
	 */
	public function __construct($file, $hash = true, $lifetime = null) {

		// if cache file doesn't exist, create it
		if (!JFile::exists($file)) {
			JFolder::create(dirname($file));
			$buffer = '';
			JFile::write($file, $buffer);
		}

		// set file and parse it
		$this->_file = $file;
		$this->_hash = $hash;
		$this->_parse();

		// clear out of date values
		if ($lifetime) {
			$lifetime = (int) $lifetime;
			$remove = array();
			foreach ($this->_items as $key => $value) {
				if ((time() - $value['timestamp']) > $lifetime) {
					$remove[] = $key;
				}
			}
			foreach ($remove as $key) {
				unset($this->_items[$key]);
			}
		}
	}

	/**
	 * Check if the cache file is writable and readable
	 *
	 * @return boolean If the cache can be used
	 *
	 * @since 2.0
	 */
	public function check() {
		return is_readable($this->_file) && is_writable($this->_file);
	}

	/**
	 * Get a cache content
	 *
	 * @param  string $key The key
	 *
	 * @return mixed      The cache content
	 *
	 * @since 2.0
	 */
	public function get($key) {
		if ($this->_hash)
			$key = md5($key);
		if (!array_key_exists($key, $this->_items))
			return null;

		return $this->_items[$key]['value'];
	}

	/**
	 * Set a cache content
	 *
	 * @param string $key   The key
	 * @param mixed $value The value
	 *
	 * @return AppCache $this for chaining support
	 *
	 * @since 2.0
	 */
	public function set($key, $value) {
		if ($this->_hash)
			$key = md5($key);
		if (array_key_exists($key, $this->_items) && @$this->_items[$key]['value'] == $value)
			return $this;

		$this->_items[$key]['value'] = $value;
		$this->_items[$key]['timestamp'] = time();
		$this->_dirty = true;
		return $this;
	}

	/**
	 * Parse the cache file
	 *
	 * @return AppCache $this for chaining support
	 *
	 * @since 2.0
	 */
	protected function _parse() {
		$content = JFile::read($this->_file);
		if (!empty($content)) {
			$items = json_decode($content, true);
			if (is_array($items)) {
				$this->_items = $items;
			}
		}
		return $this;
	}

	/**
	 * Save the cache file if it was changed
	 *
	 * @return AppCache $this for chaining support
	 *
	 * @since 2.0
	 */
	public function save() {
		if ($this->_dirty) {
			JFile::write($this->_file, json_encode($this->_items));
		}
		return $this;
	}

	/**
	 * Clear the cache file
	 *
	 * @return AppCache $this for chaining support
	 */
	public function clear() {
		$this->_items = array();
		$this->_dirty = true;
		return $this;
	}

}

/**
 * AppCacheException identifies an Exception in the AppCache class
 * @see AppCache
 */
class AppCacheException extends AppException {}