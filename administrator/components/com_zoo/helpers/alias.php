<?php
/**
* @package   com_zoo
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

/**
 * The helper class for alias handling.
 *
 * @package Component.Helpers
 * @since 2.0
 */
class AliasHelper extends AppHelper {

	/**
	 * Contains an instance of an AppTable object
	 *
	 * @var AppTable
	 * @since 2.0
	 */
	public $table;

	/**
	 * Retrieve this helper with table set
	 *
	 * @param string $name Table name to set
	 *
	 * @return self
	 *
	 * @since 2.0
	 */
	public function __get($name) {
		if (in_array($name, array('application', 'category', 'item', 'submission'))) {
			$this->table = $this->app->table->$name;
		} else {
			throw new AliasHelperException(sprintf('Table class (%s) not found', $name));
		}
		return $this;
	}

	/**
	 * Translate object id to alias.
	 *
	 * @param string $id The object id
	 *
	 * @return string|null The object alias if found
	 *
	 * @since 2.0
	 */
	public function translateIDToAlias($id) {
		if (!is_numeric($id)) {
			return null;
		}

		// init vars
		$db = $this->app->database;

		// search alias
		$query = 'SELECT alias'
				.' FROM '.$this->table->name
				.' WHERE id = '.$db->Quote($id)
				.' LIMIT 1';

		return $db->queryResult($query);
	}

	/**
	 * Translate object alias to id.
	 *
	 * @param string $alias The object alias
	 *
	 * @return string The object id if found, or 0
	 *
	 * @since 2.0
	 */
	public function translateAliasToID($alias) {

		// init vars
		$db = $this->app->database;

		// search alias
		$query = 'SELECT id'
				.' FROM '.$this->table->name
				.' WHERE alias = '.$db->Quote($alias)
				.' LIMIT 1';

		return $db->queryResult($query);
	}

	/**
	 * Get unique object alias.
	 *
	 * @param string $id The object id
	 * @param string $alias The object alias
	 *
	 * @return string The unique object alias string
	 *
	 * @since 2.0
	 */
	public function getUniqueAlias($id, $alias = '') {

		if (empty($alias) && $id) {
			$alias = JFilterOutput::stringURLSafe($this->table->get($id)->name);
		}

		if (!empty($alias)) {
			$i = 2;
			$new_alias = $alias;
			while ($this->checkAliasExists($new_alias, $id)) {
				$new_alias = $alias.'-'.$i++;
			}
			return $new_alias;
		}

		return $alias;
	}

	/**
	 * Method to check if an alias already exists.
	 *
	 * @param string $alias The object alias
	 * @param string $id The object id
	 *
	 * @return string The object id if found, or 0
	 *
	 * @since 2.0
	 */
	public function checkAliasExists($alias, $id = 0) {

		$xid = intval($this->translateAliasToID($alias));
		if ($xid && $xid != intval($id)) {
			return true;
		}

		return false;
	}

}

/**
 * AliasHelperException identifies an Exception in the AliasHelper class
 * @see AliasHelper
 */
class AliasHelperException extends AppException {}