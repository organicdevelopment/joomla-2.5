# ************************************************************
# Sequel Pro SQL dump
# Version 3792
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.63-0ubuntu0.11.10.1)
# Database: joomla25
# Generation Time: 2012-08-31 10:32:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table #__advancedmodules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__advancedmodules`;

CREATE TABLE `#__advancedmodules` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`moduleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__advancedmodules` WRITE;
/*!40000 ALTER TABLE `#__advancedmodules` DISABLE KEYS */;

INSERT INTO `#__advancedmodules` (`moduleid`, `params`)
VALUES
	(1,'{\"assignto_menuitems\":0,\"assignto_menuitems_selection\":[]}');

/*!40000 ALTER TABLE `#__advancedmodules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__ak_acl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__ak_acl`;

CREATE TABLE `#__ak_acl` (
  `user_id` bigint(20) unsigned NOT NULL,
  `permissions` mediumtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__ak_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__ak_profiles`;

CREATE TABLE `#__ak_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `configuration` longtext,
  `filters` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__ak_profiles` WRITE;
/*!40000 ALTER TABLE `#__ak_profiles` DISABLE KEYS */;

INSERT INTO `#__ak_profiles` (`id`, `description`, `configuration`, `filters`)
VALUES
	(1,'Default Backup Profile','###AES128###LGHDpNf98QtcB1+zxMzHJB1ZdVSVkPX4AMF8muTkLx3BqqLhfsWfdlOCHCGo+xcHonsBYwzsF0Ug2Gq+PG9pnJB21evpwuebEs1XsTX/4hJcOIIQJAbqHn/nakBoiIHQP1XY1d0CWqzOixNnL/yVD8Wk22AjiyE7by65/ntvEc9khksOU/l4l+zegKJ36VMkTkWJICHHX8/XDbOimvtI70UMHG07sDCDO3sXd4XEhDK4l5TbovRH7QkwCYlndBMWA9TVRLeZcL/HIwOYK0TPJxH+nioPcdZvaykjVlwW/WVL9tKhWq6GnBVVVm/you5uWGHWcWNlQLqNRbq7cYH7vPUnWcHVyA7XPzCbIAeRAd0zyat1pZkCGNdV4rBhZ2fqzetkpWDbSm24glKwDZN605bbGXRAuP2VTruIQSsjs4j9KqkJDaNZWDr0sKQiLu/y5vWo+uZ9W2lg7H4wf5yy9SNiM3CAjXRM6N+bvdz0HtFZzJfHqhbtmm5P+tqsEXq+pb1eZLi+hqXk9zD7+IOTkDKNoujJgeRdJHtkrj3Gs9qp/LOXDpbZjGkoRD9oDqFoj7IrZzF+qhbUxl9DQganuK3M5JM3lOqZjyyo+hei0m6PAyMTgQAHYLyLGgn4xjtcpO1jxmDuADYfgsOu3cklEUuM4n3Elb3+uIEAiun9VHeuL7yKeGbHOxmQVL2+drD0NdJ9uNtf5bQIXfjErn5rItrwKt8xZdG0KIZyzrGSkcHJDNBmQQkRwj7HBBJJocD1vR79pqfYpBYv+A7OzRN/odAq6cwiqZnNr24I3AIIncdEVursFBA2YJijZ4Fi/N7OETcT0HmP7lvGUjBTqJaYxX/mTaVHqMh9hqbJULdCONULMnYt3D8KCR8wh/1O1EFvGCrBc2p5BtKUYHiaCUGwu8w0Jx3DQD5xi3VYklP8J1vA38T9c6jvWxXYLGLcKZdZa5NwMFJv5d9enhUQY0OiV/MAplbXrcoDrjK3dGTaEBB/ux5+O2IVzEtOl/pjUCy+ifh08swcTTThUFFR0ouPFv1OMR9ZRmfLzxfo8aP+7DXgykeO+lVqGCeIcDffSzvHIP9XX0Fiyp7hDm6ZzfYp1hUWU9fEi5sdkjf5/HjuqrTDBk0QWAP0noRdoEZr243bLgZKC2epmDNcns+QvOnUwFXn725/fH/sir0YnmvGi/OxEDY/iCMjCxOYbQczOuu7r7Vzp6PGLRXhJMRfij8ldanIKYV68q9Jmz2hr/QRXxqdaltJr5X3rnukCwhu2SwydZmidA96PV5wWgtca/Nblugc84PhGCuhQAD+i7OR0ihvYDILI/lYgDqAKFioO/CqD//wGXqJoUlDYLjZHd86T69TpfATMgiRFygFtz6sp1ClZ/Q5OJlK3xCF6MJbQzE7TwZcp7ZAuWbXZybIq1aHce18vNLNjVjYacr0B9o98MOL/5cbyKg5nmBJUXDCXatvxoL6JQ7WIaPPbSRc46ALDo+kRhj6Op9HIexzCsP8aKZAU+idqcRsxChY+rdqL61tyVLAviYi5XzlEHtMYic1bRul4UkRGhaH/7mo1bMIGZeXepoglcMfNk6yljNFmkESdKJ5vwRlW6xZfBrC+dP8J/HeLb4n3Mre980hTFoLX9SzcC0W1fQ5uiJrEzar0jaULnhgjPyu/8QXyVHlq8ovGK0T2b+Im9epJEHe4w3zH7V9o67j2IX2FeEVd5vaNnxF/rbJy0n6r7GT+k/LIR8/vKBTyFan9j6+3ECpy899cFgMs/Yb2qkov+fzXcGkyshkKLwrl70P8UHpD0nzs1+e5Vn+i87gRqEUxXmU510/+foVhTtc3rESmOsooZNw2wfbDOvE9X8OVZ2VD0gdr+zvu8YbiafrWPML4iEfLvh3La++qBgNnUpjY8MlTYJWzcMZmaVvOiOjSrQY5qzUHMQMkxn5QAyiBTlnIHiubW2yoNFje6hAlJ4z4RrdExJm1S96mPr/ECN32WmrlECYnD8ntpsq4rgO2Pfr67G5XFO3ACjp7tt1vefpvimhRXASZF/C8wUAAA==','a:2:{s:11:\"directories\";a:1:{s:10:\"[SITEROOT]\";a:2:{i:0;s:35:\"administrator/components/com_akeeba\";i:1;s:21:\"components/com_akeeba\";}}s:6:\"tables\";a:1:{s:8:\"[SITEDB]\";a:4:{i:0;s:9:\"#__ak_acl\";i:1;s:14:\"#__ak_profiles\";i:2;s:11:\"#__ak_stats\";i:3;s:13:\"#__ak_storage\";}}}');

/*!40000 ALTER TABLE `#__ak_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__ak_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__ak_stats`;

CREATE TABLE `#__ak_stats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `comment` longtext,
  `backupstart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT '1',
  `archivename` longtext,
  `absolute_path` longtext,
  `multipart` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT '1',
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_fullstatus` (`filesexist`,`status`),
  KEY `idx_stale` (`status`,`origin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__ak_storage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__ak_storage`;

CREATE TABLE `#__ak_storage` (
  `tag` varchar(255) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` longtext,
  PRIMARY KEY (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__assets`;

CREATE TABLE `#__assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__assets` WRITE;
/*!40000 ALTER TABLE `#__assets` DISABLE KEYS */;

INSERT INTO `#__assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`)
VALUES
	(1,0,1,113,0,'root.1','Root Asset','{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
	(2,1,1,2,1,'com_admin','com_admin','{}'),
	(3,1,3,6,1,'com_banners','com_banners','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(4,1,7,8,1,'com_cache','com_cache','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
	(5,1,9,10,1,'com_checkin','com_checkin','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
	(6,1,11,12,1,'com_config','com_config','{}'),
	(7,1,13,16,1,'com_contact','com_contact','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(8,1,17,24,1,'com_content','com_content','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
	(9,1,25,26,1,'com_cpanel','com_cpanel','{}'),
	(10,1,27,28,1,'com_installer','com_installer','{\"core.admin\":[],\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
	(11,1,29,30,1,'com_languages','com_languages','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(12,1,31,32,1,'com_login','com_login','{}'),
	(13,1,33,34,1,'com_mailto','com_mailto','{}'),
	(14,1,35,36,1,'com_massmail','com_massmail','{}'),
	(15,1,37,38,1,'com_media','com_media','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
	(16,1,39,40,1,'com_menus','com_menus','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(17,1,41,42,1,'com_messages','com_messages','{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
	(18,1,43,44,1,'com_modules','com_modules','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(19,1,45,48,1,'com_newsfeeds','com_newsfeeds','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(20,1,49,50,1,'com_plugins','com_plugins','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(21,1,51,52,1,'com_redirect','com_redirect','{\"core.admin\":{\"7\":1},\"core.manage\":[]}'),
	(22,1,53,54,1,'com_search','com_search','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
	(23,1,55,56,1,'com_templates','com_templates','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(24,1,57,60,1,'com_users','com_users','{\"core.admin\":{\"7\":1},\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(25,1,61,64,1,'com_weblinks','com_weblinks','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":[],\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1},\"core.edit.own\":[]}'),
	(26,1,65,66,1,'com_wrapper','com_wrapper','{}'),
	(27,8,18,23,2,'com_content.category.2','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(28,3,4,5,2,'com_banners.category.3','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(29,7,14,15,2,'com_contact.category.4','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(30,19,46,47,2,'com_newsfeeds.category.5','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(31,25,62,63,2,'com_weblinks.category.6','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[],\"core.edit.own\":[]}'),
	(32,24,58,59,1,'com_users.notes.category.7','Uncategorised','{\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(33,1,67,68,1,'com_finder','com_finder','{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
	(41,1,77,78,1,'com_akeeba','akeeba','{}'),
	(36,1,71,72,1,'com_sef','com_sef','{}'),
	(60,27,21,22,3,'com_content.article.3','Contact','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(38,1,73,74,1,'com_jce','jce','{}'),
	(40,27,19,20,3,'com_content.article.2','Home','{\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(42,1,79,80,1,'com_smarticons.category.8','Standard','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(43,1,81,82,1,'com_smarticons.icon.1','Add New Article','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(44,1,83,84,1,'com_smarticons.icon.2','Article Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(45,1,85,86,1,'com_smarticons.icon.3','Category Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(46,1,87,88,1,'com_smarticons.icon.4','Media Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(47,1,89,90,1,'com_smarticons.icon.5','Menu Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(48,1,91,92,1,'com_smarticons.icon.6','User Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(49,1,93,94,1,'com_smarticons.icon.7','Module Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(50,1,95,96,1,'com_smarticons.icon.8','Extension Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(51,1,97,98,1,'com_smarticons.icon.9','Language Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(52,1,99,100,1,'com_smarticons.icon.10','Global Configuration','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(53,1,101,102,1,'com_smarticons.icon.11','Template Manager','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(54,1,103,104,1,'com_smarticons.icon.12','Edit Profile','{\"core.view\":{\"1\":1},\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(55,1,105,106,1,'com_smarticons','com_smarticons','{}'),
	(56,1,107,108,1,'com_nonumbermanager','com_nonumbermanager','{\"core.admin\":[],\"core.manage\":[]}'),
	(57,1,109,110,1,'com_advancedmodules','com_advancedmodules','{\"core.admin\":[],\"core.manage\":[],\"core.create\":[],\"core.delete\":[],\"core.edit\":[],\"core.edit.state\":[]}'),
	(59,1,111,112,1,'com_rsform','rsform','{}');

/*!40000 ALTER TABLE `#__assets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__associations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__associations`;

CREATE TABLE `#__associations` (
  `id` varchar(50) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__banner_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__banner_clients`;

CREATE TABLE `#__banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__banner_tracks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__banner_tracks`;

CREATE TABLE `#__banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__banners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__banners`;

CREATE TABLE `#__banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__categories`;

CREATE TABLE `#__categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__categories` WRITE;
/*!40000 ALTER TABLE `#__categories` DISABLE KEYS */;

INSERT INTO `#__categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`)
VALUES
	(1,0,0,0,15,0,'','system','ROOT',X'726F6F74','','',1,0,'0000-00-00 00:00:00',1,'{}','','','',0,'2009-10-18 16:07:09',0,'0000-00-00 00:00:00',0,'*'),
	(2,27,1,1,2,1,'uncategorised','com_content','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:26:37',0,'0000-00-00 00:00:00',0,'*'),
	(3,28,1,3,4,1,'uncategorised','com_banners','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\",\"foobar\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:27:35',0,'0000-00-00 00:00:00',0,'*'),
	(4,29,1,5,6,1,'uncategorised','com_contact','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:27:57',0,'0000-00-00 00:00:00',0,'*'),
	(5,30,1,7,8,1,'uncategorised','com_newsfeeds','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:28:15',0,'0000-00-00 00:00:00',0,'*'),
	(6,31,1,9,10,1,'uncategorised','com_weblinks','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:28:33',0,'0000-00-00 00:00:00',0,'*'),
	(7,32,1,11,12,1,'uncategorised','com_users.notes','Uncategorised',X'756E63617465676F7269736564','','',1,0,'0000-00-00 00:00:00',1,'{\"target\":\"\",\"image\":\"\"}','','','{\"page_title\":\"\",\"author\":\"\",\"robots\":\"\"}',42,'2010-06-28 13:28:33',0,'0000-00-00 00:00:00',0,'*'),
	(8,42,1,13,14,1,'','com_smarticons','Standard',X'7374616E64617264','','<p>Standard icons that come with every Joomla! installation.</p>',1,0,'0000-00-00 00:00:00',1,'','','','',42,'2012-04-27 10:17:48',0,'0000-00-00 00:00:00',0,'*');

/*!40000 ALTER TABLE `#__categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__com_smarticons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__com_smarticons`;

CREATE TABLE `#__com_smarticons` (
  `idIcon` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `catid` int(11) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Text` varchar(45) NOT NULL,
  `Target` varchar(255) NOT NULL,
  `Icon` varchar(255) NOT NULL,
  `Display` tinyint(1) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(5) NOT NULL,
  `params` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  PRIMARY KEY (`idIcon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__com_smarticons` WRITE;
/*!40000 ALTER TABLE `#__com_smarticons` DISABLE KEYS */;

INSERT INTO `#__com_smarticons` (`idIcon`, `asset_id`, `catid`, `Title`, `Text`, `Target`, `Icon`, `Display`, `published`, `ordering`, `params`, `checked_out`, `checked_out_time`)
VALUES
	(1,43,8,'Add New Article','','index.php?option=com_content&task=article.add','images/smarticons/icon-48-article-add.png',1,1,1,'',0,'0000-00-00 00:00:00'),
	(2,44,8,'Article Manager','','index.php?option=com_content','images/smarticons/icon-48-article.png',1,1,2,'',0,'0000-00-00 00:00:00'),
	(3,45,8,'Category Manager','','index.php?option=com_categories&extension=com_content','images/smarticons/icon-48-category.png',1,1,3,'',0,'0000-00-00 00:00:00'),
	(4,46,8,'Media Manager','','index.php?option=com_media','images/smarticons/icon-48-media.png',1,1,4,'',0,'0000-00-00 00:00:00'),
	(5,47,8,'Menu Manager','','index.php?option=com_menus','images/smarticons/icon-48-menumgr.png',1,1,5,'',0,'0000-00-00 00:00:00'),
	(6,48,8,'User Manager','','index.php?option=com_users','images/smarticons/icon-48-user.png',1,1,6,'',0,'0000-00-00 00:00:00'),
	(7,49,8,'Module Manager','','index.php?option=com_modules','images/smarticons/icon-48-module.png',1,1,7,'',0,'0000-00-00 00:00:00'),
	(8,50,8,'Extension Manager','','index.php?option=com_installer','images/smarticons/icon-48-extension.png',1,1,8,'',0,'0000-00-00 00:00:00'),
	(9,51,8,'Language Manager','','index.php?option=com_languages','images/smarticons/icon-48-language.png',1,1,9,'',0,'0000-00-00 00:00:00'),
	(10,52,8,'Global Configuration','','index.php?option=com_config','images/smarticons/icon-48-config.png',1,1,10,'',0,'0000-00-00 00:00:00'),
	(11,53,8,'Template Manager','','index.php?option=com_templates','images/smarticons/icon-48-themes.png',1,1,11,'',0,'0000-00-00 00:00:00'),
	(12,54,8,'Edit Profile','','index.php?option=com_admin&task=profile.edit','images/smarticons/icon-48-info.png',1,1,12,'',0,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `#__com_smarticons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__compitition_comititions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__compitition_comititions`;

CREATE TABLE `#__compitition_comititions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__contact_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__contact_details`;

CREATE TABLE `#__contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__content`;

CREATE TABLE `#__content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `title_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Deprecated in Joomla! 3.0',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(10) unsigned NOT NULL DEFAULT '0',
  `mask` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__content` WRITE;
/*!40000 ALTER TABLE `#__content` DISABLE KEYS */;

INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`)
VALUES
	(1,0,'404',X'343034',X'','<h1>404: Not Found</h1>\r\n<h2>Sorry, but the content you requested could not be found</h2>','',1,0,0,0,'2001-01-01 00:00:00',42,'','2012-04-27 15:24:26',0,0,'0000-00-00 00:00:00','2001-01-01 00:00:00','0000-00-00 00:00:00','','','menu_image=-1\nitem_title=0\npageclass_sfx=\nback_button=\nrating=0\nauthor=0\ncreatedate=0\nmodifydate=0\npdf=0\nprint=0\nemail=0',1,0,0,'','',1,317,'',0,'','');

/*!40000 ALTER TABLE `#__content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__content_frontpage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__content_frontpage`;

CREATE TABLE `#__content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__content_rating
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__content_rating`;

CREATE TABLE `#__content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__core_log_searches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__core_log_searches`;

CREATE TABLE `#__core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__extensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__extensions`;

CREATE TABLE `#__extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__extensions` WRITE;
/*!40000 ALTER TABLE `#__extensions` DISABLE KEYS */;

INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`)
VALUES
	(1,'com_mailto','component','com_mailto','',0,1,1,1,'{\"legacy\":false,\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(2,'com_wrapper','component','com_wrapper','',0,1,1,1,'{\"legacy\":false,\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(3,'com_admin','component','com_admin','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(4,'com_banners','component','com_banners','',1,1,1,0,'{\"legacy\":false,\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\"}','{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(5,'com_cache','component','com_cache','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(6,'com_categories','component','com_categories','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(7,'com_checkin','component','com_checkin','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"Unknown\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2008 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(8,'com_contact','component','com_contact','',1,1,1,0,'{\"legacy\":false,\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_contact_category\":\"hide\",\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_misc\":\"1\",\"show_image\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"show_profile\":\"0\",\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"show_headings\":\"1\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"allow_vcard_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_category_crumb\":\"0\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(9,'com_cpanel','component','com_cpanel','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10,'com_installer','component','com_installer','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(11,'com_languages','component','com_languages','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}','{\"administrator\":\"en-GB\",\"site\":\"en-GB\"}','','',0,'0000-00-00 00:00:00',0,0),
	(12,'com_login','component','com_login','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(13,'com_media','component','com_media','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\"}','{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"1\",\"allowed_media_usergroup\":\"3\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\",\"enable_flash\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(14,'com_menus','component','com_menus','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(15,'com_messages','component','com_messages','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(16,'com_modules','component','com_modules','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(17,'com_newsfeeds','component','com_newsfeeds','',1,1,1,0,'{\"legacy\":false,\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_word_count\":\"0\",\"show_headings\":\"1\",\"show_name\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"display_num\":\"\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\",\"show_cat_items\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(18,'com_plugins','component','com_plugins','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(19,'com_search','component','com_search','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\"}','{\"enabled\":\"0\",\"show_date\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(20,'com_templates','component','com_templates','',1,1,1,1,'{\"legacy\":false,\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}','{\"template_positions_display\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(21,'com_weblinks','component','com_weblinks','',1,1,1,0,'{\"legacy\":false,\"name\":\"com_weblinks\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_comp_description\":\"1\",\"comp_description\":\"\",\"show_link_hits\":\"1\",\"show_link_description\":\"1\",\"show_other_cats\":\"0\",\"show_headings\":\"0\",\"show_numbers\":\"0\",\"show_report\":\"1\",\"count_clicks\":\"1\",\"target\":\"0\",\"link_icons\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(22,'com_content','component','com_content','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\"}','{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"0\",\"show_category\":\"0\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"0\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"0\",\"show_item_navigation\":\"0\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"0\",\"readmore_limit\":\"100\",\"show_icons\":\"0\",\"show_print_icon\":\"0\",\"show_email_icon\":\"0\",\"show_hits\":\"0\",\"show_noauth\":\"0\",\"urls_position\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"0\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"0\",\"show_cat_num_articles_cat\":\"0\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(23,'com_config','component','com_config','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}','{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}','','',0,'0000-00-00 00:00:00',0,0),
	(24,'com_redirect','component','com_redirect','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(25,'com_users','component','com_users','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\"}','{\"allowUserRegistration\":\"1\",\"new_usertype\":\"2\",\"useractivation\":\"1\",\"frontend_userparams\":\"1\",\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(27,'com_finder','component','com_finder','',1,1,0,0,'{\"legacy\":false,\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','{\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_advanced\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stemmer\":\"snowball\"}','','',0,'0000-00-00 00:00:00',0,0),
	(100,'PHPMailer','library','phpmailer','',0,1,1,1,'{\"legacy\":false,\"name\":\"PHPMailer\",\"type\":\"library\",\"creationDate\":\"2001\",\"author\":\"PHPMailer\",\"copyright\":\"(c) 2001-2003, Brent R. Matzelle, (c) 2004-2009, Andy Prevost. All Rights Reserved., (c) 2010-2011, Jim Jagielski. All Rights Reserved.\",\"authorEmail\":\"jimjag@gmail.com\",\"authorUrl\":\"https:\\/\\/code.google.com\\/a\\/apache-extras.org\\/p\\/phpmailer\\/\",\"version\":\"5.2\",\"description\":\"LIB_PHPMAILER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(101,'SimplePie','library','simplepie','',0,1,1,1,'{\"legacy\":false,\"name\":\"SimplePie\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"SimplePie\",\"copyright\":\"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/simplepie.org\\/\",\"version\":\"1.2\",\"description\":\"LIB_SIMPLEPIE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(102,'phputf8','library','phputf8','',0,1,1,1,'{\"legacy\":false,\"name\":\"phputf8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(103,'Joomla! Platform','library','joomla','',0,1,1,1,'{\"legacy\":false,\"name\":\"Joomla! Platform\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"http:\\/\\/www.joomla.org\",\"version\":\"11.4\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(200,'mod_articles_archive','module','mod_articles_archive','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters.\\n\\t\\tAll rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(201,'mod_articles_latest','module','mod_articles_latest','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(202,'mod_articles_popular','module','mod_articles_popular','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(203,'mod_banners','module','mod_banners','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(204,'mod_breadcrumbs','module','mod_breadcrumbs','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(205,'mod_custom','module','mod_custom','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(206,'mod_feed','module','mod_feed','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(207,'mod_footer','module','mod_footer','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(208,'mod_login','module','mod_login','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(209,'mod_menu','module','mod_menu','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(210,'mod_articles_news','module','mod_articles_news','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(211,'mod_random_image','module','mod_random_image','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(212,'mod_related_items','module','mod_related_items','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(213,'mod_search','module','mod_search','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(214,'mod_stats','module','mod_stats','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(215,'mod_syndicate','module','mod_syndicate','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(216,'mod_users_latest','module','mod_users_latest','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(217,'mod_weblinks','module','mod_weblinks','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_weblinks\",\"type\":\"module\",\"creationDate\":\"July 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(218,'mod_whosonline','module','mod_whosonline','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(219,'mod_wrapper','module','mod_wrapper','',0,1,1,0,'{\"legacy\":false,\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(220,'mod_articles_category','module','mod_articles_category','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(221,'mod_articles_categories','module','mod_articles_categories','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(222,'mod_languages','module','mod_languages','',0,1,1,1,'{\"legacy\":false,\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(223,'mod_finder','module','mod_finder','',0,1,0,0,'{\"legacy\":false,\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(300,'mod_custom','module','mod_custom','',1,1,1,1,'{\"legacy\":false,\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(301,'mod_feed','module','mod_feed','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(302,'mod_latest','module','mod_latest','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(303,'mod_logged','module','mod_logged','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(304,'mod_login','module','mod_login','',1,1,1,1,'{\"legacy\":false,\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(305,'mod_menu','module','mod_menu','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(307,'mod_popular','module','mod_popular','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(308,'mod_quickicon','module','mod_quickicon','',1,1,1,1,'{\"legacy\":false,\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(309,'mod_status','module','mod_status','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(310,'mod_submenu','module','mod_submenu','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(311,'mod_title','module','mod_title','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(312,'mod_toolbar','module','mod_toolbar','',1,1,1,1,'{\"legacy\":false,\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(313,'mod_multilangstatus','module','mod_multilangstatus','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\"}','{\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(400,'plg_authentication_gmail','plugin','gmail','authentication',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\"}','{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}','','',0,'0000-00-00 00:00:00',1,0),
	(401,'plg_authentication_joomla','plugin','joomla','authentication',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(402,'plg_authentication_ldap','plugin','ldap','authentication',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\"}','{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}','','',0,'0000-00-00 00:00:00',3,0),
	(404,'plg_content_emailcloak','plugin','emailcloak','content',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\"}','{\"mode\":\"1\"}','','',0,'0000-00-00 00:00:00',1,0),
	(405,'plg_content_geshi','plugin','geshi','content',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_content_geshi\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"\",\"authorUrl\":\"qbnz.com\\/highlighter\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_GESHI_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',2,0),
	(406,'plg_content_loadmodule','plugin','loadmodule','content',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\"}','{\"style\":\"xhtml\"}','','',0,'2011-09-18 15:22:50',0,0),
	(407,'plg_content_pagebreak','plugin','pagebreak','content',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\"}','{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}','','',0,'0000-00-00 00:00:00',4,0),
	(408,'plg_content_pagenavigation','plugin','pagenavigation','content',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\"}','{\"position\":\"1\"}','','',0,'0000-00-00 00:00:00',5,0),
	(409,'plg_content_vote','plugin','vote','content',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',6,0),
	(410,'plg_editors_codemirror','plugin','codemirror','editors',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\"}','{\"linenumbers\":\"0\",\"tabmode\":\"indent\"}','','',0,'0000-00-00 00:00:00',1,0),
	(411,'plg_editors_none','plugin','none','editors',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Unknown\",\"copyright\":\"\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"\",\"version\":\"2.5.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',2,0),
	(412,'plg_editors_tinymce','plugin','tinymce','editors',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2012\",\"author\":\"Moxiecode Systems AB\",\"copyright\":\"Moxiecode Systems AB\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"tinymce.moxiecode.com\\/\",\"version\":\"3.5.2\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\"}','{\"mode\":\"1\",\"skin\":\"0\",\"compressed\":\"0\",\"cleanup_startup\":\"0\",\"cleanup_save\":\"2\",\"entity_encoding\":\"raw\",\"lang_mode\":\"0\",\"lang_code\":\"en\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"extended_elements\":\",@[data-lightbox],@[data-spotlight],@[data-lightbox],@[data-spotlight],@[data-lightbox],@[data-spotlight],@[data-lightbox],@[data-spotlight]\",\"toolbar\":\"top\",\"toolbar_align\":\"left\",\"html_height\":\"550\",\"html_width\":\"750\",\"element_path\":\"1\",\"fonts\":\"1\",\"paste\":\"1\",\"searchreplace\":\"1\",\"insertdate\":\"1\",\"format_date\":\"%Y-%m-%d\",\"inserttime\":\"1\",\"format_time\":\"%H:%M:%S\",\"colors\":\"1\",\"table\":\"1\",\"smilies\":\"1\",\"media\":\"1\",\"hr\":\"1\",\"directionality\":\"1\",\"fullscreen\":\"1\",\"style\":\"1\",\"layer\":\"1\",\"xhtmlxtras\":\"1\",\"visualchars\":\"1\",\"nonbreaking\":\"1\",\"template\":\"1\",\"blockquote\":\"1\",\"wordcount\":\"1\",\"advimage\":\"1\",\"advlink\":\"1\",\"autosave\":\"1\",\"contextmenu\":\"1\",\"inlinepopups\":\"1\",\"safari\":\"0\",\"custom_plugin\":\"\",\"custom_button\":\"\"}','','',0,'0000-00-00 00:00:00',3,0),
	(413,'plg_editors-xtd_article','plugin','article','editors-xtd',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',1,0),
	(414,'plg_editors-xtd_image','plugin','image','editors-xtd',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',2,0),
	(415,'plg_editors-xtd_pagebreak','plugin','pagebreak','editors-xtd',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',3,0),
	(416,'plg_editors-xtd_readmore','plugin','readmore','editors-xtd',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',4,0),
	(417,'plg_search_categories','plugin','categories','search',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(418,'plg_search_contacts','plugin','contacts','search',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(419,'plg_search_content','plugin','content','search',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(420,'plg_search_newsfeeds','plugin','newsfeeds','search',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(421,'plg_search_weblinks','plugin','weblinks','search',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_search_weblinks\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(422,'plg_system_languagefilter','plugin','languagefilter','system',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',1,0),
	(423,'plg_system_p3p','plugin','p3p','system',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\"}','{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}','','',0,'0000-00-00 00:00:00',2,0),
	(424,'plg_system_cache','plugin','cache','system',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\"}','{\"browsercache\":\"0\",\"cachetime\":\"15\"}','','',0,'0000-00-00 00:00:00',9,0),
	(425,'plg_system_debug','plugin','debug','system',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\"}','{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}','','',0,'0000-00-00 00:00:00',4,0),
	(426,'plg_system_log','plugin','log','system',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',5,0),
	(427,'plg_system_redirect','plugin','redirect','system',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',6,0),
	(428,'plg_system_remember','plugin','remember','system',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',7,0),
	(429,'plg_system_sef','plugin','sef','system',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',8,0),
	(430,'plg_system_logout','plugin','logout','system',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',3,0),
	(431,'plg_user_contactcreator','plugin','contactcreator','user',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\"}','{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}','','',0,'0000-00-00 00:00:00',1,0),
	(432,'plg_user_joomla','plugin','joomla','user',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2009 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','{\"autoregister\":\"1\"}','','',0,'0000-00-00 00:00:00',2,0),
	(433,'plg_user_profile','plugin','profile','user',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\"}','{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(434,'plg_extension_joomla','plugin','joomla','extension',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',1,0),
	(435,'plg_content_joomla','plugin','joomla','content',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(436,'plg_system_languagecode','plugin','languagecode','system',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',10,0),
	(437,'plg_quickicon_joomlaupdate','plugin','joomlaupdate','quickicon',0,0,1,1,'{\"legacy\":false,\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(438,'plg_quickicon_extensionupdate','plugin','extensionupdate','quickicon',0,1,1,1,'{\"legacy\":false,\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(439,'plg_captcha_recaptcha','plugin','recaptcha','captcha',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\"}','{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}','','',0,'0000-00-00 00:00:00',0,0),
	(440,'plg_system_highlight','plugin','highlight','system',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',7,0),
	(441,'plg_content_finder','plugin','finder','content',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(442,'plg_finder_categories','plugin','categories','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',1,0),
	(443,'plg_finder_contacts','plugin','contacts','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',2,0),
	(444,'plg_finder_content','plugin','content','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',3,0),
	(445,'plg_finder_newsfeeds','plugin','newsfeeds','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',4,0),
	(446,'plg_finder_weblinks','plugin','weblinks','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_finder_weblinks\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_FINDER_WEBLINKS_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',5,0),
	(10022,'System Language - ARTIO JoomSEF','plugin','joomseflang','system',0,0,1,0,'{\"legacy\":false,\"name\":\"System Language - ARTIO JoomSEF\",\"type\":\"plugin\",\"creationDate\":\"1. August 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"4.1.0\",\"description\":\"PLG_JOOMSEFLANG_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(502,'bluestork','template','bluestork','',1,1,1,0,'{\"legacy\":false,\"name\":\"bluestork\",\"type\":\"template\",\"creationDate\":\"07\\/02\\/09\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"TPL_BLUESTORK_XML_DESCRIPTION\",\"group\":\"\"}','{\"useRoundedCorners\":\"1\",\"showSiteName\":\"0\",\"textBig\":\"0\",\"highContrast\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10021,'System - ARTIO JoomSEF Google Analytics','plugin','joomsefgoogle','system',0,1,1,0,'{\"legacy\":false,\"name\":\"System - ARTIO JoomSEF Google Analytics\",\"type\":\"plugin\",\"creationDate\":\"31. October 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"4.0.0\",\"description\":\"Plugin which add Google Adsense Code to Pages.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10018,'Content - ARTIO JoomSEF','plugin','joomsef','content',0,1,1,0,'{\"legacy\":false,\"name\":\"Content - ARTIO JoomSEF\",\"type\":\"plugin\",\"creationDate\":\"12. June 2010\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"4.0.0\",\"description\":\"PLG_CONTENT_JOOMSEF_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10019,'Extension Install - ARTIO JoomSEF','plugin','joomsefinstall','extension',0,1,1,0,'{\"legacy\":false,\"name\":\"Extension Install - ARTIO JoomSEF\",\"type\":\"plugin\",\"creationDate\":\"21. October 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"4.1.1\",\"description\":\"PLG_JOOMSEFINSTALL_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10020,'System - ARTIO JoomSEF','plugin','joomsef','system',0,1,1,0,'{\"legacy\":false,\"name\":\"System - ARTIO JoomSEF\",\"type\":\"plugin\",\"creationDate\":\"2. November 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"4.0.1\",\"description\":\"PLG_JOOMSEF_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(600,'English (United Kingdom)','language','en-GB','',0,1,1,1,'{\"legacy\":false,\"name\":\"English (United Kingdom)\",\"type\":\"language\",\"creationDate\":\"2008-03-15\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.5\",\"description\":\"en-GB site language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(601,'English (United Kingdom)','language','en-GB','',1,1,1,1,'{\"legacy\":false,\"name\":\"English (United Kingdom)\",\"type\":\"language\",\"creationDate\":\"2008-03-15\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.5\",\"description\":\"en-GB administrator language\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(700,'files_joomla','file','joomla','',0,1,1,1,'{\"legacy\":false,\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"June 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.6\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(800,'PKG_JOOMLA','package','pkg_joomla','',0,1,1,1,'{\"legacy\":false,\"name\":\"PKG_JOOMLA\",\"type\":\"package\",\"creationDate\":\"2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"http:\\/\\/www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PKG_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10050,'yoo_master','template','yoo_master','',0,1,1,0,'{\"legacy\":false,\"name\":\"yoo_master\",\"type\":\"template\",\"creationDate\":\"May 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Master is the May 2012 theme of the YOOtheme club. It is based on YOOtheme\'s Warp theme framework. NOTE: It is not free or public. This theme is for members of the YOOtheme club only.\",\"group\":\"\"}','{\"config\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10001,'ZOO Item','module','mod_zooitem','',0,1,0,0,'{\"legacy\":true,\"name\":\"ZOO Item\",\"type\":\"module\",\"creationDate\":\"July 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.6.0\",\"description\":\"Item module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"theme\":\"\",\"layout\":\"\",\"media_position\":\"left\",\"application\":\"\",\"subcategories\":\"0\",\"count\":\"4\",\"order\":\"_itemname\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10002,'ZOO Comment','module','mod_zoocomment','',0,1,0,0,'{\"legacy\":true,\"name\":\"ZOO Comment\",\"type\":\"module\",\"creationDate\":\"July 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.6.0\",\"description\":\"Comment module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"theme\":\"\",\"application\":\"\",\"subcategories\":\"0\",\"count\":\"10\",\"show_avatar\":\"1\",\"avatar_size\":\"40\",\"show_author\":\"1\",\"show_meta\":\"1\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10003,'ZOO Tag','module','mod_zootag','',0,1,0,0,'{\"legacy\":true,\"name\":\"ZOO Tag\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Tag module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"theme\":\"\",\"application\":\"\",\"count\":\"10\",\"order\":\"alpha\",\"menu_item\":\"\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10004,'ZOO Category','module','mod_zoocategory','',0,1,0,0,'{\"legacy\":true,\"name\":\"ZOO Category\",\"type\":\"module\",\"creationDate\":\"Febuary 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Category module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"theme\":\"\",\"application\":\"\",\"depth\":\"0\",\"menu_item\":\"\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10005,'ZOO Quick Icons','module','mod_zooquickicon','',1,1,2,0,'{\"legacy\":true,\"name\":\"ZOO Quick Icons\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Quick Icons module for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',102,0),
	(10006,'ZOO Search','plugin','zoosearch','search',0,1,1,0,'{\"legacy\":true,\"name\":\"ZOO Search\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Search plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"search_fulltext\":\"0\",\"search_limit\":\"50\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10007,'Content - ZOO Shortcode','plugin','zooshortcode','content',0,1,1,0,'{\"legacy\":true,\"name\":\"Content - ZOO Shortcode\",\"type\":\"plugin\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Shortcode plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com) Usage: {zooitem OR zoocategory: ID OR alias} Optionally: {zooitem: ID text: MYTEXT}\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10008,'System - ZOO Event','plugin','zooevent','system',0,1,1,0,'{\"legacy\":true,\"name\":\"System - ZOO Event\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Event plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10009,'com_zoo','component','com_zoo','',1,1,0,0,'{\"legacy\":false,\"name\":\"com_zoo\",\"type\":\"component\",\"creationDate\":\"July 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.6.4\",\"description\":\"ZOO component for Joomla 2.5 developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10066,'COM_ADVANCEDMODULES','component','com_advancedmodules','',1,1,0,0,'{\"legacy\":false,\"name\":\"COM_ADVANCEDMODULES\",\"type\":\"component\",\"creationDate\":\"August 2012\",\"author\":\"NoNumber (Peter van Westen)\",\"copyright\":\"Copyright \\u00a9 2012 NoNumber All Rights Reserved\",\"authorEmail\":\"peter@nonumber.nl\",\"authorUrl\":\"http:\\/\\/www.nonumber.nl\",\"version\":\"3.2.6PRO\",\"description\":\"COM_ADVANCEDMODULES_DESC\",\"group\":\"\"}','{\"list_title\":\"0\",\"open_in_modals\":\"1\",\"show_color\":\"1\",\"main_colors\":\"FF0000,FF8000,FFFF00,80FF00,00FF00,00FF80,00FFFF,0080FF,0000FF,8000FF,FF00FF,FF0080,000000,666666,999999,CCCCCC,FFFFFF\",\"show_hideempty\":\"1\",\"show_note\":\"2\",\"show_configmsg\":\"1\",\"show_config_in_item\":\"1\",\"use_sef\":\"2\",\"show_mirror_module\":\"1\",\"show_match_method\":\"1\",\"match_method\":\"and\",\"show_show_ignores\":\"1\",\"show_ignores\":\"1\",\"show_assignto_homepage\":\"1\",\"show_assignto_content\":\"1\",\"show_assignto_k2\":\"0\",\"show_assignto_zoo\":\"1\",\"show_assignto_components\":\"1\",\"show_assignto_urls\":\"1\",\"show_assignto_browsers\":\"1\",\"show_assignto_date\":\"1\",\"show_assignto_usergrouplevels\":\"1\",\"show_assignto_users\":\"1\",\"show_assignto_languages\":\"1\",\"show_assignto_templates\":\"1\",\"show_assignto_php\":\"1\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10055,'Akeeba Backup Notification Module','module','mod_akadmin','',1,1,2,0,'{\"legacy\":true,\"name\":\"Akeeba Backup Notification Module\",\"type\":\"module\",\"creationDate\":\"2012-03-21\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2009-2010 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.4.3\",\"description\":\"\\n\\t<h1>Akeeba Backup Notification Module<\\/h1>\\n\\t<p>This is a handy  module to display a Akeeba icon on your administrator\\n\\tback-end\'s Control Panel page. The icon displays a warning site if the last\\n\\tbackup is failed, or if you haven\'t backed up your site for a period of time\\n\\t(user-defined). Clicking it brings you to the Akeeba Backup &quot;Backup Now&quot;\\n\\tpage.<\\/p>\\n\\t\",\"group\":\"\"}','[]','','',0,'0000-00-00 00:00:00',0,0),
	(10056,'Akeeba Backup Lazy Scheduling','plugin','aklazy','system',0,0,1,0,'{\"legacy\":true,\"name\":\"Akeeba Backup Lazy Scheduling\",\"type\":\"plugin\",\"creationDate\":\"2011-04-17\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2010-2012 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.3\",\"description\":\"\\n\\t\\tAutomate your Akeeba Backup installation without using CRON, taking\\n\\t\\tadvantage of your site\'s visitor activity. Important: Read the\\n\\t\\tdocumentation before proceeding. THIS PLUGIN IS NO LONGER SUPPORTED.\\n\\t\\tDO NOT REQUEST SUPPORT IF IT DOESN\'T WORK. READ THE DOCUMENTATION FOR\\n\\t\\tA LIST OF KNOWN ISSUES.\\n\\t\",\"group\":\"\"}','{\"daysfreq\":\"1\",\"backuptime\":\"00:00\",\"profile\":\"1\",\"@spacer\":\"\",\"test\":\"0\",\"resetpw\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10057,'akeeba','component','com_akeeba','',1,1,0,0,'{\"legacy\":true,\"name\":\"Akeeba\",\"type\":\"component\",\"creationDate\":\"2012-03-21\",\"author\":\"Nicholas K. Dionysopoulos\",\"copyright\":\"Copyright (c)2006-2010 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@dionysopoulos.me\",\"authorUrl\":\"http:\\/\\/www.akeebabackup.com\",\"version\":\"3.4.3\",\"description\":\"Akeeba Backup Core (formerly JoomlaPack) - Full Joomla! site backup solution, Core Edition. Making backup as simple as ABC!\",\"group\":\"\"}','{\"siteurl\":\"http:\\/\\/joomla25.siteincubator.co.uk\\/\",\"jlibrariesdir\":\"\\/var\\/www\\/vhosts\\/joomla25.siteincubator.co.uk\\/httpdocs\\/libraries\",\"jversion\":\"1.6\",\"lastversion\":\"3.4.3\",\"liveupdate\":\"lastcheck=1335523370\\nupdatedata=\\\"\\\"{\\\\\\\"supported\\\\\\\":true,\\\\\\\"stuck\\\\\\\":false,\\\\\\\"version\\\\\\\":\\\\\\\"3.4.3\\\\\\\",\\\\\\\"date\\\\\\\":\\\\\\\"2012-03-21\\\\\\\",\\\\\\\"stability\\\\\\\":\\\\\\\"stable\\\\\\\",\\\\\\\"downloadURL\\\\\\\":\\\\\\\"http:\\\\\\\\\\\\\\/\\\\\\\\\\\\\\/joomlacode.org\\\\\\\\\\\\\\/gf\\\\\\\\\\\\\\/download\\\\\\\\\\\\\\/frsrelease\\\\\\\\\\\\\\/16861\\\\\\\\\\\\\\/73285\\\\\\\\\\\\\\/com_akeeba-3.4.3-core.zip\\\\\\\",\\\\\\\"infoURL\\\\\\\":\\\\\\\"https:\\\\\\\\\\\\\\/\\\\\\\\\\\\\\/www.akeebabackup.com\\\\\\\\\\\\\\/download\\\\\\\\\\\\\\/akeeba-backup\\\\\\\\\\\\\\/akeeba-backup-3-4-3.html\\\\\\\",\\\\\\\"releasenotes\\\\\\\":\\\\\\\"<h3 class=\\\\\\\\\\\\\\\"p1\\\\\\\\\\\\\\\"><span class=\\\\\\\\\\\\\\\"s1\\\\\\\\\\\\\\\"><strong>Changelog<\\\\\\\\\\\\\\/strong><\\\\\\\\\\\\\\/span><\\\\\\\\\\\\\\/h3><p style=\\\\\\\\\\\\\\\"font-weight: bold;\\\\\\\\\\\\\\\">Bug fixes<\\\\\\\\\\\\\\/p><ul><li>Call time pass by reference removed in PHP 5.4.0, causing a fatal error when using Akeeba Backup on PHP 5.4<\\\\\\\\\\\\\\/li><li>The Box.net integration would copy the backup archive to the administrator directory and not delete it<\\\\\\\\\\\\\\/li><li>Regression: using the email after front-end backup causes a backup failure under Joomla! 2.5<\\\\\\\\\\\\\\/li><li>No emails after front-end backup sent under Joomla! 2.5<\\\\\\\\\\\\\\/li><\\\\\\\\\\\\\\/ul>\\\\\\\"}\\\"\\\"\\nstuck=0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10062,'Minima','package','pkg_Minima','',0,1,1,0,'{\"legacy\":true,\"name\":\"Minima\",\"type\":\"package\",\"creationDate\":\"Unknown\",\"author\":\"Unknown\",\"copyright\":\"\",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"0.9\",\"description\":\"Minima template package.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10065,'com_smarticons','component','com_smarticons','',1,1,0,0,'{\"legacy\":false,\"name\":\"COM_SMARTICONS\",\"type\":\"component\",\"creationDate\":\"Aug 2011\",\"author\":\"Bogdan-Ioan SUTA\",\"copyright\":\"(C) 2011 SUTA Bogdan-Ioan\",\"authorEmail\":\"bogdan.suta@naicum.ro\",\"authorUrl\":\"http:\\/\\/extensions.naicum.ro\",\"version\":\"1.1.0\",\"description\":\"COM_SMARTICONS_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10067,'PLG_SYSTEM_ADVANCEDMODULES','plugin','advancedmodules','system',0,1,1,0,'{\"legacy\":true,\"name\":\"PLG_SYSTEM_ADVANCEDMODULES\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"NoNumber (Peter van Westen)\",\"copyright\":\"Copyright \\u00a9 2012 NoNumber All Rights Reserved\",\"authorEmail\":\"peter@nonumber.nl\",\"authorUrl\":\"http:\\/\\/www.nonumber.nl\",\"version\":\"3.2.6PRO\",\"description\":\"PLG_SYSTEM_ADVANCEDMODULES_DESC\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10012,'System - Nooku','plugin','koowa','system',0,1,1,0,'','{\"auth_basic\":\"1\"}','','',0,'0000-00-00 00:00:00',1,0),
	(10013,'mod_default','module','mod_default','',0,1,1,0,'false','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10014,'mod_default','module','mod_default','',1,1,1,0,'false','{}','','',0,'0000-00-00 00:00:00',0,0),
	(28,'com_joomlaupdate','component','com_joomlaupdate','',1,1,0,1,'{\"legacy\":false,\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10016,'com_test','component','com_test','',0,1,1,0,'','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10023,'Banners','sef_ext','ext_joomsef4_banners','',0,1,1,1,'{\"legacy\":false,\"name\":\"Banners\",\"type\":\"sef_ext\",\"creationDate\":\"22. June 2010\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.2\",\"description\":\"Adds SEO support for Banners component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10024,'Contacts','sef_ext','ext_joomsef4_contact','',0,1,1,1,'{\"legacy\":false,\"name\":\"Contacts\",\"type\":\"sef_ext\",\"creationDate\":\"24. January 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.2\",\"description\":\"Adds SEO support for Contacts component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10025,'Content','sef_ext','ext_joomsef4_content','',0,1,1,1,'{\"legacy\":false,\"name\":\"Content\",\"type\":\"sef_ext\",\"creationDate\":\"24. October 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.4\",\"description\":\"Adds SEO support for Content component.\",\"group\":\"\"}','acceptVars=view; id; catid; type; year; month; filter; limit; limitstart; start; task','+^[0-9]*$=limit,limitstart,start,month,year\n+^[a-zA-Z]+$=task,type,view\n+^[0-9]+(:[\\w\\-_\\d]+)?$=catid,id','',0,'0000-00-00 00:00:00',0,0),
	(10026,'Mail To','sef_ext','ext_joomsef4_mailto','',0,1,1,1,'{\"legacy\":false,\"name\":\"Mail To\",\"type\":\"sef_ext\",\"creationDate\":\"22. June 2010\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.1\",\"description\":\"Adds SEO support for Mail To component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10027,'NewsFeeds','sef_ext','ext_joomsef4_newsfeeds','',0,1,1,1,'{\"legacy\":false,\"name\":\"NewsFeeds\",\"type\":\"sef_ext\",\"creationDate\":\"25. January 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.2\",\"description\":\"Adds SEO support for NewsFeeds component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10028,'Search','sef_ext','ext_joomsef4_search','',0,1,1,1,'{\"legacy\":false,\"name\":\"Search\",\"type\":\"sef_ext\",\"creationDate\":\"13. January 2012\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.3\",\"description\":\"Adds SEO support for Search component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10029,'Users','sef_ext','ext_joomsef4_users','',0,1,1,1,'{\"legacy\":false,\"name\":\"Users\",\"type\":\"sef_ext\",\"creationDate\":\"10. January 2012\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.2\",\"description\":\"Adds SEO support for User component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10030,'WebLinks','sef_ext','ext_joomsef4_weblinks','',0,1,1,1,'{\"legacy\":false,\"name\":\"WebLinks\",\"type\":\"sef_ext\",\"creationDate\":\"25. January 2011\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.2\",\"description\":\"Adds SEO support for WebLinks component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10031,'Wrapper','sef_ext','ext_joomsef4_wrapper','',0,1,1,1,'{\"legacy\":false,\"name\":\"Wrapper\",\"type\":\"sef_ext\",\"creationDate\":\"10. January 2012\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"www.artio.net\",\"version\":\"3.0.3\",\"description\":\"Adds SEO support for Wrapper component.\",\"group\":\"\"}','ignoreSource=0\nitemid=1','','',0,'0000-00-00 00:00:00',0,0),
	(10032,'com_sef','component','com_sef','',1,1,0,0,'{\"legacy\":false,\"name\":\"com_sef\",\"type\":\"component\",\"creationDate\":\"29. June 2012\",\"author\":\"ARTIO s.r.o.\",\"copyright\":\"ARTIO s.r.o.\",\"authorEmail\":\"info@artio.net\",\"authorUrl\":\"http:\\/\\/www.artio.net\",\"version\":\"4.2.8\",\"description\":\"ARTIO JoomSEF rewrites Joomla! URLs to be Search Engine Friendly together advanced\\n\\tURL editing options, such as metatags definitions. It integrates JoomFish support with\\n\\toption of internationalized, multilingual URLs. The configuration is easy and straightforward.\\n\\tSupport for many further popular extensions is included!\",\"group\":\"\"}','{\"devel\":\"i:0;\",\"alwaysUseLang\":\"b:1;\",\"enabled\":\"s:1:\\\"1\\\";\",\"professionalMode\":\"s:1:\\\"0\\\";\",\"showInfoTexts\":\"b:1;\",\"replacement\":\"s:1:\\\"-\\\";\",\"pagerep\":\"s:1:\\\"-\\\";\",\"stripthese\":\"s:47:\\\",|~|!|@|%|^|*|(|)|+|<|>|:|;|{|}|[|]|---|--|..|.\\\";\",\"suffix\":\"s:0:\\\"\\\";\",\"addFile\":\"s:0:\\\"\\\";\",\"friendlytrim\":\"s:3:\\\"-|.\\\";\",\"canonicalLink\":\"b:1;\",\"pagetext\":\"s:27:\\\"JText::_(\'COM_SEF_PAGE\')-%s\\\";\",\"langPlacement\":\"i:0;\",\"lowerCase\":\"s:1:\\\"1\\\";\",\"useAlias\":\"s:1:\\\"0\\\";\",\"excludeSource\":\"b:0;\",\"reappendSource\":\"b:0;\",\"ignoreSource\":\"b:1;\",\"appendNonSef\":\"b:1;\",\"transitSlash\":\"b:1;\",\"redirectSlash\":\"b:0;\",\"useCache\":\"s:1:\\\"1\\\";\",\"cacheSize\":\"i:1000;\",\"cacheMinHits\":\"i:10;\",\"cacheRecordHits\":\"b:0;\",\"cacheShowErr\":\"b:0;\",\"translateNames\":\"b:1;\",\"page404\":\"s:1:\\\"0\\\";\",\"record404\":\"s:1:\\\"0\\\";\",\"showMessageOn404\":\"s:1:\\\"0\\\";\",\"use404itemid\":\"s:1:\\\"0\\\";\",\"itemid404\":\"i:0;\",\"nonSefRedirect\":\"b:1;\",\"useMoved\":\"b:1;\",\"useMovedAsk\":\"b:1;\",\"replacements\":\"s:1009:\\\"\\u00c1|A, \\u00c2|A, \\u00c5|A, \\u0102|A, \\u00c4|A, \\u00c0|A, \\u00c6|A, \\u0106|C, \\u00c7|C, \\u010c|C, \\u010e|D, \\u00c9|E, \\u00c8|E, \\u00cb|E, \\u011a|E, \\u00ca|E, \\u00cc|I, \\u00cd|I, \\u00ce|I, \\u00cf|I, \\u0139|L, \\u013e|l, \\u013d|L, \\u0143|N, \\u0147|N, \\u00d1|N, \\u00d2|O, \\u00d3|O, \\u00d4|O, \\u00d5|O, \\u00d6|O, \\u00d8|O, \\u0154|R, \\u0158|R, \\u0160|S, \\u015a|S, \\u0164|T, \\u016e|U, \\u00da|U, \\u0170|U, \\u00dc|U, \\u00db|U, \\u00dd|Y, \\u017d|Z, \\u0179|Z, \\u00e1|a, \\u00e2|a, \\u00e5|a, \\u00e4|a, \\u00e0|a, \\u00e6|a, \\u0107|c, \\u00e7|c, \\u010d|c, \\u010f|d, \\u0111|d, \\u00e9|e, \\u0119|e, \\u00eb|e, \\u011b|e, \\u00e8|e, \\u00ea|e, \\u00ec|i, \\u00ed|i, \\u00ee|i, \\u00ef|i, \\u013a|l, \\u0144|n, \\u0148|n, \\u00f1|n, \\u00f2|o, \\u00f3|o, \\u00f4|o, \\u0151|o, \\u00f6|o, \\u00f8|o, \\u0161|s, \\u015b|s, \\u0159|r, \\u0155|r, \\u0165|t, \\u016f|u, \\u00fa|u, \\u0171|u, \\u00fc|u, \\u00fb|u, \\u00fd|y, \\u017e|z, \\u017a|z, \\u02d9|-, \\u00df|ss, \\u0104|A, \\u00b5|u, \\u0105|a, \\u0118|E, \\u017c|z, \\u017b|Z, \\u0142|l, \\u0141|L, \\u0410|A, \\u0430|a, \\u0411|B, \\u0431|b, \\u0412|V, \\u0432|v, \\u0413|G, \\u0433|g, \\u0414|D, \\u0434|d, \\u0415|E, \\u0435|e, \\u0416|Zh, \\u0436|zh, \\u0417|Z, \\u0437|z, \\u0418|I, \\u0438|i, \\u0419|I, \\u0439|i, \\u041a|K, \\u043a|k, \\u041b|L, \\u043b|l, \\u041c|M, \\u043c|m, \\u041d|N, \\u043d|n, \\u041e|O, \\u043e|o, \\u041f|P, \\u043f|p, \\u0420|R, \\u0440|r, \\u0421|S, \\u0441|s, \\u0422|T, \\u0442|t, \\u0423|U, \\u0443|u, \\u0424|F, \\u0444|f, \\u0425|Kh, \\u0445|kh, \\u0426|Tc, \\u0446|tc, \\u0427|Ch, \\u0447|ch, \\u0428|Sh, \\u0448|sh, \\u0429|Shch, \\u0449|shch, \\u042b|Y, \\u044b|y, \\u042d|E, \\u044d|e, \\u042e|Iu, \\u044e|iu, \\u042f|Ia, \\u044f|ia, \\u042a| , \\u044a| , \\u042c| , \\u044c| , \\u0401|E, \\u0451|e\\\";\",\"predefined\":\"a:5:{i:0;s:9:\\\"com_login\\\";i:1;s:13:\\\"com_newsfeeds\\\";i:2;s:7:\\\"com_sef\\\";i:3;s:12:\\\"com_weblinks\\\";i:4;s:12:\\\"com_joomfish\\\";}\",\"serverNewVersionURL\":\"s:55:\\\"http:\\/\\/www.artio.cz\\/updates\\/joomla\\/joomsef4\\/version.xml\\\";\",\"serverAutoUpgrade\":\"s:40:\\\"http:\\/\\/www.artio.net\\/joomla-auto-upgrade\\\";\",\"serverLicenser\":\"s:34:\\\"http:\\/\\/www.artio.net\\/license-check\\\";\",\"langDomain\":\"a:0:{}\",\"disableNewSEF\":\"s:1:\\\"0\\\";\",\"dontRemoveSid\":\"b:1;\",\"setQueryString\":\"b:1;\",\"parseJoomlaSEO\":\"b:1;\",\"customNonSef\":\"s:0:\\\"\\\";\",\"jfBrowserLang\":\"b:1;\",\"jfLangCookie\":\"b:1;\",\"jfSubDomains\":\"a:0:{}\",\"contentUseIndex\":\"b:0;\",\"checkJunkUrls\":\"b:1;\",\"junkWords\":\"s:38:\\\"http:\\/\\/ http\\/\\/ https:\\/\\/ https\\/\\/ www. @\\\";\",\"junkExclude\":\"s:0:\\\"\\\";\",\"preventNonSefOverwrite\":\"b:1;\",\"mainLanguage\":\"i:0;\",\"allowUTF\":\"b:0;\",\"numberDuplicates\":\"b:0;\",\"artioUserName\":\"s:0:\\\"\\\";\",\"artioPassword\":\"s:0:\\\"\\\";\",\"artioDownloadId\":\"s:0:\\\"\\\";\",\"trace\":\"b:0;\",\"traceLevel\":\"i:3;\",\"autoCanonical\":\"b:1;\",\"sefComponentUrls\":\"b:0;\",\"versionChecker\":\"s:1:\\\"1\\\";\",\"tag_generator\":\"s:0:\\\"\\\";\",\"tag_googlekey\":\"s:0:\\\"\\\";\",\"tag_livekey\":\"s:0:\\\"\\\";\",\"tag_yahookey\":\"s:0:\\\"\\\";\",\"customMetaTags\":\"a:0:{}\",\"wwwHandling\":\"s:1:\\\"0\\\";\",\"enable_metadata\":\"s:1:\\\"1\\\";\",\"metadata_auto\":\"s:1:\\\"1\\\";\",\"prefer_joomsef_title\":\"s:1:\\\"1\\\";\",\"use_sitename\":\"s:1:\\\"2\\\";\",\"sitename_sep\":\"s:1:\\\"-\\\";\",\"rewrite_keywords\":\"s:1:\\\"1\\\";\",\"rewrite_description\":\"s:1:\\\"1\\\";\",\"prevent_dupl\":\"s:1:\\\"1\\\";\",\"check_base_href\":\"i:1;\",\"sitemap_changed\":\"b:1;\",\"sitemap_filename\":\"s:7:\\\"sitemap\\\";\",\"sitemap_indexed\":\"s:1:\\\"0\\\";\",\"sitemap_frequency\":\"s:6:\\\"weekly\\\";\",\"sitemap_priority\":\"s:3:\\\"0.5\\\";\",\"sitemap_show_date\":\"s:1:\\\"1\\\";\",\"sitemap_show_frequency\":\"s:1:\\\"1\\\";\",\"sitemap_show_priority\":\"s:1:\\\"1\\\";\",\"sitemap_pingauto\":\"s:1:\\\"1\\\";\",\"sitemap_yahooId\":\"s:0:\\\"\\\";\",\"sitemap_services\":\"a:0:{}\",\"external_nofollow\":\"s:1:\\\"0\\\";\",\"internal_enable\":\"s:1:\\\"1\\\";\",\"internal_nofollow\":\"s:1:\\\"0\\\";\",\"internal_newwindow\":\"s:1:\\\"0\\\";\",\"internal_maxlinks\":\"s:1:\\\"1\\\";\",\"artioFeedDisplay\":\"s:1:\\\"0\\\";\",\"artioFeedUrl\":\"s:37:\\\"http:\\/\\/www.artio.net\\/joomsef-news\\/rss\\\";\",\"fixIndexPhp\":\"b:1;\",\"fixDocumentFormat\":\"b:0;\",\"useGlobalFilters\":\"b:1;\",\"logErrors\":\"b:1;\",\"cronEnabled\":\"b:0;\",\"cronOnlyLocal\":\"b:1;\",\"cronKey\":\"s:0:\\\"\\\";\",\"autolock_urls\":\"b:0;\",\"update_urls\":\"b:0;\",\"langEnable\":\"s:1:\\\"0\\\";\",\"langPlacementJoomla\":\"s:1:\\\"0\\\";\",\"addLangMulti\":\"s:1:\\\"1\\\";\",\"alwaysUseLangJoomla\":\"s:1:\\\"1\\\";\",\"browserLangJoomla\":\"s:1:\\\"1\\\";\",\"langCookieJoomla\":\"s:1:\\\"1\\\";\",\"mainLanguageJoomla\":\"s:2:\\\"en\\\";\",\"subDomainsJoomla\":\"a:1:{s:2:\\\"en\\\";s:28:\\\"joomla25.siteincubator.co.uk\\\";}\",\"google_email\":\"s:0:\\\"\\\";\",\"google_password\":\"s:0:\\\"\\\";\",\"google_apikey\":\"s:0:\\\"\\\";\",\"google_id\":\"s:0:\\\"\\\";\",\"google_enable\":\"s:1:\\\"0\\\";\",\"google_exclude_ip\":\"s:0:\\\"\\\";\",\"google_exclude_level\":\"a:0:{}\",\"wrongDomainHandling\":\"s:1:\\\"0\\\";\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10076,'TCPDF','library','tcpdf','',0,1,1,0,'{\"legacy\":true,\"name\":\"TCPDF\",\"type\":\"library\",\"creationDate\":\"28 January 2011\",\"author\":\"Nicola Asuni\",\"copyright\":\"http:\\/\\/www.tcpdf.org\",\"authorEmail\":\"\",\"authorUrl\":\"http:\\/\\/www.tcpdf.org\",\"version\":\"2.5.0\",\"description\":\"Class for generating PDF files on-the-fly without requiring external extensions.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10034,'RSform! Frontend List','module','mod_forme_list','',0,1,0,0,'{\"legacy\":true,\"name\":\"RSform! Frontend List\",\"type\":\"module\",\"creationDate\":\"May 2007\",\"author\":\"www.rsjoomla.com\",\"copyright\":\"(C) 2007 www.rsjoomla.com. All rights reserved.\",\"authorEmail\":\"alex@rsjoomla.com\",\"authorUrl\":\"www.rsjoomla.com\",\"version\":\"1.0.4\",\"description\":\"This module is used to display forms created with RSform!.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10035,'RSform!','module','mod_forme','',0,1,0,0,'{\"legacy\":true,\"name\":\"RSform!\",\"type\":\"module\",\"creationDate\":\"May 2007\",\"author\":\"www.rsjoomla.com\",\"copyright\":\"(C) 2007 www.rsjoomla.com. All rights reserved.\",\"authorEmail\":\"alex@rsjoomla.com\",\"authorUrl\":\"www.rsjoomla.com\",\"version\":\"1.0.4\",\"description\":\"This module is used to display forms created with RSform!.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10036,'Content - mosforme','plugin','mosforme','content',0,0,1,0,'{\"legacy\":true,\"name\":\"Content - mosforme\",\"type\":\"plugin\",\"creationDate\":\"May 2007\",\"author\":\"www.rsjoomla.com\",\"copyright\":\"(C) 2007 www.rsjoomla.com\",\"authorEmail\":\"alex@rsjoomla.com\",\"authorUrl\":\"www.rsjoomla.com\",\"version\":\"1.0.4\",\"description\":\"Loads RSform! Forms within Content, Syntax: {mosforme 1}<br\\/><br\\/><font color=\\\"red\\\"><strong>Make sure you publish the mambot before you use it.<\\/strong><\\/font>\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10037,'Editor - JCE','plugin','jce','editors',0,1,1,0,'{\"legacy\":true,\"name\":\"Editor - JCE\",\"type\":\"plugin\",\"creationDate\":\"19 August 2012\",\"author\":\"Ryan Demmer\",\"copyright\":\"2006-2010 Ryan Demmer\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"http:\\/\\/www.joomlacontenteditor.net\",\"version\":\"2.2.6\",\"description\":\"WF_EDITOR_PLUGIN_DESC\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10038,'jce','component','com_jce','',1,1,0,0,'{\"legacy\":false,\"name\":\"JCE\",\"type\":\"component\",\"creationDate\":\"19 August 2012\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2006 - 2012 Ryan Demmer. All rights reserved\",\"authorEmail\":\"info@joomlacontenteditor.net\",\"authorUrl\":\"www.joomlacontenteditor.net\",\"version\":\"2.2.6\",\"description\":\"WF_ADMIN_DESC\",\"group\":\"\"}','{\"editor\":{\"verify_html\":\"1\",\"schema\":\"html5\",\"entity_encoding\":\"raw\",\"cleanup_pluginmode\":\"0\",\"forced_root_block\":\"p\",\"content_style_reset\":\"0\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"compress_javascript\":\"0\",\"compress_css\":\"1\",\"compress_gzip\":\"1\",\"use_cookies\":\"1\",\"custom_config\":\"\",\"callback_file\":\"\"}}','','',0,'0000-00-00 00:00:00',0,0),
	(10039,'Widgetkit','module','mod_widgetkit','',0,1,0,0,'{\"legacy\":true,\"name\":\"Widgetkit\",\"type\":\"module\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Widgetkit module for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"widget_id\":\"\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10040,'Widgetkit Twitter','module','mod_widgetkit_twitter','',0,1,0,0,'{\"legacy\":true,\"name\":\"Widgetkit Twitter\",\"type\":\"module\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Twitter module for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{\"style\":\"list\",\"from_user\":\"\",\"to_user\":\"\",\"ref_user\":\"\",\"hashtag\":\"\",\"word\":\"\",\"nots\":\"\",\"limit\":\"5\",\"image_size\":\"48\",\"show_image\":\"1\",\"show_author\":\"1\",\"show_date\":\"1\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10041,'System - Widgetkit','plugin','widgetkit_system','system',0,1,1,0,'{\"legacy\":false,\"name\":\"System - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10053,'Smart Search - ZOO','plugin','zoosmartsearch','finder',0,1,1,0,'{\"legacy\":false,\"name\":\"Smart Search - ZOO\",\"type\":\"plugin\",\"creationDate\":\"Febuary 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"2.5.0\",\"description\":\"Smart Search plugin for ZOO developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10042,'Content - Widgetkit','plugin','widgetkit_content','content',0,1,1,0,'{\"legacy\":false,\"name\":\"Content - Widgetkit\",\"type\":\"plugin\",\"creationDate\":\"May 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10052,'plg_system_jch_optimize','plugin','jch_optimize','system',0,0,1,0,'{\"legacy\":false,\"name\":\"plg_system_jch_optimize\",\"type\":\"plugin\",\"creationDate\":\"March 2010\",\"author\":\"Samuel Marshall\",\"copyright\":\"Copyright (C) 2010 Samuel Marshall. All rights reserved.\",\"authorEmail\":\"sdmarshall73@gmail.com\",\"authorUrl\":\"http:\\/\\/jch-optimize.sourceforge.net\",\"version\":\"2.0.1\",\"description\":\"JCH_OPTIMIZE_DESCRIPTION\",\"group\":\"\"}','{\"css\":\"0\",\"import\":\"1\",\"javascript\":\"0\",\"gzip\":\"1\",\"css_minify\":\"1\",\"js_minify\":\"1\",\"html_minify\":\"1\",\"defer_js\":\"0\",\"bottom_js\":\"2\",\"lifetime\":\"30\",\"excludeAllExtensions\":\"1\",\"excludeCss\":\"\",\"excludeJs\":\"\",\"excludeComponents\":\"\",\"jqueryNOConflict\":\"1\",\"jquery\":\"jquery.js\",\"customOrder\":\"mootools.js,jquery.js,jquery.innerfade.js\",\"htaccess\":\"0\",\"csg_enable\":\"0\",\"csg_file_output\":\"PNG\",\"csg_min_max_images\":\"1\",\"csg_direction\":\"vertical\",\"csg_wrap_images\":\"off\",\"csg_include_images\":\"\",\"csg_exclude_images\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10043,'System - Widgetkit ZOO','plugin','widgetkit_zoo','system',0,1,1,0,'{\"legacy\":false,\"name\":\"System - Widgetkit ZOO\",\"type\":\"plugin\",\"creationDate\":\"June 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"ZOO plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10044,'System - Widgetkit Joomla','plugin','widgetkit_joomla','system',0,1,1,0,'{\"legacy\":false,\"name\":\"System - Widgetkit Joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) 2007 - 2011 YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.0.0\",\"description\":\"Joomla Content plugin for Widgetkit developed by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10045,'widgetkit','component','com_widgetkit','',1,1,0,0,'{\"legacy\":false,\"name\":\"Widgetkit\",\"type\":\"component\",\"creationDate\":\"July 2012\",\"author\":\"YOOtheme\",\"copyright\":\"Copyright (C) YOOtheme GmbH\",\"authorEmail\":\"info@yootheme.com\",\"authorUrl\":\"http:\\/\\/www.yootheme.com\",\"version\":\"1.2.1\",\"description\":\"Widgetkit - A widget toolkit by YOOtheme (http:\\/\\/www.yootheme.com)\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10046,'Organic Site Credit','module','mod_organic_development','',0,1,1,0,'{\"legacy\":true,\"name\":\"Developed by Organic Development\",\"type\":\"module\",\"creationDate\":\"July 2007\",\"author\":\"Organic Development\",\"copyright\":\"(C) 2007 Organic Development.co.uk\",\"authorEmail\":\"oli@organicdevelopment.co.uk\",\"authorUrl\":\"http:\\/\\/www.organicdevelopment.co.uk\",\"version\":\"1\",\"description\":\"\\n\\t\\n      Organic Development Footer link module\\n\\n  \",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10047,'mod_organic_support','module','mod_organic_support','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_organic_support\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Organic Development\",\"copyright\":\"Copyright (C) 2005 - 2011 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"1.7.0\",\"description\":\"MOD_PROPERTIESS_SEARCH_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10048,'Zoo - Joomsef','sef_ext','ext_joomsef4_zoo','',0,1,1,0,'{\"legacy\":false,\"name\":\"Zoo\",\"type\":\"sef_ext\",\"creationDate\":\"Nov 2011\",\"author\":\"Organic Development ltd\",\"copyright\":\"Organic Development ltd\",\"authorEmail\":\"info@organic-development.com\",\"authorUrl\":\"www.organic-development.com\",\"version\":\"1\",\"description\":\"Adds SEO support for Zoo component.\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10063,'organic','template','organic','',1,1,1,0,'{\"legacy\":false,\"name\":\"bluestork\",\"type\":\"template\",\"creationDate\":\"07\\/02\\/09\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"TPL_BLUESTORK_XML_DESCRIPTION\",\"group\":\"\"}','{\"useRoundedCorners\":\"1\",\"showSiteName\":\"0\",\"textBig\":\"0\",\"highContrast\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10064,'Smart Icons','module','mod_smarticons','',1,1,2,0,'{\"legacy\":false,\"name\":\"Smart Icons\",\"type\":\"module\",\"creationDate\":\"Feb 2011\",\"author\":\"Bogdan-Ioan SUTA\",\"copyright\":\"(C) 2011 SUTA Bogdan-Ioan\",\"authorEmail\":\"bogdan.suta@naicum.ro\",\"authorUrl\":\"http:\\/\\/extensions.naicum.ro\",\"version\":\"1.1.0\",\"description\":\"Module for showing custom quick icons from the SmartIcons component\",\"group\":\"\"}','{\"cache\":\"1\",\"cache_time\":\"900\"}','','',0,'0000-00-00 00:00:00',0,0),
	(314,'mod_version','module','mod_version','',1,1,1,0,'{\"legacy\":false,\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\"}','{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10069,'COM_NONUMBERMANAGER','component','com_nonumbermanager','',1,1,0,0,'{\"legacy\":false,\"name\":\"COM_NONUMBERMANAGER\",\"type\":\"component\",\"creationDate\":\"August 2012\",\"author\":\"NoNumber (Peter van Westen)\",\"copyright\":\"Copyright \\u00a9 2012 NoNumber All Rights Reserved\",\"authorEmail\":\"peter@nonumber.nl\",\"authorUrl\":\"http:\\/\\/www.nonumber.nl\",\"version\":\"3.3.0\",\"description\":\"COM_NONUMBERMANAGER_DESC\",\"group\":\"\"}','{\"key\":\"b8c6a9MNVBC8V8NN\",\"check_data\":\"1\",\"hide_notinstalled\":\"1\",\"use_proxy\":\"0\",\"proxy_host\":\"\",\"proxy_port\":\"\",\"proxy_login\":\"\",\"proxy_password\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10077,'rsform','component','com_rsform','',1,1,0,0,'{\"legacy\":true,\"name\":\"RSForm\",\"type\":\"component\",\"creationDate\":\"July 2012\",\"author\":\"RSJoomla!\",\"copyright\":\"(C) 2007-2012 www.rsjoomla.com\",\"authorEmail\":\"support@rsjoomla.com\",\"authorUrl\":\"www.rsjoomla.com\",\"version\":\"1.4.0 R44\",\"description\":\"With RSForm!Pro you can quickly add forms to your Joomla! website.\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10073,'RSForm! Pro Module','module','mod_rsform','',0,1,0,0,'{\"legacy\":true,\"name\":\"RSForm! Pro Module\",\"type\":\"module\",\"creationDate\":\"July 2010\",\"author\":\"RSJoomla\",\"copyright\":\"(C) 2007-2010 www.rsjoomla.com\",\"authorEmail\":\"support@rsjoomla.com\",\"authorUrl\":\"www.rsjoomla.com\",\"version\":\"1.3.0\",\"description\":\"This module is used to display forms created with RSForm! Pro. Works with at least RSForm! Pro 1.3.0 REV30!\",\"group\":\"\"}','{\"formId\":\"1\",\"moduleclass_sfx\":\"\"}','','',0,'0000-00-00 00:00:00',0,0),
	(10074,'PLG_SYSTEM_NNFRAMEWORK','plugin','nnframework','system',0,1,1,0,'{\"legacy\":true,\"name\":\"PLG_SYSTEM_NNFRAMEWORK\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"NoNumber (Peter van Westen)\",\"copyright\":\"Copyright \\u00a9 2012 NoNumber All Rights Reserved\",\"authorEmail\":\"peter@nonumber.nl\",\"authorUrl\":\"http:\\/\\/www.nonumber.nl\",\"version\":\"12.8.2\",\"description\":\"PLG_SYSTEM_NNFRAMEWORK_DESC\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0),
	(10075,'plg_quickicon_jcefilebrowser','plugin','jcefilebrowser','quickicon',0,1,1,0,'{\"legacy\":false,\"name\":\"plg_quickicon_jcefilebrowser\",\"type\":\"plugin\",\"creationDate\":\"April 2012\",\"author\":\"Ryan Demmer\",\"copyright\":\"Copyright (C) 2012 Ryan Demmer. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"PLG_QUICKICON_JCEFILEBROWSER_XML_DESCRIPTION\",\"group\":\"\"}','{}','','',0,'0000-00-00 00:00:00',0,0),
	(10078,'bootstrap_admin','template','bootstrap_admin','',1,1,1,0,'{\"legacy\":false,\"name\":\"bootstrap_admin\",\"type\":\"template\",\"creationDate\":\"07\\/02\\/09\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"2.5.0\",\"description\":\"TPL_BLUESTORK_XML_DESCRIPTION\",\"group\":\"\"}','','','',0,'0000-00-00 00:00:00',0,0);

/*!40000 ALTER TABLE `#__extensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__finder_filters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_filters`;

CREATE TABLE `#__finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links`;

CREATE TABLE `#__finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms0
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms0`;

CREATE TABLE `#__finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms1`;

CREATE TABLE `#__finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms2
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms2`;

CREATE TABLE `#__finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms3
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms3`;

CREATE TABLE `#__finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms4
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms4`;

CREATE TABLE `#__finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms5
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms5`;

CREATE TABLE `#__finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms6
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms6`;

CREATE TABLE `#__finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms7
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms7`;

CREATE TABLE `#__finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms8`;

CREATE TABLE `#__finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_terms9
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_terms9`;

CREATE TABLE `#__finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termsa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termsa`;

CREATE TABLE `#__finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termsb
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termsb`;

CREATE TABLE `#__finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termsc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termsc`;

CREATE TABLE `#__finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termsd
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termsd`;

CREATE TABLE `#__finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termse`;

CREATE TABLE `#__finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_links_termsf
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_links_termsf`;

CREATE TABLE `#__finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_taxonomy`;

CREATE TABLE `#__finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__finder_taxonomy` WRITE;
/*!40000 ALTER TABLE `#__finder_taxonomy` DISABLE KEYS */;

INSERT INTO `#__finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`)
VALUES
	(1,0,'ROOT',0,0,0);

/*!40000 ALTER TABLE `#__finder_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__finder_taxonomy_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_taxonomy_map`;

CREATE TABLE `#__finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_terms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_terms`;

CREATE TABLE `#__finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__finder_terms_common
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_terms_common`;

CREATE TABLE `#__finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__finder_terms_common` WRITE;
/*!40000 ALTER TABLE `#__finder_terms_common` DISABLE KEYS */;

INSERT INTO `#__finder_terms_common` (`term`, `language`)
VALUES
	('a','en'),
	('about','en'),
	('after','en'),
	('ago','en'),
	('all','en'),
	('am','en'),
	('an','en'),
	('and','en'),
	('ani','en'),
	('any','en'),
	('are','en'),
	('aren\'t','en'),
	('as','en'),
	('at','en'),
	('be','en'),
	('but','en'),
	('by','en'),
	('for','en'),
	('from','en'),
	('get','en'),
	('go','en'),
	('how','en'),
	('if','en'),
	('in','en'),
	('into','en'),
	('is','en'),
	('isn\'t','en'),
	('it','en'),
	('its','en'),
	('me','en'),
	('more','en'),
	('most','en'),
	('must','en'),
	('my','en'),
	('new','en'),
	('no','en'),
	('none','en'),
	('not','en'),
	('noth','en'),
	('nothing','en'),
	('of','en'),
	('off','en'),
	('often','en'),
	('old','en'),
	('on','en'),
	('onc','en'),
	('once','en'),
	('onli','en'),
	('only','en'),
	('or','en'),
	('other','en'),
	('our','en'),
	('ours','en'),
	('out','en'),
	('over','en'),
	('page','en'),
	('she','en'),
	('should','en'),
	('small','en'),
	('so','en'),
	('some','en'),
	('than','en'),
	('thank','en'),
	('that','en'),
	('the','en'),
	('their','en'),
	('theirs','en'),
	('them','en'),
	('then','en'),
	('there','en'),
	('these','en'),
	('they','en'),
	('this','en'),
	('those','en'),
	('thus','en'),
	('time','en'),
	('times','en'),
	('to','en'),
	('too','en'),
	('true','en'),
	('under','en'),
	('until','en'),
	('up','en'),
	('upon','en'),
	('use','en'),
	('user','en'),
	('users','en'),
	('veri','en'),
	('version','en'),
	('very','en'),
	('via','en'),
	('want','en'),
	('was','en'),
	('way','en'),
	('were','en'),
	('what','en'),
	('when','en'),
	('where','en'),
	('whi','en'),
	('which','en'),
	('who','en'),
	('whom','en'),
	('whose','en'),
	('why','en'),
	('wide','en'),
	('will','en'),
	('with','en'),
	('within','en'),
	('without','en'),
	('would','en'),
	('yes','en'),
	('yet','en'),
	('you','en'),
	('your','en'),
	('yours','en');

/*!40000 ALTER TABLE `#__finder_terms_common` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__finder_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_tokens`;

CREATE TABLE `#__finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;



# Dump of table #__finder_tokens_aggregate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_tokens_aggregate`;

CREATE TABLE `#__finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;



# Dump of table #__finder_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__finder_types`;

CREATE TABLE `#__finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__languages`;

CREATE TABLE `#__languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_ordering` (`ordering`),
  KEY `idx_access` (`access`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__languages` WRITE;
/*!40000 ALTER TABLE `#__languages` DISABLE KEYS */;

INSERT INTO `#__languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`)
VALUES
	(1,'en-GB','English (UK)','English (UK)','en','en','','','','',1,0,1);

/*!40000 ALTER TABLE `#__languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__menu`;

CREATE TABLE `#__menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'The relative ordering of the menu item in the tree.',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(333)),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__menu` WRITE;
/*!40000 ALTER TABLE `#__menu` DISABLE KEYS */;

INSERT INTO `#__menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `ordering`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`)
VALUES
	(1,'','Menu_Item_Root',X'726F6F74','','','','',1,0,0,0,0,0,'0000-00-00 00:00:00',0,0,'',0,'',0,127,0,'*',0),
	(2,'menu','com_banners',X'42616E6E657273','','Banners','index.php?option=com_banners','component',0,1,1,4,0,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',1,10,0,'*',1),
	(3,'menu','com_banners',X'42616E6E657273','','Banners/Banners','index.php?option=com_banners','component',0,2,2,4,0,0,'0000-00-00 00:00:00',0,0,'class:banners',0,'',2,3,0,'*',1),
	(4,'menu','com_banners_categories',X'43617465676F72696573','','Banners/Categories','index.php?option=com_categories&extension=com_banners','component',0,2,2,6,0,0,'0000-00-00 00:00:00',0,0,'class:banners-cat',0,'',4,5,0,'*',1),
	(5,'menu','com_banners_clients',X'436C69656E7473','','Banners/Clients','index.php?option=com_banners&view=clients','component',0,2,2,4,0,0,'0000-00-00 00:00:00',0,0,'class:banners-clients',0,'',6,7,0,'*',1),
	(6,'menu','com_banners_tracks',X'547261636B73','','Banners/Tracks','index.php?option=com_banners&view=tracks','component',0,2,2,4,0,0,'0000-00-00 00:00:00',0,0,'class:banners-tracks',0,'',8,9,0,'*',1),
	(7,'menu','com_contact',X'436F6E7461637473','','Contacts','index.php?option=com_contact','component',0,1,1,8,0,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',11,16,0,'*',1),
	(8,'menu','com_contact',X'436F6E7461637473','','Contacts/Contacts','index.php?option=com_contact','component',0,7,2,8,0,0,'0000-00-00 00:00:00',0,0,'class:contact',0,'',12,13,0,'*',1),
	(9,'menu','com_contact_categories',X'43617465676F72696573','','Contacts/Categories','index.php?option=com_categories&extension=com_contact','component',0,7,2,6,0,0,'0000-00-00 00:00:00',0,0,'class:contact-cat',0,'',14,15,0,'*',1),
	(10,'menu','com_messages',X'4D6573736167696E67','','Messaging','index.php?option=com_messages','component',0,1,1,15,0,0,'0000-00-00 00:00:00',0,0,'class:messages',0,'',17,22,0,'*',1),
	(11,'menu','com_messages_add',X'4E65772050726976617465204D657373616765','','Messaging/New Private Message','index.php?option=com_messages&task=message.add','component',0,10,2,15,0,0,'0000-00-00 00:00:00',0,0,'class:messages-add',0,'',18,19,0,'*',1),
	(12,'menu','com_messages_read',X'526561642050726976617465204D657373616765','','Messaging/Read Private Message','index.php?option=com_messages','component',0,10,2,15,0,0,'0000-00-00 00:00:00',0,0,'class:messages-read',0,'',20,21,0,'*',1),
	(13,'menu','com_newsfeeds',X'4E657773204665656473','','News Feeds','index.php?option=com_newsfeeds','component',0,1,1,17,0,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',23,28,0,'*',1),
	(14,'menu','com_newsfeeds_feeds',X'4665656473','','News Feeds/Feeds','index.php?option=com_newsfeeds','component',0,13,2,17,0,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds',0,'',24,25,0,'*',1),
	(15,'menu','com_newsfeeds_categories',X'43617465676F72696573','','News Feeds/Categories','index.php?option=com_categories&extension=com_newsfeeds','component',0,13,2,6,0,0,'0000-00-00 00:00:00',0,0,'class:newsfeeds-cat',0,'',26,27,0,'*',1),
	(16,'menu','com_redirect',X'5265646972656374','','Redirect','index.php?option=com_redirect','component',0,1,1,24,0,0,'0000-00-00 00:00:00',0,0,'class:redirect',0,'',41,42,0,'*',1),
	(17,'menu','com_search',X'426173696320536561726368','','Basic Search','index.php?option=com_search','component',0,1,1,19,0,0,'0000-00-00 00:00:00',0,0,'class:search',0,'',33,34,0,'*',1),
	(18,'menu','com_weblinks',X'5765626C696E6B73','','Weblinks','index.php?option=com_weblinks','component',0,1,1,21,0,0,'0000-00-00 00:00:00',0,0,'class:weblinks',0,'',35,40,0,'*',1),
	(19,'menu','com_weblinks_links',X'4C696E6B73','','Weblinks/Links','index.php?option=com_weblinks','component',0,18,2,21,0,0,'0000-00-00 00:00:00',0,0,'class:weblinks',0,'',36,37,0,'*',1),
	(20,'menu','com_weblinks_categories',X'43617465676F72696573','','Weblinks/Categories','index.php?option=com_categories&extension=com_weblinks','component',0,18,2,6,0,0,'0000-00-00 00:00:00',0,0,'class:weblinks-cat',0,'',38,39,0,'*',1),
	(21,'menu','com_finder',X'536D61727420536561726368','','Smart Search','index.php?option=com_finder','component',0,1,1,27,0,0,'0000-00-00 00:00:00',0,0,'class:finder',0,'',31,32,0,'*',1),
	(101,'mainmenu','Home',X'686F6D65','','home','index.php?option=com_content&view=article&id=2','component',1,1,1,22,0,0,'0000-00-00 00:00:00',0,1,'',0,'{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}',29,30,1,'*',0),
	(140,'main','COM_SMARTICONS_MENU',X'636F6D2D736D61727469636F6E732D6D656E75','','com-smarticons-menu','index.php?option=com_smarticons','component',0,1,1,10065,0,0,'0000-00-00 00:00:00',0,1,'../media/com_smarticons/images/SmartIcons16x16.png',0,'',47,48,0,'',1),
	(194,'main','ZOO',X'7A6F6F','','zoo','index.php?option=com_zoo','component',0,1,1,10009,0,0,'0000-00-00 00:00:00',0,1,'components/com_zoo/assets/images/zoo_16.png',0,'',51,52,0,'',1),
	(139,'main','COM_AKEEBA',X'636F6D2D616B65656261','','com-akeeba','index.php?option=com_akeeba','component',0,1,1,10057,0,0,'0000-00-00 00:00:00',0,1,'components/com_akeeba/assets/images/akeeba-16.png',0,'',45,46,0,'',1),
	(22,'menu','com_joomlaupdate',X'4A6F6F6D6C612120557064617465','','Joomla! Update','index.php?option=com_joomlaupdate','component',0,1,1,28,0,0,'0000-00-00 00:00:00',0,0,'class:joomlaupdate',0,'',43,44,0,'*',1),
	(209,'main','COM_SEF_SUPPORT',X'636F6D2D7365662D737570706F7274','','com-sef/com-sef-support','index.php?option=com_sef&controller=info&task=help','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-help.png',0,'',80,81,0,'',1),
	(208,'main','COM_SEF_UPGRADE',X'636F6D2D7365662D75706772616465','','com-sef/com-sef-upgrade','index.php?option=com_sef&task=showUpgrade','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-update.png',0,'',78,79,0,'',1),
	(207,'main','COM_SEF_SEPARATOR2',X'636F6D2D7365662D736570617261746F7232','','com-sef/com-sef-separator2','index.php?option=com_sef','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'separator',0,'',76,77,0,'',1),
	(206,'main','COM_SEF_STATISTICS',X'636F6D2D7365662D73746174697374696373','','com-sef/com-sef-statistics','index.php?option=com_sef&controller=statistics','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-statistics.png',0,'',74,75,0,'',1),
	(205,'main','COM_SEF_REDIRECTS',X'636F6D2D7365662D726564697265637473','','com-sef/com-sef-redirects','index.php?option=com_sef&controller=movedurls','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-301-redirects.png',0,'',72,73,0,'',1),
	(204,'main','COM_SEF_SITEMAP',X'636F6D2D7365662D736974656D6170','','com-sef/com-sef-sitemap','index.php?option=com_sef&controller=sitemap','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-manage-sitemap.png',0,'',70,71,0,'',1),
	(203,'main','COM_SEF_METATAGS',X'636F6D2D7365662D6D65746174616773','','com-sef/com-sef-metatags','index.php?option=com_sef&controller=metatags','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-manage-tags.png',0,'',68,69,0,'',1),
	(201,'main','COM_SEF_SEPARATOR1',X'636F6D2D7365662D736570617261746F7231','','com-sef/com-sef-separator1','index.php?option=com_sef','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'separator',0,'',64,65,0,'',1),
	(202,'main','COM_SEF_SEFURLS',X'636F6D2D7365662D73656675726C73','','com-sef/com-sef-sefurls','index.php?option=com_sef&controller=sefurls&viewmode=3','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-url-edit.png',0,'',66,67,0,'',1),
	(200,'main','COM_SEF_HTACCESS',X'636F6D2D7365662D6874616363657373','','com-sef/com-sef-htaccess','index.php?option=com_sef&controller=htaccess','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-edit.png',0,'',62,63,0,'',1),
	(227,'main','COM_RSFORM_UPDATES',X'636F6D2D7273666F726D2D75706461746573','','rsformpro/com-rsform-updates','index.php?option=com_rsform&task=updates.manage','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',102,103,0,'',1),
	(226,'main','COM_RSFORM_BACKUP_RESTORE',X'636F6D2D7273666F726D2D6261636B75702D726573746F7265','','rsformpro/com-rsform-backup-restore','index.php?option=com_rsform&task=backup.restore','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',100,101,0,'',1),
	(225,'main','COM_RSFORM_CONFIGURATION',X'636F6D2D7273666F726D2D636F6E66696775726174696F6E','','rsformpro/com-rsform-configuration','index.php?option=com_rsform&task=configuration.edit','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',98,99,0,'',1),
	(214,'main','WF_MENU_INSTALL',X'77662D6D656E752D696E7374616C6C','','jce/wf-menu-install','index.php?option=com_jce&view=installer','component',0,210,2,10038,0,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-install.png',0,'',90,91,0,'',1),
	(210,'main','JCE',X'6A6365','','jce','index.php?option=com_jce','component',0,1,1,10038,0,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/logo.png',0,'',83,92,0,'',1),
	(211,'main','WF_MENU_CPANEL',X'77662D6D656E752D6370616E656C','','jce/wf-menu-cpanel','index.php?option=com_jce','component',0,210,2,10038,0,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-cpanel.png',0,'',84,85,0,'',1),
	(212,'main','WF_MENU_CONFIG',X'77662D6D656E752D636F6E666967','','jce/wf-menu-config','index.php?option=com_jce&view=config','component',0,210,2,10038,0,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-config.png',0,'',86,87,0,'',1),
	(213,'main','WF_MENU_PROFILES',X'77662D6D656E752D70726F66696C6573','','jce/wf-menu-profiles','index.php?option=com_jce&view=profiles','component',0,210,2,10038,0,0,'0000-00-00 00:00:00',0,1,'components/com_jce/media/img/menu/jce-profiles.png',0,'',88,89,0,'',1),
	(195,'main','COM_WIDGETKIT',X'636F6D2D7769646765746B6974','','com-widgetkit','index.php?option=com_widgetkit','component',0,1,1,10045,0,0,'0000-00-00 00:00:00',0,1,'components/com_widgetkit/images/widgetkit_16.png',0,'',53,54,0,'',1),
	(193,'menu','COM_NONUMBERMANAGER',X'6E6F6E756D6265726D616E61676572','','nonumbermanager','index.php?option=com_nonumbermanager','component',1,1,1,10069,0,0,'0000-00-00 00:00:00',0,1,'components/com_nonumbermanager/images/icon-nonumbermanager.png',0,'',49,50,0,'*',1),
	(223,'main','COM_RSFORM_MANAGE_FORMS',X'636F6D2D7273666F726D2D6D616E6167652D666F726D73','','rsformpro/com-rsform-manage-forms','index.php?option=com_rsform&task=forms.manage','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',94,95,0,'',1),
	(224,'main','COM_RSFORM_MANAGE_SUBMISSIONS',X'636F6D2D7273666F726D2D6D616E6167652D7375626D697373696F6E73','','rsformpro/com-rsform-manage-submissions','index.php?option=com_rsform&task=submissions.manage','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',96,97,0,'',1),
	(222,'main','RSFormPro',X'7273666F726D70726F','','rsformpro','index.php?option=com_rsform','component',0,1,1,10077,0,0,'0000-00-00 00:00:00',0,1,'components/com_rsform/assets/images/rsformpro.gif',0,'',93,106,0,'',1),
	(199,'main','COM_SEF_EXTENSIONS',X'636F6D2D7365662D657874656E73696F6E73','','com-sef/com-sef-extensions','index.php?option=com_sef&controller=extension','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-sefplugin.png',0,'',60,61,0,'',1),
	(198,'main','COM_SEF_CONFIG',X'636F6D2D7365662D636F6E666967','','com-sef/com-sef-config','index.php?option=com_sef&controller=config&task=edit','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-sefconfig.png',0,'',58,59,0,'',1),
	(196,'main','COM_SEF',X'636F6D2D736566','','com-sef','index.php?option=com_sef','component',0,1,1,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon.png',0,'',55,82,0,'',1),
	(197,'main','COM_SEF_CPANEL',X'636F6D2D7365662D6370616E656C','','com-sef/com-sef-cpanel','index.php?option=com_sef','component',0,196,2,10032,0,0,'0000-00-00 00:00:00',0,1,'components/com_sef/assets/images/icon-16-sefcpanel.png',0,'',56,57,0,'',1),
	(228,'main','COM_RSFORM_PLUGINS',X'636F6D2D7273666F726D2D706C7567696E73','','rsformpro/com-rsform-plugins','index.php?option=com_rsform&task=goto.plugins','component',0,222,2,10077,0,0,'0000-00-00 00:00:00',0,1,'class:component',0,'',104,105,0,'',1);

/*!40000 ALTER TABLE `#__menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__menu_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__menu_types`;

CREATE TABLE `#__menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__menu_types` WRITE;
/*!40000 ALTER TABLE `#__menu_types` DISABLE KEYS */;

INSERT INTO `#__menu_types` (`id`, `menutype`, `title`, `description`)
VALUES
	(1,'mainmenu','Main Menu','The main menu for the site');

/*!40000 ALTER TABLE `#__menu_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__messages`;

CREATE TABLE `#__messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__messages_cfg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__messages_cfg`;

CREATE TABLE `#__messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__modules`;

CREATE TABLE `#__modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__modules` WRITE;
/*!40000 ALTER TABLE `#__modules` DISABLE KEYS */;

INSERT INTO `#__modules` (`id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`)
VALUES
	(1,'Main Menu','','',1,'menu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',1,1,'{\"menutype\":\"mainmenu\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"_menu\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\"}',0,'*'),
	(2,'Login','','',1,'login',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_login',1,1,'',1,'*'),
	(3,'Popular Articles','','',3,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_popular',3,1,'{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),
	(4,'Recently Added Articles','','',4,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_latest',3,1,'{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),
	(8,'Toolbar','','',1,'toolbar',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_toolbar',3,1,'',1,'*'),
	(9,'Quick Icons','','',1,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',-2,'mod_quickicon',3,1,'',1,'*'),
	(10,'Logged-in Users','','',2,'cpanel',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_logged',3,1,'{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"automatic_title\":\"1\"}',1,'*'),
	(12,'Admin Menu','','',1,'menu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_menu',3,1,'{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}',1,'*'),
	(13,'Admin Submenu','','',1,'submenu',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_submenu',3,1,'',1,'*'),
	(14,'User Status','','',2,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_status',3,1,'',1,'*'),
	(15,'Title','','',1,'title',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_title',3,1,'',1,'*'),
	(79,'Multilanguage status','','',1,'status',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',0,'mod_multilangstatus',3,1,'{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),
	(84,'ZOO Quick Icons','','',101,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_zooquickicon',1,1,'',1,'*'),
	(91,'Akeeba Backup Notification Module','','',99,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',-2,'mod_akadmin',1,1,'',1,'*'),
	(90,'Joomla Version','','',1,'footer',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_version',3,1,'{\"format\":\"short\",\"product\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}',1,'*'),
	(95,'Smart Icons','','',1,'icon',0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,'mod_smarticons',3,1,'',1,'*');

/*!40000 ALTER TABLE `#__modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__modules_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__modules_menu`;

CREATE TABLE `#__modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__modules_menu` WRITE;
/*!40000 ALTER TABLE `#__modules_menu` DISABLE KEYS */;

INSERT INTO `#__modules_menu` (`moduleid`, `menuid`)
VALUES
	(1,0),
	(2,0),
	(3,0),
	(4,0),
	(8,0),
	(9,0),
	(10,0),
	(12,0),
	(13,0),
	(14,0),
	(15,0),
	(79,0),
	(84,0),
	(90,0),
	(91,0),
	(95,0);

/*!40000 ALTER TABLE `#__modules_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__newsfeeds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__newsfeeds`;

CREATE TABLE `#__newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__overrider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__overrider`;

CREATE TABLE `#__overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__redirect_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__redirect_links`;

CREATE TABLE `#__redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_component_type_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_component_type_fields`;

CREATE TABLE `#__rsform_component_type_fields` (
  `ComponentTypeFieldId` int(11) NOT NULL AUTO_INCREMENT,
  `ComponentTypeId` int(11) NOT NULL DEFAULT '0',
  `FieldName` text NOT NULL,
  `FieldType` enum('hidden','hiddenparam','textbox','textarea','select','emailattach') NOT NULL DEFAULT 'hidden',
  `FieldValues` text NOT NULL,
  `Ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ComponentTypeFieldId`),
  KEY `ComponentTypeId` (`ComponentTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_component_type_fields` WRITE;
/*!40000 ALTER TABLE `#__rsform_component_type_fields` DISABLE KEYS */;

INSERT INTO `#__rsform_component_type_fields` (`ComponentTypeFieldId`, `ComponentTypeId`, `FieldName`, `FieldType`, `FieldValues`, `Ordering`)
VALUES
	(2,1,'NAME','textbox','',1),
	(3,1,'CAPTION','textbox','',2),
	(4,1,'REQUIRED','select','NO\r\nYES',3),
	(5,1,'SIZE','textbox','20',4),
	(6,1,'MAXSIZE','textbox','',5),
	(7,1,'VALIDATIONRULE','select','//<code>\r\nreturn RSgetValidationRules();\r\n//</code>',6),
	(8,1,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',7),
	(9,1,'ADDITIONALATTRIBUTES','textarea','',8),
	(10,1,'DEFAULTVALUE','textarea','',9),
	(11,1,'DESCRIPTION','textarea','',11),
	(12,1,'COMPONENTTYPE','hidden','1',15),
	(13,2,'NAME','textbox','',1),
	(14,2,'CAPTION','textbox','',2),
	(15,2,'REQUIRED','select','NO\r\nYES',3),
	(16,2,'COLS','textbox','50',4),
	(17,2,'ROWS','textbox','5',5),
	(18,2,'VALIDATIONRULE','select','//<code>\r\nreturn RSgetValidationRules();\r\n//</code>',6),
	(19,2,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',7),
	(20,2,'ADDITIONALATTRIBUTES','textarea','',8),
	(21,2,'DEFAULTVALUE','textarea','',9),
	(22,2,'DESCRIPTION','textarea','',10),
	(23,2,'COMPONENTTYPE','hidden','2',10),
	(24,3,'NAME','textbox','',1),
	(25,3,'CAPTION','textbox','',2),
	(26,3,'SIZE','textbox','',3),
	(27,3,'MULTIPLE','select','NO\r\nYES',4),
	(28,3,'ITEMS','textarea','',5),
	(29,3,'REQUIRED','select','NO\r\nYES',6),
	(30,3,'ADDITIONALATTRIBUTES','textarea','',7),
	(31,3,'DESCRIPTION','textarea','',8),
	(32,3,'COMPONENTTYPE','hidden','3',10),
	(33,4,'NAME','textbox','',1),
	(34,4,'CAPTION','textbox','',2),
	(35,4,'ITEMS','textarea','',3),
	(36,4,'FLOW','select','HORIZONTAL\r\nVERTICAL',4),
	(37,4,'REQUIRED','select','NO\r\nYES',5),
	(38,4,'ADDITIONALATTRIBUTES','textarea','',6),
	(39,4,'DESCRIPTION','textarea','',6),
	(40,4,'COMPONENTTYPE','hidden','4',7),
	(41,5,'NAME','textbox','',1),
	(42,5,'CAPTION','textbox','',2),
	(43,5,'ITEMS','textarea','',3),
	(44,5,'FLOW','select','HORIZONTAL\r\nVERTICAL',4),
	(45,5,'REQUIRED','select','NO\r\nYES',5),
	(46,5,'ADDITIONALATTRIBUTES','textarea','',6),
	(47,5,'DESCRIPTION','textarea','',6),
	(48,5,'COMPONENTTYPE','hidden','5',7),
	(49,6,'NAME','textbox','',1),
	(50,6,'CAPTION','textbox','',2),
	(51,6,'REQUIRED','select','NO\r\nYES',3),
	(52,6,'DATEFORMAT','textbox','DDMMYYYY',4),
	(53,6,'CALENDARLAYOUT','select','FLAT\r\nPOPUP',5),
	(54,6,'ADDITIONALATTRIBUTES','textarea','',6),
	(55,6,'DESCRIPTION','textarea','',7),
	(56,6,'COMPONENTTYPE','hidden','6',8),
	(57,7,'NAME','textbox','',1),
	(58,7,'CAPTION','textbox','',2),
	(59,7,'LABEL','textbox','',3),
	(60,7,'RESET','select','NO\r\nYES',4),
	(61,7,'RESETLABEL','textbox','',5),
	(62,7,'ADDITIONALATTRIBUTES','textarea','',6),
	(63,7,'DESCRIPTION','textarea','',7),
	(64,7,'COMPONENTTYPE','hidden','7',8),
	(65,8,'NAME','textbox','',1),
	(66,8,'CAPTION','textbox','',2),
	(67,8,'LENGTH','textbox','4',3),
	(68,8,'BACKGROUNDCOLOR','textbox','#FFFFFF',4),
	(69,8,'TEXTCOLOR','textbox','#000000',5),
	(70,8,'TYPE','select','ALPHA\r\nNUMERIC\r\nALPHANUMERIC',6),
	(71,8,'ADDITIONALATTRIBUTES','textarea','style=\"text-align:center;width:75px;\"',7),
	(72,8,'DESCRIPTION','textarea','',9),
	(73,8,'COMPONENTTYPE','hidden','8',9),
	(74,9,'NAME','textbox','',1),
	(75,9,'CAPTION','textbox','',2),
	(76,9,'FILESIZE','textbox','',3),
	(77,9,'REQUIRED','select','NO\r\nYES',4),
	(78,9,'ACCEPTEDFILES','textarea','',5),
	(79,9,'DESTINATION','textbox','//<code>\r\nreturn JPATH_SITE.DS.\'components\'.DS.\'com_rsform\'.DS.\'uploads\'.DS;\r\n//</code>',6),
	(80,9,'ADDITIONALATTRIBUTES','textarea','',7),
	(81,9,'DESCRIPTION','textarea','',8),
	(82,9,'COMPONENTTYPE','hidden','9',9),
	(83,10,'NAME','textbox','',1),
	(84,10,'TEXT','textarea','',1),
	(85,10,'COMPONENTTYPE','hidden','10',9),
	(86,11,'NAME','textbox','',1),
	(87,11,'DEFAULTVALUE','textarea','',1),
	(88,11,'ADDITIONALATTRIBUTES','textarea','',1),
	(89,11,'COMPONENTTYPE','hidden','11',9),
	(118,12,'COMPONENTTYPE','hidden','12',10),
	(117,12,'ADDITIONALATTRIBUTES','textarea','',9),
	(144,3,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(115,12,'RESETLABEL','textbox','',7),
	(114,12,'RESET','select','NO\r\nYES',6),
	(113,12,'IMAGERESET','textbox','',5),
	(112,12,'IMAGEBUTTON','textbox','',4),
	(111,12,'LABEL','textbox','',3),
	(110,12,'CAPTION','textbox','',2),
	(109,12,'NAME','textbox','',1),
	(119,13,'NAME','textbox','',1),
	(120,13,'CAPTION','textbox','',3),
	(121,13,'LABEL','textbox','',2),
	(122,13,'RESET','select','NO\r\nYES',6),
	(123,13,'RESETLABEL','textbox','',7),
	(125,13,'ADDITIONALATTRIBUTES','textarea','',9),
	(126,13,'COMPONENTTYPE','hidden','13',10),
	(127,14,'NAME','textbox','',1),
	(128,14,'CAPTION','textbox','',2),
	(129,14,'REQUIRED','select','NO\r\nYES',3),
	(130,14,'SIZE','textbox','',4),
	(131,14,'MAXSIZE','textbox','',5),
	(132,14,'DEFAULTVALUE','textarea','',6),
	(133,14,'ADDITIONALATTRIBUTES','textarea','',7),
	(134,14,'COMPONENTTYPE','hidden','14',8),
	(135,15,'NAME','textbox','',1),
	(138,15,'LENGTH','textbox','8',4),
	(140,15,'ADDITIONALATTRIBUTES','textarea','',7),
	(141,15,'COMPONENTTYPE','hidden','15',8),
	(142,14,'DESCRIPTION','textarea','',100),
	(143,8,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(145,4,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(146,5,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(147,6,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(148,14,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(149,9,'VALIDATIONMESSAGE','textarea','INVALIDINPUT',100),
	(150,8,'FLOW','select','VERTICAL\r\nHORIZONTAL',7),
	(151,8,'SHOWREFRESH','select','NO\r\nYES',8),
	(152,8,'REFRESHTEXT','textbox','REFRESH',11),
	(153,6,'READONLY','select','NO\r\nYES',6),
	(154,12,'DESCRIPTION','textarea','',10),
	(155,6,'POPUPLABEL','textbox','...',6),
	(157,15,'CHARACTERS','select','ALPHANUMERIC\r\nALPHA\r\nNUMERIC',3),
	(160,2,'WYSIWYG','select','NO\r\nYES',11),
	(161,8,'SIZE','textbox','15',12),
	(162,8,'IMAGETYPE','select','FREETYPE\r\nNOFREETYPE\r\nINVISIBLE',3),
	(163,13,'BUTTONTYPE','select','TYPEINPUT\nTYPEBUTTON',9),
	(164,7,'BUTTONTYPE','select','TYPEINPUT\nTYPEBUTTON',6),
	(165,1,'VALIDATIONEXTRA','textbox','',6),
	(166,2,'VALIDATIONEXTRA','textbox','',6),
	(167,14,'VALIDATIONRULE','select','//<code>\r\nreturn RSgetValidationRules();\r\n//</code>',9),
	(168,9,'PREFIX','textarea','',6),
	(169,13,'PREVBUTTON','textbox','//<code>\r\nreturn JText::_(\'PREV\');\r\n//</code>',8),
	(170,41,'NAME','textbox','',1),
	(171,41,'COMPONENTTYPE','hidden','41',5),
	(172,41,'NEXTBUTTON','textbox','//<code>\r\nreturn JText::_(\'NEXT\');\r\n//</code>',2),
	(173,41,'PREVBUTTON','textbox','//<code>\r\nreturn JText::_(\'PREV\');\r\n//</code>',3),
	(174,41,'ADDITIONALATTRIBUTES','textarea','',4),
	(175,41,'VALIDATENEXTPAGE','select','NO\r\nYES',5),
	(176,6,'MINDATE','textbox','',5),
	(177,6,'MAXDATE','textbox','',5),
	(178,6,'DEFAULTVALUE','textarea','',2),
	(179,9,'EMAILATTACH','emailattach','',102),
	(180,41,'DISPLAYPROGRESS','select','NO\r\nYES',6),
	(181,41,'DISPLAYPROGRESSMSG','textarea','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>',7),
	(182,0,'','hidden','',0),
	(183,13,'DISPLAYPROGRESS','select','NO\r\nYES',8),
	(184,13,'DISPLAYPROGRESSMSG','textarea','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>',8);

/*!40000 ALTER TABLE `#__rsform_component_type_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_component_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_component_types`;

CREATE TABLE `#__rsform_component_types` (
  `ComponentTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `ComponentTypeName` text NOT NULL,
  PRIMARY KEY (`ComponentTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_component_types` WRITE;
/*!40000 ALTER TABLE `#__rsform_component_types` DISABLE KEYS */;

INSERT INTO `#__rsform_component_types` (`ComponentTypeId`, `ComponentTypeName`)
VALUES
	(1,'textBox'),
	(2,'textArea'),
	(3,'selectList'),
	(4,'checkboxGroup'),
	(5,'radioGroup'),
	(6,'calendar'),
	(7,'button'),
	(8,'captcha'),
	(9,'fileUpload'),
	(10,'freeText'),
	(11,'hidden'),
	(12,'imageButton'),
	(13,'submitButton'),
	(14,'password'),
	(15,'ticket'),
	(41,'pageBreak');

/*!40000 ALTER TABLE `#__rsform_component_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_components
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_components`;

CREATE TABLE `#__rsform_components` (
  `ComponentId` int(11) NOT NULL AUTO_INCREMENT,
  `FormId` int(11) NOT NULL DEFAULT '0',
  `ComponentTypeId` int(11) NOT NULL DEFAULT '0',
  `Order` int(11) NOT NULL DEFAULT '0',
  `Published` tinyint(1) NOT NULL DEFAULT '1',
  UNIQUE KEY `ComponentId` (`ComponentId`),
  KEY `ComponentTypeId` (`ComponentTypeId`),
  KEY `FormId` (`FormId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_components` WRITE;
/*!40000 ALTER TABLE `#__rsform_components` DISABLE KEYS */;

INSERT INTO `#__rsform_components` (`ComponentId`, `FormId`, `ComponentTypeId`, `Order`, `Published`)
VALUES
	(1,1,1,2,1),
	(2,1,10,1,1),
	(3,1,1,3,1),
	(4,1,3,4,1),
	(5,1,5,5,1),
	(6,1,4,6,1),
	(7,1,6,7,1),
	(8,1,13,8,1),
	(9,1,10,9,1),
	(10,2,1,2,1),
	(11,2,10,1,1),
	(12,2,1,3,1),
	(13,2,3,6,1),
	(14,2,5,7,1),
	(15,2,4,10,1),
	(16,2,6,11,1),
	(17,2,13,12,1),
	(18,2,10,13,1),
	(19,2,41,4,1),
	(20,2,41,8,1),
	(21,2,10,5,1),
	(22,2,10,9,1),
	(23,3,1,0,1),
	(24,3,1,1,1),
	(25,3,1,2,1),
	(26,3,2,3,1),
	(27,3,13,4,1);

/*!40000 ALTER TABLE `#__rsform_components` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_condition_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_condition_details`;

CREATE TABLE `#__rsform_condition_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `operator` varchar(16) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `condition_id` (`condition_id`),
  KEY `component_id` (`component_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_conditions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_conditions`;

CREATE TABLE `#__rsform_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `action` varchar(16) NOT NULL,
  `block` tinyint(1) NOT NULL,
  `component_id` int(11) NOT NULL,
  `condition` varchar(16) NOT NULL,
  `lang_code` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `component_id` (`component_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_config`;

CREATE TABLE `#__rsform_config` (
  `ConfigId` int(11) DEFAULT NULL,
  `SettingName` varchar(64) NOT NULL DEFAULT '',
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_config` WRITE;
/*!40000 ALTER TABLE `#__rsform_config` DISABLE KEYS */;

INSERT INTO `#__rsform_config` (`ConfigId`, `SettingName`, `SettingValue`)
VALUES
	(1,'global.register.code',''),
	(2,'global.debug.mode','0'),
	(3,'global.iis','0'),
	(4,'global.editor','1'),
	(100,'global.codemirror','0'),
	(111,'auto_responsive','1');

/*!40000 ALTER TABLE `#__rsform_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_emails`;

CREATE TABLE `#__rsform_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formId` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `fromname` varchar(255) NOT NULL,
  `replyto` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `cc` varchar(255) NOT NULL,
  `bcc` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `mode` tinyint(1) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_forms`;

CREATE TABLE `#__rsform_forms` (
  `FormId` int(11) NOT NULL AUTO_INCREMENT,
  `FormName` text NOT NULL,
  `FormLayout` longtext NOT NULL,
  `FormLayoutName` text NOT NULL,
  `FormLayoutAutogenerate` tinyint(1) NOT NULL DEFAULT '1',
  `CSS` text NOT NULL,
  `JS` text NOT NULL,
  `FormTitle` text NOT NULL,
  `Published` tinyint(1) NOT NULL DEFAULT '1',
  `Lang` varchar(255) NOT NULL DEFAULT '',
  `ReturnUrl` text NOT NULL,
  `ShowThankyou` tinyint(1) NOT NULL DEFAULT '1',
  `Thankyou` text NOT NULL,
  `ShowContinue` tinyint(1) NOT NULL DEFAULT '1',
  `UserEmailText` text NOT NULL,
  `UserEmailTo` text NOT NULL,
  `UserEmailCC` varchar(255) NOT NULL,
  `UserEmailBCC` varchar(255) NOT NULL,
  `UserEmailFrom` varchar(255) NOT NULL DEFAULT '',
  `UserEmailReplyTo` varchar(255) NOT NULL,
  `UserEmailFromName` varchar(255) NOT NULL DEFAULT '',
  `UserEmailSubject` varchar(255) NOT NULL DEFAULT '',
  `UserEmailMode` tinyint(4) NOT NULL DEFAULT '1',
  `UserEmailAttach` tinyint(4) NOT NULL,
  `UserEmailAttachFile` varchar(255) NOT NULL,
  `AdminEmailText` text NOT NULL,
  `AdminEmailTo` text NOT NULL,
  `AdminEmailCC` varchar(255) NOT NULL,
  `AdminEmailBCC` varchar(255) NOT NULL,
  `AdminEmailFrom` varchar(255) NOT NULL DEFAULT '',
  `AdminEmailReplyTo` varchar(255) NOT NULL,
  `AdminEmailFromName` varchar(255) NOT NULL DEFAULT '',
  `AdminEmailSubject` varchar(255) NOT NULL DEFAULT '',
  `AdminEmailMode` tinyint(4) NOT NULL DEFAULT '1',
  `ScriptProcess` text NOT NULL,
  `ScriptProcess2` text NOT NULL,
  `ScriptDisplay` text NOT NULL,
  `UserEmailScript` text NOT NULL,
  `AdminEmailScript` text NOT NULL,
  `AdditionalEmailsScript` text NOT NULL,
  `MetaTitle` tinyint(1) NOT NULL,
  `MetaDesc` text NOT NULL,
  `MetaKeywords` text NOT NULL,
  `Required` varchar(255) NOT NULL DEFAULT '(*)',
  `ErrorMessage` text NOT NULL,
  `MultipleSeparator` varchar(64) NOT NULL DEFAULT '\\n',
  `TextareaNewLines` tinyint(1) NOT NULL DEFAULT '1',
  `CSSClass` varchar(255) NOT NULL,
  `CSSId` varchar(255) NOT NULL DEFAULT 'userForm',
  `CSSName` varchar(255) NOT NULL,
  `CSSAction` text NOT NULL,
  `CSSAdditionalAttributes` text NOT NULL,
  `AjaxValidation` tinyint(1) NOT NULL,
  `ThemeParams` text NOT NULL,
  `Keepdata` tinyint(1) NOT NULL DEFAULT '1',
  `Backendmenu` tinyint(1) NOT NULL,
  `ConfirmSubmission` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FormId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_forms` WRITE;
/*!40000 ALTER TABLE `#__rsform_forms` DISABLE KEYS */;

INSERT INTO `#__rsform_forms` (`FormId`, `FormName`, `FormLayout`, `FormLayoutName`, `FormLayoutAutogenerate`, `CSS`, `JS`, `FormTitle`, `Published`, `Lang`, `ReturnUrl`, `ShowThankyou`, `Thankyou`, `ShowContinue`, `UserEmailText`, `UserEmailTo`, `UserEmailCC`, `UserEmailBCC`, `UserEmailFrom`, `UserEmailReplyTo`, `UserEmailFromName`, `UserEmailSubject`, `UserEmailMode`, `UserEmailAttach`, `UserEmailAttachFile`, `AdminEmailText`, `AdminEmailTo`, `AdminEmailCC`, `AdminEmailBCC`, `AdminEmailFrom`, `AdminEmailReplyTo`, `AdminEmailFromName`, `AdminEmailSubject`, `AdminEmailMode`, `ScriptProcess`, `ScriptProcess2`, `ScriptDisplay`, `UserEmailScript`, `AdminEmailScript`, `AdditionalEmailsScript`, `MetaTitle`, `MetaDesc`, `MetaKeywords`, `Required`, `ErrorMessage`, `MultipleSeparator`, `TextareaNewLines`, `CSSClass`, `CSSId`, `CSSName`, `CSSAction`, `CSSAdditionalAttributes`, `AjaxValidation`, `ThemeParams`, `Keepdata`, `Backendmenu`, `ConfirmSubmission`)
VALUES
	(1,'RSformPro example','<h2>{global:formtitle}</h2>\r\n{error}\r\n<!-- Do not remove this ID, it is used to identify the page so that the pagination script can work correctly -->\r\n<fieldset class=\"formHorizontal formContainer\" id=\"rsform_1_page_0\">\r\n	<div class=\"rsform-block rsform-block-header\">\r\n		<div class=\"formControlLabel\">{Header:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Header:body}<span class=\"formValidation\">{Header:validation}</span></div>\r\n		<p class=\"formDescription\">{Header:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-fullname\">\r\n		<div class=\"formControlLabel\">{FullName:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{FullName:body}<span class=\"formValidation\">{FullName:validation}</span></div>\r\n		<p class=\"formDescription\">{FullName:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-email\">\r\n		<div class=\"formControlLabel\">{Email:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Email:body}<span class=\"formValidation\">{Email:validation}</span></div>\r\n		<p class=\"formDescription\">{Email:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-companysize\">\r\n		<div class=\"formControlLabel\">{CompanySize:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{CompanySize:body}<span class=\"formValidation\">{CompanySize:validation}</span></div>\r\n		<p class=\"formDescription\">{CompanySize:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-position\">\r\n		<div class=\"formControlLabel\">{Position:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Position:body}<span class=\"formValidation\">{Position:validation}</span></div>\r\n		<p class=\"formDescription\">{Position:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-contactby\">\r\n		<div class=\"formControlLabel\">{ContactBy:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{ContactBy:body}<span class=\"formValidation\">{ContactBy:validation}</span></div>\r\n		<p class=\"formDescription\">{ContactBy:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-contactwhen\">\r\n		<div class=\"formControlLabel\">{ContactWhen:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{ContactWhen:body}<span class=\"formValidation\">{ContactWhen:validation}</span></div>\r\n		<p class=\"formDescription\">{ContactWhen:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-submit\">\r\n		<div class=\"formControlLabel\">{Submit:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Submit:body}<span class=\"formValidation\">{Submit:validation}</span></div>\r\n		<p class=\"formDescription\">{Submit:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-footer\">\r\n		<div class=\"formControlLabel\">{Footer:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Footer:body}<span class=\"formValidation\">{Footer:validation}</span></div>\r\n		<p class=\"formDescription\">{Footer:description}</p>\r\n		</div>\r\n	</div>\r\n</fieldset>\r\n','responsive',1,'','','RSForm! Pro example',1,'','',1,'<p>Dear {FullName:value},</p><p> thank you for your submission. One of our staff members will contact you by  {ContactBy:value} as soon as possible.</p>',1,'<p>Dear {FullName:value},</p><p> we received your contact request. Someone will get back to you by {ContactBy:value} soon. </p>','{Email:value}','','','your@email.com','','Your Company','Contact confirmation',1,0,'','<p>Customize this e-mail also. You will receive it as administrator. </p><p>{FullName:caption}:{FullName:value}<br />\n{Email:caption}:{Email:value}<br />\n{CompanySize:caption}:{CompanySize:value}<br />\n{Position:caption}:{Position:value}<br />\n{ContactBy:caption}:{ContactBy:value}<br />\n{ContactWhen:caption}:{ContactWhen:value}</p>','youradminemail@email.com','','','{Email:value}','','Your Company','Contact',1,'','','','','','',0,'This is the meta description of your form. You can use it for SEO purposes.','rsform, contact, form, joomla','(*)','<p class=\"formRed\">Please complete all required fields!</p>',', ',1,'','userForm','','','',0,'',1,0,0),
	(2,'RSformPro Multipage example','<h2>{global:formtitle}</h2>\r\n{error}\r\n<!-- Do not remove this ID, it is used to identify the page so that the pagination script can work correctly -->\r\n<fieldset class=\"formHorizontal formContainer\" id=\"rsform_2_page_0\">\r\n	<div class=\"rsform-block rsform-block-header\">\r\n		<div class=\"formControlLabel\">{Header:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Header:body}<span class=\"formValidation\">{Header:validation}</span></div>\r\n		<p class=\"formDescription\">{Header:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-fullname\">\r\n		<div class=\"formControlLabel\">{FullName:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{FullName:body}<span class=\"formValidation\">{FullName:validation}</span></div>\r\n		<p class=\"formDescription\">{FullName:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-email\">\r\n		<div class=\"formControlLabel\">{Email:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Email:body}<span class=\"formValidation\">{Email:validation}</span></div>\r\n		<p class=\"formDescription\">{Email:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-page1\">\r\n		<div class=\"formControlLabel\">&nbsp;</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Page1:body}</div>\r\n		</div>\r\n	</div>\r\n	</fieldset>\r\n<!-- Do not remove this ID, it is used to identify the page so that the pagination script can work correctly -->\r\n<fieldset class=\"formHorizontal formContainer\" id=\"rsform_2_page_1\">\r\n	<div class=\"rsform-block rsform-block-companyheader\">\r\n		<div class=\"formControlLabel\">{CompanyHeader:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{CompanyHeader:body}<span class=\"formValidation\">{CompanyHeader:validation}</span></div>\r\n		<p class=\"formDescription\">{CompanyHeader:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-companysize\">\r\n		<div class=\"formControlLabel\">{CompanySize:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{CompanySize:body}<span class=\"formValidation\">{CompanySize:validation}</span></div>\r\n		<p class=\"formDescription\">{CompanySize:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-position\">\r\n		<div class=\"formControlLabel\">{Position:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Position:body}<span class=\"formValidation\">{Position:validation}</span></div>\r\n		<p class=\"formDescription\">{Position:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-page2\">\r\n		<div class=\"formControlLabel\">&nbsp;</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Page2:body}</div>\r\n		</div>\r\n	</div>\r\n	</fieldset>\r\n<!-- Do not remove this ID, it is used to identify the page so that the pagination script can work correctly -->\r\n<fieldset class=\"formHorizontal formContainer\" id=\"rsform_2_page_2\">\r\n	<div class=\"rsform-block rsform-block-contactheader\">\r\n		<div class=\"formControlLabel\">{ContactHeader:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{ContactHeader:body}<span class=\"formValidation\">{ContactHeader:validation}</span></div>\r\n		<p class=\"formDescription\">{ContactHeader:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-contactby\">\r\n		<div class=\"formControlLabel\">{ContactBy:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{ContactBy:body}<span class=\"formValidation\">{ContactBy:validation}</span></div>\r\n		<p class=\"formDescription\">{ContactBy:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-contactwhen\">\r\n		<div class=\"formControlLabel\">{ContactWhen:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{ContactWhen:body}<span class=\"formValidation\">{ContactWhen:validation}</span></div>\r\n		<p class=\"formDescription\">{ContactWhen:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-submit\">\r\n		<div class=\"formControlLabel\">{Submit:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Submit:body}<span class=\"formValidation\">{Submit:validation}</span></div>\r\n		<p class=\"formDescription\">{Submit:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-footer\">\r\n		<div class=\"formControlLabel\">{Footer:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Footer:body}<span class=\"formValidation\">{Footer:validation}</span></div>\r\n		<p class=\"formDescription\">{Footer:description}</p>\r\n		</div>\r\n	</div>\r\n</fieldset>\r\n','responsive',1,'','','RSForm! Pro Multipage example',1,'','',0,'<p>Dear {FullName:value},</p><p> thank you for your submission. One of our staff members will contact you by  {ContactBy:value} as soon as possible.</p>',1,'<p>Dear {FullName:value},</p><p> we received your contact request. Someone will get back to you by {ContactBy:value} soon. </p>','{Email:value}','','','your@email.com','','Your Company','Contact confirmation',1,0,'','<p>Customize this e-mail also. You will receive it as administrator. </p><p>{FullName:caption}:{FullName:value}<br />\n{Email:caption}:{Email:value}<br />\n{CompanySize:caption}:{CompanySize:value}<br />\n{Position:caption}:{Position:value}<br />\n{ContactBy:caption}:{ContactBy:value}<br />\n{ContactWhen:caption}:{ContactWhen:value}</p>','youradminemail@email.com','','','{Email:value}','','Your Company','Contact',1,'','','','','','',0,'This is the meta description of your form. You can use it for SEO purposes.','rsform, contact, form, joomla','(*)','<p class=\"formRed\">Please complete all required fields!</p>',', ',1,'','userForm','','','',0,'',1,0,0),
	(3,'contact-us','<h2>{global:formtitle}</h2>\r\n{error}\r\n<!-- Do not remove this ID, it is used to identify the page so that the pagination script can work correctly -->\r\n<fieldset class=\"formHorizontal formContainer\" id=\"rsform_3_page_0\">\r\n	<div class=\"rsform-block rsform-block-name\">\r\n		<div class=\"formControlLabel\">{Name:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Name:body}<span class=\"formValidation\">{Name:validation}</span></div>\r\n		<p class=\"formDescription\">{Name:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-email\">\r\n		<div class=\"formControlLabel\">{Email:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Email:body}<span class=\"formValidation\">{Email:validation}</span></div>\r\n		<p class=\"formDescription\">{Email:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-subject\">\r\n		<div class=\"formControlLabel\">{Subject:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Subject:body}<span class=\"formValidation\">{Subject:validation}</span></div>\r\n		<p class=\"formDescription\">{Subject:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-message\">\r\n		<div class=\"formControlLabel\">{Message:caption}<strong class=\"formRequired\">(*)</strong></div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Message:body}<span class=\"formValidation\">{Message:validation}</span></div>\r\n		<p class=\"formDescription\">{Message:description}</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"rsform-block rsform-block-send\">\r\n		<div class=\"formControlLabel\">{Send:caption}</div>\r\n		<div class=\"formControls\">\r\n		<div class=\"formBody\">{Send:body}<span class=\"formValidation\">{Send:validation}</span></div>\r\n		<p class=\"formDescription\">{Send:description}</p>\r\n		</div>\r\n	</div>\r\n</fieldset>\r\n','responsive',1,'','','Contact Us',1,'en-GB','',1,'<p>Thank you for your submission! We will contact you as soon as possible.</p>',1,'<p>Thank you for contacting us. We will get back to you as soon as possible.</p>\n<p>{Name:caption}: {Name:value}</p>\n<p>{Email:caption}: {Email:value}</p>\n<p>{Subject:caption}: {Subject:value}</p>\n<p>{Message:caption}: {Message:value}</p>\n<p>{Send:caption}: {Send:value}</p>','{Email:value}','','','info@organic-development.com','','Enter From Name','Thank you for your submission!',1,0,'','<p>You have a new submission.</p>\n<p>{Name:caption}: {Name:value}</p>\n<p>{Email:caption}: {Email:value}</p>\n<p>{Subject:caption}: {Subject:value}</p>\n<p>{Message:caption}: {Message:value}</p>\n<p>{Send:caption}: {Send:value}</p>','info@organic-development.com','','','info@organic-development.com','','Enter From Name','New submission from \'Contact Us\'!',1,'','','','','','',0,'','','(*)','<p class=\"formRed\">Please complete all required fields!</p>','\\n',1,'','userForm','','','',0,'',1,0,0);

/*!40000 ALTER TABLE `#__rsform_forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_mappings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_mappings`;

CREATE TABLE `#__rsform_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formId` int(11) NOT NULL,
  `connection` tinyint(1) NOT NULL,
  `host` varchar(255) NOT NULL,
  `port` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `database` varchar(255) NOT NULL,
  `method` tinyint(1) NOT NULL,
  `table` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `wheredata` text NOT NULL,
  `extra` text NOT NULL,
  `andor` text NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_posts`;

CREATE TABLE `#__rsform_posts` (
  `form_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `method` tinyint(1) NOT NULL,
  `silent` tinyint(1) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_posts` WRITE;
/*!40000 ALTER TABLE `#__rsform_posts` DISABLE KEYS */;

INSERT INTO `#__rsform_posts` (`form_id`, `enabled`, `method`, `silent`, `url`)
VALUES
	(3,0,1,1,'http://');

/*!40000 ALTER TABLE `#__rsform_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_properties
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_properties`;

CREATE TABLE `#__rsform_properties` (
  `PropertyId` int(11) NOT NULL AUTO_INCREMENT,
  `ComponentId` int(11) NOT NULL DEFAULT '0',
  `PropertyName` text NOT NULL,
  `PropertyValue` text NOT NULL,
  UNIQUE KEY `PropertyId` (`PropertyId`),
  KEY `ComponentId` (`ComponentId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__rsform_properties` WRITE;
/*!40000 ALTER TABLE `#__rsform_properties` DISABLE KEYS */;

INSERT INTO `#__rsform_properties` (`PropertyId`, `ComponentId`, `PropertyName`, `PropertyValue`)
VALUES
	(1,1,'NAME','FullName'),
	(2,1,'CAPTION','Full Name'),
	(3,1,'REQUIRED','YES'),
	(4,1,'SIZE','20'),
	(5,1,'MAXSIZE',''),
	(6,1,'VALIDATIONRULE','none'),
	(7,1,'VALIDATIONMESSAGE','Please type your full name.'),
	(8,1,'ADDITIONALATTRIBUTES',''),
	(9,1,'DEFAULTVALUE',''),
	(10,1,'DESCRIPTION',''),
	(11,2,'NAME','Header'),
	(12,2,'TEXT','<b>This text describes the form. It is added using the Free Text component</b>. HTML code can be added directly here.'),
	(13,3,'NAME','Email'),
	(14,3,'CAPTION','E-mail'),
	(15,3,'REQUIRED','YES'),
	(16,3,'SIZE','20'),
	(17,3,'MAXSIZE',''),
	(18,3,'VALIDATIONRULE','email'),
	(19,3,'VALIDATIONMESSAGE','Invalid email address.'),
	(20,3,'ADDITIONALATTRIBUTES',''),
	(21,3,'DEFAULTVALUE',''),
	(22,3,'DESCRIPTION',''),
	(23,4,'NAME','CompanySize'),
	(24,4,'CAPTION','Number of Employees'),
	(25,4,'SIZE',''),
	(26,4,'MULTIPLE','NO'),
	(27,4,'ITEMS','|Please Select[c]\n1-20\n21-50\n51-100\n>100|More than 100'),
	(28,4,'REQUIRED','YES'),
	(29,4,'ADDITIONALATTRIBUTES',''),
	(30,4,'DESCRIPTION',''),
	(31,4,'VALIDATIONMESSAGE','Please tell us how big is your company.'),
	(32,5,'NAME','Position'),
	(33,5,'CAPTION','Position'),
	(34,5,'ITEMS','CEO\nCFO\nCTO\nHR[c]'),
	(35,5,'FLOW','HORIZONTAL'),
	(36,5,'REQUIRED','YES'),
	(37,5,'ADDITIONALATTRIBUTES',''),
	(38,5,'DESCRIPTION',''),
	(39,5,'VALIDATIONMESSAGE','Please specify your position in the company'),
	(40,6,'NAME','ContactBy'),
	(41,6,'CAPTION','How should we contact you?'),
	(42,6,'ITEMS','E-mail[c]\nPhone\nNewsletter[c]\nMail'),
	(43,6,'FLOW','HORIZONTAL'),
	(44,6,'REQUIRED','NO'),
	(45,6,'ADDITIONALATTRIBUTES',''),
	(46,6,'DESCRIPTION',''),
	(47,6,'VALIDATIONMESSAGE',''),
	(48,7,'NAME','ContactWhen'),
	(49,7,'CAPTION','When would you like to be contacted?'),
	(50,7,'REQUIRED','YES'),
	(51,7,'DATEFORMAT','dd.mm.yyyy'),
	(52,7,'CALENDARLAYOUT','POPUP'),
	(53,7,'ADDITIONALATTRIBUTES',''),
	(54,7,'READONLY','YES'),
	(55,7,'POPUPLABEL','...'),
	(56,7,'DESCRIPTION',''),
	(57,7,'VALIDATIONMESSAGE','Please select a date when we should contact you.'),
	(58,8,'NAME','Submit'),
	(59,8,'LABEL','Submit'),
	(60,8,'CAPTION',''),
	(61,8,'RESET','YES'),
	(62,8,'RESETLABEL','Reset'),
	(63,8,'ADDITIONALATTRIBUTES',''),
	(64,9,'NAME','Footer'),
	(65,9,'TEXT','This form is an example. Please check our knowledgebase for articles related to how you should build your form. Articles are updated daily. <a href=\"http://www.rsjoomla.com/\" target=\"_blank\">http://www.rsjoomla.com/</a>'),
	(68,10,'NAME','FullName'),
	(69,10,'CAPTION','Full Name'),
	(70,10,'REQUIRED','YES'),
	(71,10,'SIZE','20'),
	(72,10,'MAXSIZE',''),
	(73,10,'VALIDATIONRULE','none'),
	(74,10,'VALIDATIONMESSAGE','Please type your full name.'),
	(75,10,'ADDITIONALATTRIBUTES',''),
	(76,10,'DEFAULTVALUE',''),
	(77,10,'DESCRIPTION',''),
	(78,10,'VALIDATIONEXTRA',''),
	(79,11,'NAME','Header'),
	(80,11,'TEXT','<b>This text describes the form. It is added using the Free Text component</b>. HTML code can be added directly here.'),
	(81,12,'NAME','Email'),
	(82,12,'CAPTION','E-mail'),
	(83,12,'REQUIRED','YES'),
	(84,12,'SIZE','20'),
	(85,12,'MAXSIZE',''),
	(86,12,'VALIDATIONRULE','email'),
	(87,12,'VALIDATIONMESSAGE','Invalid email address.'),
	(88,12,'ADDITIONALATTRIBUTES',''),
	(89,12,'DEFAULTVALUE',''),
	(90,12,'DESCRIPTION',''),
	(91,12,'VALIDATIONEXTRA',''),
	(92,13,'NAME','CompanySize'),
	(93,13,'CAPTION','Number of Employees'),
	(94,13,'SIZE',''),
	(95,13,'MULTIPLE','NO'),
	(96,13,'ITEMS','|Please Select[c]\n1-20\n21-50\n51-100\n>100|More than 100'),
	(97,13,'REQUIRED','YES'),
	(98,13,'ADDITIONALATTRIBUTES',''),
	(99,13,'DESCRIPTION',''),
	(100,13,'VALIDATIONMESSAGE','Please tell us how big is your company.'),
	(101,14,'NAME','Position'),
	(102,14,'CAPTION','Position'),
	(103,14,'ITEMS','CEO\nCFO\nCTO\nHR[c]'),
	(104,14,'FLOW','HORIZONTAL'),
	(105,14,'REQUIRED','YES'),
	(106,14,'ADDITIONALATTRIBUTES',''),
	(107,14,'DESCRIPTION',''),
	(108,14,'VALIDATIONMESSAGE','Please specify your position in the company'),
	(109,15,'NAME','ContactBy'),
	(110,15,'CAPTION','How should we contact you?'),
	(111,15,'ITEMS','E-mail[c]\nPhone\nNewsletter[c]\nMail'),
	(112,15,'FLOW','HORIZONTAL'),
	(113,15,'REQUIRED','NO'),
	(114,15,'ADDITIONALATTRIBUTES',''),
	(115,15,'DESCRIPTION',''),
	(116,15,'VALIDATIONMESSAGE',''),
	(117,16,'NAME','ContactWhen'),
	(118,16,'CAPTION','When would you like to be contacted?'),
	(119,16,'REQUIRED','YES'),
	(120,16,'DATEFORMAT','dd.mm.yyyy'),
	(121,16,'CALENDARLAYOUT','POPUP'),
	(122,16,'ADDITIONALATTRIBUTES',''),
	(123,16,'READONLY','YES'),
	(124,16,'POPUPLABEL','...'),
	(125,16,'DESCRIPTION',''),
	(126,16,'VALIDATIONMESSAGE','Please select a date when we should contact you.'),
	(127,17,'NAME','Submit'),
	(128,17,'LABEL','Submit'),
	(129,17,'CAPTION',''),
	(130,17,'RESET','YES'),
	(131,17,'RESETLABEL','Reset'),
	(132,17,'ADDITIONALATTRIBUTES',''),
	(133,18,'NAME','Footer'),
	(134,18,'TEXT','This form is an example. Please check our knowledgebase for articles related to how you should build your form. Articles are updated daily. <a href=\"http://www.rsjoomla.com/\" target=\"_blank\">http://www.rsjoomla.com/</a>'),
	(135,19,'NAME','Page1'),
	(136,19,'NEXTBUTTON','Next >'),
	(137,19,'PREVBUTTON','Prev'),
	(138,19,'ADDITIONALATTRIBUTES',''),
	(139,20,'NAME','Page2'),
	(140,20,'NEXTBUTTON','Next >'),
	(141,20,'PREVBUTTON','Prev'),
	(142,20,'ADDITIONALATTRIBUTES',''),
	(143,21,'NAME','CompanyHeader'),
	(144,21,'TEXT','Please tell us a little about your company.'),
	(145,22,'NAME','ContactHeader'),
	(146,22,'TEXT','Please let us know how and when to contact you.'),
	(147,1,'VALIDATIONEXTRA',''),
	(148,3,'VALIDATIONEXTRA',''),
	(149,10,'VALIDATIONEXTRA',''),
	(150,12,'VALIDATIONEXTRA',''),
	(151,8,'DISPLAYPROGRESS','NO'),
	(152,8,'DISPLAYPROGRESSMSG','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>'),
	(153,17,'DISPLAYPROGRESS','NO'),
	(154,17,'DISPLAYPROGRESSMSG','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>'),
	(155,19,'DISPLAYPROGRESS','NO'),
	(156,19,'DISPLAYPROGRESSMSG','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>'),
	(157,20,'DISPLAYPROGRESS','NO'),
	(158,20,'DISPLAYPROGRESSMSG','<div>\r\n <p><em>Page <strong>{page}</strong> of {total}</em></p>\r\n <div class=\"rsformProgressContainer\">\r\n  <div class=\"rsformProgressBar\" style=\"width: {percent}%;\"></div>\r\n </div>\r\n</div>'),
	(159,23,'NAME','Name'),
	(160,23,'CAPTION','Your Name'),
	(161,23,'REQUIRED','YES'),
	(162,23,'SIZE','20'),
	(163,23,'MAXSIZE',''),
	(164,23,'VALIDATIONRULE','none'),
	(165,23,'VALIDATIONEXTRA',''),
	(166,23,'VALIDATIONMESSAGE','Please let us know your name.'),
	(167,23,'ADDITIONALATTRIBUTES',''),
	(168,23,'DEFAULTVALUE',''),
	(169,23,'DESCRIPTION',''),
	(170,24,'NAME','Email'),
	(171,24,'CAPTION','Your Email'),
	(172,24,'REQUIRED','YES'),
	(173,24,'SIZE','20'),
	(174,24,'MAXSIZE',''),
	(175,24,'VALIDATIONRULE','email'),
	(176,24,'VALIDATIONEXTRA',''),
	(177,24,'VALIDATIONMESSAGE','Please let us know your email address.'),
	(178,24,'ADDITIONALATTRIBUTES',''),
	(179,24,'DEFAULTVALUE',''),
	(180,24,'DESCRIPTION',''),
	(181,25,'NAME','Subject'),
	(182,25,'CAPTION','Subject'),
	(183,25,'REQUIRED','YES'),
	(184,25,'SIZE','20'),
	(185,25,'MAXSIZE',''),
	(186,25,'VALIDATIONRULE','none'),
	(187,25,'VALIDATIONEXTRA',''),
	(188,25,'VALIDATIONMESSAGE','Please write a subject for your message.'),
	(189,25,'ADDITIONALATTRIBUTES',''),
	(190,25,'DEFAULTVALUE',''),
	(191,25,'DESCRIPTION',''),
	(192,26,'NAME','Message'),
	(193,26,'CAPTION','Message'),
	(194,26,'REQUIRED','YES'),
	(195,26,'COLS','50'),
	(196,26,'ROWS','5'),
	(197,26,'VALIDATIONRULE','none'),
	(198,26,'VALIDATIONEXTRA',''),
	(199,26,'VALIDATIONMESSAGE','Please let us know your message.'),
	(200,26,'ADDITIONALATTRIBUTES',''),
	(201,26,'DEFAULTVALUE',''),
	(202,26,'DESCRIPTION',''),
	(203,26,'WYSIWYG','NO'),
	(204,27,'NAME','Send'),
	(205,27,'LABEL','Send'),
	(206,27,'CAPTION',''),
	(207,27,'RESET','NO'),
	(208,27,'RESETLABEL',''),
	(209,27,'ADDITIONALATTRIBUTES','');

/*!40000 ALTER TABLE `#__rsform_properties` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__rsform_submission_columns
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_submission_columns`;

CREATE TABLE `#__rsform_submission_columns` (
  `FormId` int(11) NOT NULL,
  `ColumnName` varchar(255) NOT NULL,
  `ColumnStatic` tinyint(1) NOT NULL,
  PRIMARY KEY (`FormId`,`ColumnName`,`ColumnStatic`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_submission_values
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_submission_values`;

CREATE TABLE `#__rsform_submission_values` (
  `SubmissionValueId` int(11) NOT NULL AUTO_INCREMENT,
  `FormId` int(11) NOT NULL,
  `SubmissionId` int(11) NOT NULL DEFAULT '0',
  `FieldName` text NOT NULL,
  `FieldValue` text NOT NULL,
  PRIMARY KEY (`SubmissionValueId`),
  KEY `FormId` (`FormId`),
  KEY `SubmissionId` (`SubmissionId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_submissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_submissions`;

CREATE TABLE `#__rsform_submissions` (
  `SubmissionId` int(11) NOT NULL AUTO_INCREMENT,
  `FormId` int(11) NOT NULL DEFAULT '0',
  `DateSubmitted` datetime NOT NULL,
  `UserIp` varchar(15) NOT NULL DEFAULT '',
  `Username` varchar(255) NOT NULL DEFAULT '',
  `UserId` text NOT NULL,
  `Lang` varchar(255) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`SubmissionId`),
  KEY `FormId` (`FormId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__rsform_translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__rsform_translations`;

CREATE TABLE `#__rsform_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `lang_code` varchar(32) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `reference_id` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__schemas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__schemas`;

CREATE TABLE `#__schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__schemas` WRITE;
/*!40000 ALTER TABLE `#__schemas` DISABLE KEYS */;

INSERT INTO `#__schemas` (`extension_id`, `version_id`)
VALUES
	(10023,'3.0.2'),
	(10024,'3.0.2'),
	(10025,'3.0.4'),
	(10026,'3.0.1'),
	(10027,'3.0.2'),
	(10028,'3.0.3'),
	(10029,'3.0.2'),
	(10030,'3.0.2'),
	(10031,'3.0.3'),
	(10032,'4.2.5'),
	(10065,'1.1.0');

/*!40000 ALTER TABLE `#__schemas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__sef_statistics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sef_statistics`;

CREATE TABLE `#__sef_statistics` (
  `url_id` int(5) NOT NULL,
  `page_rank` int(3) NOT NULL,
  `total_indexed` int(10) NOT NULL,
  `popularity` int(10) NOT NULL,
  `facebook_indexed` int(10) NOT NULL,
  `twitter_indexed` int(10) NOT NULL,
  `validation_score` varchar(255) NOT NULL,
  `page_speed_score` mediumtext NOT NULL,
  PRIMARY KEY (`url_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sef_subdomains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sef_subdomains`;

CREATE TABLE `#__sef_subdomains` (
  `subdomain` varchar(255) NOT NULL DEFAULT '',
  `Itemid` mediumtext NOT NULL,
  `Itemid_titlepage` int(10) NOT NULL,
  `option` varchar(255) NOT NULL,
  `lang` varchar(10) NOT NULL,
  PRIMARY KEY (`subdomain`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefaliases
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefaliases`;

CREATE TABLE `#__sefaliases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `vars` text NOT NULL,
  `url` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefexts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefexts`;

CREATE TABLE `#__sefexts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `filters` text,
  `params` text,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefexttexts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefexttexts`;

CREATE TABLE `#__sefexttexts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `lang_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__seflog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__seflog`;

CREATE TABLE `#__seflog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `component` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefmoved
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefmoved`;

CREATE TABLE `#__sefmoved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old` varchar(255) NOT NULL,
  `new` varchar(255) NOT NULL,
  `lastHit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `old` (`old`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefurls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefurls`;

CREATE TABLE `#__sefurls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cpt` int(11) NOT NULL DEFAULT '0',
  `sefurl` varchar(255) NOT NULL,
  `origurl` varchar(255) NOT NULL,
  `Itemid` varchar(20) DEFAULT NULL,
  `metadesc` varchar(255) DEFAULT '',
  `metakey` varchar(255) DEFAULT '',
  `metatitle` varchar(255) DEFAULT '',
  `metalang` varchar(30) DEFAULT '',
  `metarobots` varchar(30) DEFAULT '',
  `metagoogle` varchar(30) DEFAULT '',
  `metaauthor` varchar(30) DEFAULT '',
  `canonicallink` varchar(255) DEFAULT '',
  `dateadd` date NOT NULL DEFAULT '0000-00-00',
  `priority` int(11) NOT NULL DEFAULT '0',
  `trace` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `sef` tinyint(1) NOT NULL DEFAULT '1',
  `sm_indexed` tinyint(1) NOT NULL DEFAULT '0',
  `sm_date` date NOT NULL DEFAULT '0000-00-00',
  `sm_frequency` varchar(20) NOT NULL DEFAULT 'weekly',
  `sm_priority` varchar(10) NOT NULL DEFAULT '0.5',
  `flag` tinyint(1) NOT NULL DEFAULT '0',
  `host` varchar(255) NOT NULL DEFAULT '',
  `showsitename` int(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  KEY `sefurl` (`sefurl`),
  KEY `origurl` (`origurl`,`Itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefurlword_xref
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefurlword_xref`;

CREATE TABLE `#__sefurlword_xref` (
  `word` int(11) NOT NULL,
  `url` int(11) NOT NULL,
  PRIMARY KEY (`word`,`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__sefwords
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__sefwords`;

CREATE TABLE `#__sefwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__session`;

CREATE TABLE `#__session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  `usertype` varchar(50) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__template_styles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__template_styles`;

CREATE TABLE `#__template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__template_styles` WRITE;
/*!40000 ALTER TABLE `#__template_styles` DISABLE KEYS */;

INSERT INTO `#__template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`)
VALUES
	(2,'bluestork',1,'0','Bluestork - Default','{\"useRoundedCorners\":\"1\",\"showSiteName\":\"0\"}'),
	(13,'organic',1,'0','Organic - Default','{\"useRoundedCorners\":\"1\",\"showSiteName\":\"1\"}'),
	(10,'yoo_master',0,'1','yoo_master - Default','{\"config\":\"\"}'),
	(14,'bootstrap_admin',1,'1','Bootstrap Admin - Default','{}');

/*!40000 ALTER TABLE `#__template_styles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__update_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__update_categories`;

CREATE TABLE `#__update_categories` (
  `categoryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT '',
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `updatesite` int(11) DEFAULT '0',
  PRIMARY KEY (`categoryid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Update Categories';



# Dump of table #__update_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__update_sites`;

CREATE TABLE `#__update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  PRIMARY KEY (`update_site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Update Sites';

LOCK TABLES `#__update_sites` WRITE;
/*!40000 ALTER TABLE `#__update_sites` DISABLE KEYS */;

INSERT INTO `#__update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`)
VALUES
	(1,'Joomla Core','collection','http://update.joomla.org/core/list.xml',1,1345626981),
	(2,'Joomla Extension Directory','collection','http://update.joomla.org/jed/list.xml',1,1345626981),
	(21,'JCE Editor Updates','extension','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1\n        ',1,0),
	(4,'ext_joomsef4_banners','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_banners.xml',1,1345626981),
	(5,'ext_joomsef4_contact','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_contact.xml',1,1345626981),
	(6,'ext_joomsef4_content','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_content.xml',1,1345626981),
	(7,'ext_joomsef4_mailto','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_mailto.xml',1,1345626981),
	(8,'ext_joomsef4_newfeeds','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_newsfeeds.xml',1,1345626981),
	(9,'ext_joomsef4_search','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_search.xml',1,1345626981),
	(10,'ext_joomsef4_users','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_users.xml',1,1345626981),
	(11,'ext_joomsef4_weblinks','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_weblinks.xml',1,1345626981),
	(12,'ext_joomsef4_wrapper','sef_update','http://www.artio.net/joomla-updates/list/ext_joomsef4_wrapper.xml',1,1345626981),
	(13,'com_joomsef','sef_update','http://www.artio.net/joomla-updates/list/com_joomsef4.xml',1,1345626981),
	(14,'JCE Editor Updates','extension','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1',1,1345626981),
	(15,'Akeeba Backup Core Updates','extension','http://nocdn.akeebabackup.com/updates/abcore.xml',1,1345626981),
	(20,'SmartIcons Update Site','extension','http://extensions.naicum.ro/update/smarticons-1-update.xml',0,1335522250);

/*!40000 ALTER TABLE `#__update_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__update_sites_extensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__update_sites_extensions`;

CREATE TABLE `#__update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

LOCK TABLES `#__update_sites_extensions` WRITE;
/*!40000 ALTER TABLE `#__update_sites_extensions` DISABLE KEYS */;

INSERT INTO `#__update_sites_extensions` (`update_site_id`, `extension_id`)
VALUES
	(1,700),
	(2,700),
	(4,10023),
	(5,10024),
	(6,10025),
	(7,10026),
	(8,10027),
	(9,10028),
	(10,10029),
	(11,10030),
	(12,10031),
	(13,10032),
	(14,10038),
	(15,10057),
	(20,10065),
	(21,10038);

/*!40000 ALTER TABLE `#__update_sites_extensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__updates`;

CREATE TABLE `#__updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `categoryid` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(10) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  PRIMARY KEY (`update_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Available Updates';

LOCK TABLES `#__updates` WRITE;
/*!40000 ALTER TABLE `#__updates` DISABLE KEYS */;

INSERT INTO `#__updates` (`update_id`, `update_site_id`, `extension_id`, `categoryid`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`)
VALUES
	(1,13,10032,0,'ARTIO JoomSEF','','com_sef','component','',1,'4.2.5','','http://www.artio.net/joomla-updates/list/com_joomsef4.xml',''),
	(2,15,0,0,'akeebacore','Akeeba Backup Core','com_akeeba','component','',0,'3.4.3','','http://nocdn.akeebabackup.com/updates/abcore.xml','https://www.akeebabackup.com/download/akeeba-backup/akeeba-backup-3-4-3.html'),
	(3,17,0,0,'minima','','minima','template','',1,'0.8','','http://minimatemplate.com/update/templates/minima/extension.xml',''),
	(4,17,0,0,'mypanel','','mypanel','module','',1,'0.8','','http://minimatemplate.com/update/modules/mypanel/extension.xml',''),
	(5,17,0,0,'myshortcuts','','myshortcuts','module','',1,'0.8','','http://minimatemplate.com/update/modules/myshortcuts/extension.xml',''),
	(7,13,0,0,'ARTIO JoomSEF','','com_sef','component','',1,'4.2.8','','http://www.artio.net/joomla-updates/list/com_joomsef4.xml',''),
	(8,13,0,0,'ARTIO JoomSEF','','com_sef','component','',1,'4.2.8','','http://www.artio.net/joomla-updates/list/com_joomsef4.xml',''),
	(9,14,10038,0,'JCE Editor','','com_jce','component','',1,'2.2.6','','https://www.joomlacontenteditor.net/index.php?option=com_updates&view=update&format=xml&id=1/extension.xml','http://www.joomlacontenteditor.net/news/item/jce-226-released?category_id=32'),
	(10,15,10057,0,'akeebacore','Akeeba Backup Core','com_akeeba','component','',1,'3.6.2','','http://nocdn.akeebabackup.com/updates/abcore.xml','https://www.akeebabackup.com/downloads/akeeba-backup/akeeba-backup-3-6-2.html');

/*!40000 ALTER TABLE `#__updates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__user_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__user_notes`;

CREATE TABLE `#__user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__user_profiles`;

CREATE TABLE `#__user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';



# Dump of table #__user_usergroup_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__user_usergroup_map`;

CREATE TABLE `#__user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__user_usergroup_map` WRITE;
/*!40000 ALTER TABLE `#__user_usergroup_map` DISABLE KEYS */;

INSERT INTO `#__user_usergroup_map` (`user_id`, `group_id`)
VALUES
	(42,8),
	(43,1),
	(43,2),
	(43,3),
	(43,6);

/*!40000 ALTER TABLE `#__user_usergroup_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__usergroups`;

CREATE TABLE `#__usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__usergroups` WRITE;
/*!40000 ALTER TABLE `#__usergroups` DISABLE KEYS */;

INSERT INTO `#__usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`)
VALUES
	(1,0,1,20,'Public'),
	(2,1,6,17,'Registered'),
	(3,2,7,14,'Author'),
	(4,3,8,11,'Editor'),
	(5,4,9,10,'Publisher'),
	(6,1,2,5,'Manager'),
	(7,6,3,4,'Administrator'),
	(8,1,18,19,'Super Users');

/*!40000 ALTER TABLE `#__usergroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__users`;

CREATE TABLE `#__users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__users` WRITE;
/*!40000 ALTER TABLE `#__users` DISABLE KEYS */;

INSERT INTO `#__users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`)
VALUES
	(42,'Super User','organic','info@organic-development.com','0f838b12299f8e9946b68f928e2094d4','deprecated',0,1,'2012-01-26 09:55:22','2012-08-22 11:15:07','0','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0),
	(43,'Client','client','client@organic-development.com','df18aba1a15060a552576a806c102021:NcPL9ISoILIQVg2EsN1J53k8t6m2yebQ','',0,0,'2012-01-26 13:19:26','2012-01-26 13:28:42','','{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}','0000-00-00 00:00:00',0);

/*!40000 ALTER TABLE `#__users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__viewlevels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__viewlevels`;

CREATE TABLE `#__viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__viewlevels` WRITE;
/*!40000 ALTER TABLE `#__viewlevels` DISABLE KEYS */;

INSERT INTO `#__viewlevels` (`id`, `title`, `ordering`, `rules`)
VALUES
	(1,'Public',0,'[1]'),
	(2,'Registered',1,'[6,2,8]'),
	(3,'Special',2,'[6,3,8]');

/*!40000 ALTER TABLE `#__viewlevels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__weblinks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__weblinks`;

CREATE TABLE `#__weblinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__wf_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__wf_profiles`;

CREATE TABLE `#__wf_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `users` text NOT NULL,
  `types` varchar(255) NOT NULL,
  `components` text NOT NULL,
  `area` tinyint(3) NOT NULL,
  `rows` text NOT NULL,
  `plugins` text NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__wf_profiles` WRITE;
/*!40000 ALTER TABLE `#__wf_profiles` DISABLE KEYS */;

INSERT INTO `#__wf_profiles` (`id`, `name`, `description`, `users`, `types`, `components`, `area`, `rows`, `plugins`, `published`, `ordering`, `checked_out`, `checked_out_time`, `params`)
VALUES
	(1,'Default','Super Users','','8','',0,'help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,blockquote,formatselect,styleselect,removeformat,cleanup;fontselect,fontsizeselect,forecolor,backcolor,spacer,paste,indent,outdent,numlist,bullist,sub,sup,textcase,charmap,hr;directionality,fullscreen,preview,source,print,searchreplace,spacer,table;visualaid,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,unlink,link,spellchecker,article,filemanager,imgmanager_ext,mediamanager','cleanup,paste,textcase,directionality,fullscreen,preview,source,print,searchreplace,table,visualchars,visualblocks,nonbreaking,style,xhtmlxtras,anchor,link,spellchecker,article,filemanager,imgmanager_ext,mediamanager,browser,contextmenu,inlinepopups,media',1,1,0,'0000-00-00 00:00:00','{\"editor\":{\"width\":\"\",\"height\":\"\",\"toolbar_theme\":\"default\",\"toolbar_align\":\"left\",\"toolbar_location\":\"top\",\"statusbar_location\":\"bottom\",\"path\":\"1\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"resizing_use_cookie\":\"1\",\"toggle\":\"1\",\"toggle_label\":\"[show\\/hide]\",\"toggle_state\":\"1\",\"dialog_theme\":\"jce\",\"relative_urls\":\"1\",\"verify_html\":\"\",\"schema\":\"\",\"forced_root_block\":\"\",\"profile_content_css\":\"2\",\"profile_content_css_custom\":\"\",\"theme_advanced_styles\":\"\",\"theme_advanced_blockformats\":[\"p\",\"div\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\",\"address\",\"code\",\"pre\",\"samp\",\"span\",\"section\",\"article\",\"hgroup\",\"aside\",\"figure\",\"dt\",\"dd\"],\"theme_advanced_fonts_add\":\"\",\"theme_advanced_fonts_remove\":\"\",\"theme_advanced_font_sizes\":\"0.8em,1.0em,1.2em,1.4em\",\"custom_colors\":\"\",\"dir\":\"\",\"filesystem\":{\"name\":\"joomla\",\"joomla\":{\"allow_root\":\"0\",\"restrict_dir\":\"administrator,cache,components,includes,language,libraries,logs,media,modules,plugins,templates,xmlrpc\"}},\"max_size\":\"\",\"upload_conflict\":\"overwrite\",\"upload_runtimes\":[\"html5\",\"flash\"],\"browser_position\":\"bottom\",\"folder_tree\":\"1\",\"list_limit\":\"all\",\"validate_mimetype\":\"0\",\"websafe_mode\":\"utf-8\",\"upload_add_random\":\"0\",\"invalid_elements\":\"\",\"invalid_attributes\":\"dynsrc,lowsrc\",\"invalid_attribute_values\":\"\",\"extended_elements\":\"\",\"allow_javascript\":\"0\",\"allow_css\":\"0\",\"allow_php\":\"0\",\"cdata\":\"1\"},\"paste\":{\"use_dialog\":\"0\",\"dialog_width\":\"450\",\"dialog_height\":\"400\",\"force_cleanup\":\"1\",\"strip_class_attributes\":\"all\",\"remove_spans\":\"1\",\"remove_styles\":\"1\",\"remove_attributes\":\"\",\"retain_style_properties\":\"font-weight\",\"remove_empty_paragraphs\":\"1\",\"remove_styles_if_webkit\":\"1\",\"html\":\"1\",\"text\":\"1\"},\"source\":{\"highlight\":\"1\",\"numbers\":\"1\",\"wrap\":\"1\",\"format\":\"1\",\"tag_closing\":\"1\",\"selection_match\":\"1\",\"theme\":\"textmate\"},\"table\":{\"width\":\"\",\"height\":\"\",\"border\":\"0\",\"cols\":\"2\",\"rows\":\"2\",\"cellpadding\":\"\",\"cellspacing\":\"\"},\"visualblocks\":{\"state\":\"1\"},\"link\":{\"target\":\"\",\"file_browser\":\"1\",\"tabs_advanced\":\"1\",\"attributes_anchor\":\"1\",\"attributes_target\":\"1\",\"links\":{\"joomlalinks\":{\"enable\":\"1\",\"content\":\"1\",\"article_alias\":\"1\",\"static\":\"1\",\"contacts\":\"1\",\"weblinks\":\"1\",\"weblinks_alias\":\"1\",\"menu\":\"1\"}},\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"},\"window\":{\"enable\":\"1\"}},\"search\":{\"link\":{\"enable\":\"1\",\"plugins\":[\"categories\",\"contacts\",\"content\",\"newsfeeds\",\"weblinks\"]}}},\"spellchecker\":{\"engine\":\"googlespell\",\"languages\":\"English=en\",\"pspell_mode\":\"PSPELL_FAST\",\"pspell_spelling\":\"\",\"pspell_jargon\":\"\",\"pspell_encoding\":\"\",\"pspell_dictionary\":\"components\\/com_jce\\/editor\\/tiny_mce\\/plugins\\/spellchecker\\/dictionary.pws\",\"pspellshell_aspell\":\"\\/usr\\/bin\\/aspell\",\"pspellshell_tmp\":\"\\/tmp\"},\"article\":{\"show_readmore\":\"1\",\"show_pagebreak\":\"1\",\"hide_xtd_btns\":\"0\"},\"filemanager\":{\"dir\":\"assets\\/files\",\"max_size\":\"\",\"extensions\":\"xml=xml;html=htm,html;text=txt,rtf,pdf;office=doc,docx,ppt,xls;image=gif,jpeg,jpg,png;archive=zip,tar,gz;video=swf,mov,wmv,avi,flv,mp4,ogv,ogg,webm,mpeg,mpg;audio=wav,mp3,ogg,webm,aiff;openoffice=odt,odg,odp,ods,odf\",\"replace_text\":\"1\",\"text_alert\":\"1\",\"filesystem\":{\"name\":\"\"},\"target\":\"\",\"option_icon_check\":\"0\",\"option_size_check\":\"0\",\"option_date_check\":\"0\",\"icon_path\":\"media\\/jce\\/icons\",\"icon_format\":\"{$name}.png\",\"date_format\":\"%d\\/%m\\/%Y, %H:%M\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\"},\"imgmanager_ext\":{\"dir\":\"assets\\/images\",\"max_size\":\"\",\"extensions\":\"image=jpeg,jpg,png,gif\",\"filesystem\":{\"name\":\"\"},\"margin_top\":\"\",\"margin_right\":\"\",\"margin_bottom\":\"\",\"margin_left\":\"\",\"border\":\"0\",\"border_width\":\"1\",\"border_style\":\"solid\",\"border_color\":\"#000000\",\"align\":\"\",\"tabs_rollover\":\"1\",\"tabs_advanced\":\"1\",\"attributes_dimensions\":\"1\",\"attributes_align\":\"1\",\"attributes_margin\":\"1\",\"attributes_border\":\"1\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"file_delete\":\"1\",\"folder_move\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\",\"upload_resize\":\"1\",\"upload_resize_state\":\"0\",\"upload_thumbnail\":\"1\",\"upload_thumbnail_state\":\"0\",\"upload_thumbnail_crop\":\"0\",\"image_editor\":\"1\",\"thumbnail_editor\":\"1\",\"insert_multiple\":\"1\",\"mode\":\"list\",\"cache_enable\":\"0\",\"cache\":\"\",\"cache_size\":\"10\",\"cache_age\":\"30\",\"cache_files\":\"0\",\"resize_width\":\"640\",\"resize_height\":\"480\",\"resize_quality\":\"100\",\"resize_presets\":\"320x240,640x480,800x600,1024x768\",\"crop_presets\":\"4:3,16:9,20:30,320x240,240x320,640x480,480x640,800x600,1024x768\",\"thumbnail_width\":\"120\",\"thumbnail_height\":\"90\",\"thumbnail_quality\":\"80\",\"thumbnail_folder\":\"thumbnails\",\"thumbnail_prefix\":\"thumb_\",\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"},\"window\":{\"enable\":\"1\"}}},\"mediamanager\":{\"dir\":\"assets\\/media\",\"max_size\":\"\",\"extensions\":\"windowsmedia=avi,wmv,wm,asf,asx,wmx,wvx;quicktime=mov,qt,mpg,mpeg,m4a;flash=swf;shockwave=dcr;real=rm,ra,ram;divx=divx;video=mp4,ogv,ogg,webm;audio=mp3,ogg,webm;silverlight=xap\",\"filesystem\":{\"name\":\"\"},\"mediaplayer\":{\"name\":\"jceplayer\",\"jceplayer\":{\"extensions\":\"flv,f4v,mp3,mp4,xml\",\"path\":\"media\\/jce\\/mediaplayer\\/mediaplayer.swf\"}},\"margin_top\":\"\",\"margin_right\":\"\",\"margin_bottom\":\"\",\"margin_left\":\"\",\"border\":\"0\",\"border_width\":\"1\",\"border_style\":\"solid\",\"border_color\":\"#000000\",\"align\":\"\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\",\"aggregator\":{\"vimeo\":{\"enable\":\"1\",\"width\":\"400\",\"height\":\"225\"},\"youtube\":{\"enable\":\"1\",\"width\":\"425\",\"height\":\"350\"}},\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"}}},\"browser\":{\"dir\":\"assets\",\"max_size\":\"\",\"extensions\":\"xml=xml;html=htm,html;office=doc,docx,ppt,xls;text=txt,rtf;image=gif,jpeg,jpg,png;acrobat=pdf;archive=zip,tar,gz,rar;flash=swf;quicktime=mov,mp4,qt;windowsmedia=wmv,asx,asf,avi;audio=wav,mp3,aiff;openoffice=odt,odg,odp,ods,odf\",\"filesystem\":{\"name\":\"joomla\"},\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\"},\"media\":{\"strict\":\"1\",\"iframes\":\"0\",\"audio\":\"1\",\"video\":\"1\",\"object\":\"1\",\"embed\":\"1\",\"version_flash\":\"10,1,53,64\",\"version_windowsmedia\":\"10,00,00,3646\",\"version_quicktime\":\"7,3,0,0\",\"version_java\":\"1,5,0,0\",\"version_shockwave\":\"10,2,0,023\"}}'),
	(2,'Front End','Sample Front-end Profile','','3,4,5','',1,'help,newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,spacer,formatselect,styleselect;paste,searchreplace,indent,outdent,numlist,bullist,cleanup,charmap,removeformat,hr,sub,sup,textcase,nonbreaking,visualchars,visualblocks;fullscreen,preview,print,visualaid,style,xhtmlxtras,anchor,unlink,link,imgmanager,spellchecker,article','contextmenu,inlinepopups,help,paste,searchreplace,fullscreen,preview,print,style,textcase,nonbreaking,visualchars,visualblocks,xhtmlxtras,imgmanager,link,spellchecker,article,anchor',0,2,0,'0000-00-00 00:00:00',''),
	(3,'Client Manager Profile','Profile for clients with administration access','','6,7,2,3,4,5','',0,'newdocument,undo,redo,spacer,bold,italic,underline,strikethrough,blockquote,formatselect,styleselect,spacer,bullist,numlist;justifyfull,justifycenter,justifyleft,justifyright,anchor,link,unlink,spacer,spellchecker,paste,spacer,removeformat,cleanup;indent,outdent,spacer,hr,spacer,imgmanager_ext,mediamanager,filemanager,style,help','anchor,link,spellchecker,paste,cleanup,imgmanager_ext,mediamanager,filemanager,style,browser,contextmenu,inlinepopups,media',1,1,0,'0000-00-00 00:00:00','{\"editor\":{\"width\":\"\",\"height\":\"\",\"toolbar_theme\":\"default\",\"toolbar_align\":\"left\",\"toolbar_location\":\"top\",\"statusbar_location\":\"bottom\",\"path\":\"1\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"resizing_use_cookie\":\"1\",\"toggle\":\"1\",\"toggle_label\":\"[show\\/hide]\",\"toggle_state\":\"1\",\"dialog_theme\":\"jce\",\"relative_urls\":\"1\",\"verify_html\":\"\",\"schema\":\"\",\"forced_root_block\":\"\",\"profile_content_css\":\"2\",\"profile_content_css_custom\":\"\",\"theme_advanced_styles\":\"\",\"theme_advanced_blockformats\":[\"p\",\"h1\",\"h2\",\"h3\",\"h4\",\"div\"],\"theme_advanced_fonts_add\":\"\",\"theme_advanced_fonts_remove\":\"\",\"theme_advanced_font_sizes\":\"0.8em,1.0em,1.2em,1.4em\",\"custom_colors\":\"\",\"dir\":\"\",\"filesystem\":{\"name\":\"joomla\",\"joomla\":{\"allow_root\":\"0\",\"restrict_dir\":\"administrator,cache,components,includes,language,libraries,logs,media,modules,plugins,templates,xmlrpc\"}},\"max_size\":\"\",\"upload_conflict\":\"overwrite\",\"upload_runtimes\":[\"html5\",\"flash\"],\"browser_position\":\"bottom\",\"folder_tree\":\"1\",\"list_limit\":\"all\",\"validate_mimetype\":\"1\",\"websafe_mode\":\"utf-8\",\"upload_add_random\":\"0\",\"invalid_elements\":\"\",\"invalid_attributes\":\"dynsrc,lowsrc\",\"invalid_attribute_values\":\"\",\"extended_elements\":\"\",\"allow_javascript\":\"0\",\"allow_css\":\"0\",\"allow_php\":\"0\",\"cdata\":\"0\"},\"link\":{\"target\":\"\",\"file_browser\":\"1\",\"tabs_advanced\":\"1\",\"attributes_anchor\":\"1\",\"attributes_target\":\"1\",\"links\":{\"joomlalinks\":{\"enable\":\"1\",\"content\":\"1\",\"article_alias\":\"1\",\"static\":\"1\",\"contacts\":\"1\",\"weblinks\":\"1\",\"weblinks_alias\":\"1\",\"menu\":\"1\"}},\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"},\"window\":{\"enable\":\"1\"}},\"search\":{\"link\":{\"enable\":\"1\",\"plugins\":[\"categories\",\"contacts\",\"content\",\"newsfeeds\",\"weblinks\"]}}},\"spellchecker\":{\"engine\":\"googlespell\",\"languages\":\"English=en\",\"pspell_mode\":\"PSPELL_FAST\",\"pspell_spelling\":\"\",\"pspell_jargon\":\"\",\"pspell_encoding\":\"\",\"pspell_dictionary\":\"components\\/com_jce\\/editor\\/tiny_mce\\/plugins\\/spellchecker\\/dictionary.pws\",\"pspellshell_aspell\":\"\\/usr\\/bin\\/aspell\",\"pspellshell_tmp\":\"\\/tmp\"},\"paste\":{\"use_dialog\":\"0\",\"dialog_width\":\"450\",\"dialog_height\":\"400\",\"force_cleanup\":\"0\",\"strip_class_attributes\":\"all\",\"remove_spans\":\"0\",\"remove_styles\":\"0\",\"remove_attributes\":\"\",\"retain_style_properties\":\"\",\"remove_empty_paragraphs\":\"1\",\"remove_styles_if_webkit\":\"0\",\"html\":\"1\",\"text\":\"1\"},\"imgmanager_ext\":{\"dir\":\"assets\\/images\",\"max_size\":\"\",\"extensions\":\"image=jpeg,jpg,png,gif\",\"filesystem\":{\"name\":\"\"},\"margin_top\":\"\",\"margin_right\":\"\",\"margin_bottom\":\"\",\"margin_left\":\"\",\"border\":\"0\",\"border_width\":\"1\",\"border_style\":\"solid\",\"border_color\":\"#000000\",\"align\":\"\",\"tabs_rollover\":\"1\",\"tabs_advanced\":\"1\",\"attributes_dimensions\":\"1\",\"attributes_align\":\"1\",\"attributes_margin\":\"1\",\"attributes_border\":\"1\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"file_delete\":\"1\",\"folder_move\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\",\"upload_resize\":\"1\",\"upload_resize_state\":\"0\",\"upload_thumbnail\":\"1\",\"upload_thumbnail_state\":\"0\",\"upload_thumbnail_crop\":\"0\",\"image_editor\":\"1\",\"thumbnail_editor\":\"1\",\"insert_multiple\":\"1\",\"mode\":\"list\",\"cache_enable\":\"0\",\"cache\":\"\",\"cache_size\":\"10\",\"cache_age\":\"30\",\"cache_files\":\"0\",\"resize_width\":\"640\",\"resize_height\":\"480\",\"resize_quality\":\"100\",\"resize_presets\":\"320x240,640x480,800x600,1024x768\",\"crop_presets\":\"4:3,16:9,20:30,320x240,240x320,640x480,480x640,800x600,1024x768\",\"thumbnail_width\":\"120\",\"thumbnail_height\":\"90\",\"thumbnail_quality\":\"80\",\"thumbnail_folder\":\"thumbnails\",\"thumbnail_prefix\":\"thumb_\",\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"},\"window\":{\"enable\":\"1\"}}},\"mediamanager\":{\"dir\":\"assets\\/media\",\"max_size\":\"\",\"extensions\":\"windowsmedia=avi,wmv,wm,asf,asx,wmx,wvx;quicktime=mov,qt,mpg,mpeg,m4a;flash=swf;shockwave=dcr;real=rm,ra,ram;divx=divx;video=mp4,ogv,ogg,webm;audio=mp3,ogg,webm;silverlight=xap\",\"filesystem\":{\"name\":\"\"},\"mediaplayer\":{\"name\":\"jceplayer\",\"jceplayer\":{\"extensions\":\"flv,f4v,mp3,mp4,xml\",\"path\":\"media\\/jce\\/mediaplayer\\/mediaplayer.swf\"}},\"margin_top\":\"\",\"margin_right\":\"\",\"margin_bottom\":\"\",\"margin_left\":\"\",\"border\":\"0\",\"border_width\":\"1\",\"border_style\":\"solid\",\"border_color\":\"#000000\",\"align\":\"\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\",\"aggregator\":{\"vimeo\":{\"enable\":\"1\",\"width\":\"400\",\"height\":\"225\"},\"youtube\":{\"enable\":\"1\",\"width\":\"425\",\"height\":\"350\"}},\"popups\":{\"jcemediabox\":{\"enable\":\"1\"},\"widgetkit\":{\"enable\":\"1\",\"lightbox_titlePosition\":\"\",\"lightbox_padding\":\"\",\"lightbox_overlayShow\":\"\",\"lightbox_transitionIn\":\"\",\"lightbox_transitionOut\":\"\"}}},\"filemanager\":{\"dir\":\"assets\\/files\",\"max_size\":\"\",\"extensions\":\"xml=xml;html=htm,html;text=txt,rtf,pdf;office=doc,docx,ppt,xls;image=gif,jpeg,jpg,png;archive=zip,tar,gz;video=swf,mov,wmv,avi,flv,mp4,ogv,ogg,webm,mpeg,mpg;audio=wav,mp3,ogg,webm,aiff;openoffice=odt,odg,odp,ods,odf\",\"replace_text\":\"1\",\"text_alert\":\"1\",\"filesystem\":{\"name\":\"\"},\"target\":\"\",\"option_icon_check\":\"0\",\"option_size_check\":\"0\",\"option_date_check\":\"0\",\"icon_path\":\"media\\/jce\\/icons\",\"icon_format\":\"{$name}.png\",\"date_format\":\"%d\\/%m\\/%Y, %H:%M\",\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\"},\"browser\":{\"dir\":\"assets\",\"max_size\":\"\",\"extensions\":\"xml=xml;html=htm,html;office=doc,docx,ppt,xls;text=txt,rtf;image=gif,jpeg,jpg,png;acrobat=pdf;archive=zip,tar,gz,rar;flash=swf;quicktime=mov,mp4,qt;windowsmedia=wmv,asx,asf,avi;audio=wav,mp3,aiff;openoffice=odt,odg,odp,ods,odf\",\"filesystem\":{\"name\":\"\"},\"upload\":\"1\",\"folder_new\":\"1\",\"folder_delete\":\"1\",\"folder_rename\":\"1\",\"folder_move\":\"1\",\"file_delete\":\"1\",\"file_rename\":\"1\",\"file_move\":\"1\"},\"media\":{\"strict\":\"1\",\"iframes\":\"0\",\"audio\":\"1\",\"video\":\"1\",\"object\":\"1\",\"embed\":\"1\",\"version_flash\":\"10,1,53,64\",\"version_windowsmedia\":\"10,00,00,3646\",\"version_quicktime\":\"7,3,0,0\",\"version_java\":\"1,5,0,0\",\"version_shockwave\":\"10,2,0,023\"}}'),
	(4,'Blogger','Simple Blogging Profile','','','',0,'bold,italic,strikethrough,bullist,numlist,blockquote,spacer,justifyleft,justifycenter,justifyright,spacer,link,unlink,imgmanager,article,spellchecker,fullscreen,kitchensink;formatselect,underline,justifyfull,forecolor,paste,removeformat,charmap,indent,outdent,undo,redo,help','link,imgmanager,article,spellchecker,fullscreen,kitchensink,paste,contextmenu,inlinepopups',0,3,0,'0000-00-00 00:00:00','\n                \n            ');

/*!40000 ALTER TABLE `#__wf_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__widgetkit_widget
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__widgetkit_widget`;

CREATE TABLE `#__widgetkit_widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `style` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_application`;

CREATE TABLE `#__zoo_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `application_group` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__zoo_application` WRITE;
/*!40000 ALTER TABLE `#__zoo_application` DISABLE KEYS */;

INSERT INTO `#__zoo_application` (`id`, `name`, `alias`, `application_group`, `description`, `params`)
VALUES
	(1,'Content','content','content','',' {\n	\"template\": \"default\",\n	\"global.config.items_per_page\": \"15\",\n	\"global.config.item_order\":  {\n		\"0\": \"_itemname\",\n		\"1\": \"\",\n		\"2\": \"\",\n		\"3\": \"\",\n		\"4\": \"\",\n		\"5\": \"\"\n	},\n	\"global.config.show_feed_link\": \"1\",\n	\"global.config.feed_title\": \"\",\n	\"global.config.alternate_feed_link\": \"\",\n	\"global.template.show_title\": \"1\",\n	\"global.template.show_description\": \"1\",\n	\"global.template.show_image\": \"1\",\n	\"global.template.alignment\": \"left\",\n	\"global.template.date\": \"0\",\n	\"global.template.date_format\": \"%d||%B||%Y\",\n	\"global.template.items_cols\": \"1\",\n	\"global.template.items_order\": \"0\",\n	\"global.template.teaseritem_media_alignment\": \"left\",\n	\"global.template.teaseritem_media_width\": \"\",\n	\"global.template.item_media_alignment\": \"left\",\n	\"global.template.item_media_width\": \"\",\n	\"global.comments.enable_comments\": \"0\",\n	\"global.comments.require_name_and_mail\": \"1\",\n	\"global.comments.registered_users_only\": \"0\",\n	\"global.comments.approved\": \"0\",\n	\"global.comments.time_between_user_posts\": \"120\",\n	\"global.comments.email_notification\": \"\",\n	\"global.comments.email_reply_notification\": \"0\",\n	\"global.comments.avatar\": \"1\",\n	\"global.comments.order\": \"ASC\",\n	\"global.comments.max_depth\": \"5\",\n	\"global.comments.facebook_enable\": \"0\",\n	\"global.comments.facebook_app_id\": \"\",\n	\"global.comments.facebook_app_secret\": \"\",\n	\"global.comments.twitter_enable\": \"0\",\n	\"global.comments.twitter_consumer_key\": \"\",\n	\"global.comments.twitter_consumer_secret\": \"\",\n	\"global.comments.akismet_enable\": \"0\",\n	\"global.comments.akismet_api_key\": \"\",\n	\"global.comments.mollom_enable\": \"0\",\n	\"global.comments.mollom_public_key\": \"\",\n	\"global.comments.mollom_private_key\": \"\",\n	\"global.comments.blacklist\": \"\"\n}');

/*!40000 ALTER TABLE `#__zoo_application` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table #__zoo_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_category`;

CREATE TABLE `#__zoo_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`),
  KEY `PUBLISHED_INDEX` (`published`),
  KEY `APPLICATIONID_ID_INDEX` (`published`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_category_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_category_item`;

CREATE TABLE `#__zoo_category_item` (
  `category_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`item_id`),
  KEY `ITEMID_INDEX` (`item_id`),
  KEY `CATEGORYID_INDEX` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_comment`;

CREATE TABLE `#__zoo_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `content` text NOT NULL,
  `state` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `STATE_INDEX` (`state`),
  KEY `CREATED_INDEX` (`created`),
  KEY `ITEMID_INDEX` (`item_id`),
  KEY `AUTHOR_INDEX` (`author`),
  KEY `ITEMID_STATE_INDEX` (`item_id`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_item`;

CREATE TABLE `#__zoo_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `priority` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `state` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `searchable` int(11) NOT NULL,
  `elements` longtext NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`),
  KEY `PUBLISH_INDEX` (`publish_up`,`publish_down`),
  KEY `STATE_INDEX` (`state`),
  KEY `ACCESS_INDEX` (`access`),
  KEY `CREATED_BY_INDEX` (`created_by`),
  KEY `NAME_INDEX` (`name`),
  KEY `APPLICATIONID_INDEX` (`application_id`),
  KEY `TYPE_INDEX` (`type`),
  KEY `MULTI_INDEX` (`application_id`,`access`,`state`,`publish_up`,`publish_down`),
  KEY `MULTI_INDEX2` (`id`,`access`,`state`,`publish_up`,`publish_down`),
  KEY `ID_APPLICATION_INDEX` (`id`,`application_id`),
  FULLTEXT KEY `SEARCH_FULLTEXT` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_rating
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_rating`;

CREATE TABLE `#__zoo_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `element_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `value` tinyint(4) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_search_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_search_index`;

CREATE TABLE `#__zoo_search_index` (
  `item_id` int(11) NOT NULL,
  `element_id` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`item_id`,`element_id`),
  FULLTEXT KEY `SEARCH_FULLTEXT` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_submission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_submission`;

CREATE TABLE `#__zoo_submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(3) NOT NULL,
  `access` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_INDEX` (`alias`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_tag`;

CREATE TABLE `#__zoo_tag` (
  `item_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`name`),
  UNIQUE KEY `NAME_ITEMID_INDEX` (`name`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table #__zoo_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `#__zoo_version`;

CREATE TABLE `#__zoo_version` (
  `version` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `#__zoo_version` WRITE;
/*!40000 ALTER TABLE `#__zoo_version` DISABLE KEYS */;

INSERT INTO `#__zoo_version` (`version`)
VALUES
	('2.5.8');

/*!40000 ALTER TABLE `#__zoo_version` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
